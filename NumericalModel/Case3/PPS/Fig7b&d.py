"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd


def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys


rc = {'font.size': 11,
      "font.family" : "serif", 
      "mathtext.fontset" : "stix"}
plt.rcParams.update(rc)
plt.rcParams["font.serif"] = ["Times New Roman"] + plt.rcParams["font.serif"]
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '300'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean')
    T = readscalar(sol, timename, 'TMean')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

Xplane=0.74
H0=0.08
H=-Xplane*tan(8/180*pi)-H0
dx=0.01
# Number of division for linear interpolation
ngridy = 401
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -2
yinterpmax = 2
zinterpmin = H
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)




Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
    Uy_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[1, Iplane]), (yinterp, zinterp), method='linear')
    Uz_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (yinterp, zinterp), method='linear')
    Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')


# Plots the contour of sediment concentration

B0=2

# Plots the contour of U
Ut=np.zeros((100,401))
for i in range (0,100):
    for j in range (0,401):
      c=Ux_i[i][j]
      if str(c)=='[nan]':
          Ut[i][j]=0
      else:
          Ut[i][j]=c[0]



fig = plt.figure(figsize=(6, 4), dpi=500)
plt.rcParams.update({'font.size': 22,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :22,
}



################################
def addU (data):
    for i in range (0,len(data)):
        if str(data[i+1])!='nan':
            data[i]=0
            break
    return data
#####0
Uline=Ut[:,200]/0.075
Tline=T_i[:,200]/(8e-4)
zline=zinterp[:,200]

Uline=addU(Uline)
for i in range (0,len(Uline)):
    if zline[i]>-0.01:
        Uline[i]=np.nan
        

plt.plot(Uline,zline,'-.',color='blue',label='$y=0m$[$U/U_{0}$]',linewidth=2)
plt.plot(Tline,zline,'--',color='blue',label='$y=0m$'+r'[$\overline{R}/R_{0}$]',linewidth=2)
#####xp
Uline=Ut[:,250]/0.075
zline=zinterp[:,250]
Uline=addU(Uline)
Tline=T_i[:,250]/(8e-4)
plt.plot(Uline,zline,'-.',color='green',label='$y=0.5m$[$U/U_{0}$]',linewidth=2)
plt.plot(Tline,zline,'--',color='green',label='$y=0.5m$'+r'[$\overline{R}/R_{0}$]',linewidth=2)

#####xu
Uline=Ut[:,300]/0.075
zline=zinterp[:,225]
Tline=T_i[:,300]/(8e-4)
Uline=addU(Uline)
plt.plot(Uline,zline,'-.',color='orange',label='$y=1m$[$U/U_{0}$]',linewidth=2)
plt.plot(Tline,zline,'--',color='orange',label='$y=1m$'+r'[$\overline{R}/R_{0}$]',linewidth=2)

plt.xlabel('$U/U_{0}$ &' r'$\overline{R}$/${R}_{0}$',fontdict=font)
plt.ylabel('$z$ (m)',fontdict=font)

plt.legend(loc=(-0.3,-0.43),frameon=0,ncol=3)
plt.show()


plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}


plt.figure(figsize=(2.8, 1.1),dpi=500)
levels = np.arange(0, 1.02, 0.01)
a=plt.contourf(yinterp/B0, zinterp/(H0), T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
cb=plt.colorbar(a,ticks=np.arange(0,1.01,0.5),aspect=7.5)

cb.ax.tick_params(labelsize=10)
cb.set_label(label=r'$\overline{R}$/${R}_{0}$')

# Calculation of the streamline width as a function of the velociy magnitude
plt.ylim(H/H0,0)
plt.xlim(0.75,-0.75)
plt.xlabel('${y}/{B}_{0}$')
plt.ylabel('${z}/{H}_{0}$')

plt.yticks([-2,-1,0],fontsize=10)
plt.xticks([0.5,0,-0.5],fontsize=10)
plt.show()


   
plt.figure(figsize=(2.8, 1.1),dpi=500)
levels = np.arange(-0.1, 0.91,0.01)  
a=plt.contourf(yinterp/B0, zinterp/(H0), Ut/0.075, cmap='rainbow', levels=levels)

cb=plt.colorbar(a,ticks=np.arange(0,0.91,0.3),aspect=8)
cb.ax.tick_params(labelsize=10)
cb.set_label(label=r'$\overline{u}$/${U}_{0}$')

V=np.zeros((100,401))
W=np.zeros((100,401))
for i in range (0,100):
    for j in range (0,401):
      c=Uy_i[i][j]
      c2=Uz_i[i][j]
      if str(c)=='[nan]':
          V[i][j]=0
          W[i][j]=0
      else:
          W[i][j]=c2[0]
          V[i][j]=-c[0]
Uall = np.sqrt(V**2 + W**2)
lw = pow(Uall, 1.5)/Uall.max()
q=plt.quiver(yinterp[0][::10]/B0, zinterp[:,0][::10]/(H0), V[::10,::10],  W[::10,::10],width=0.008,scale=0.3,headwidth=2,color='black')
quiverkey(q, X=0.9, Y=-0.35, U=0.01875,
            label='= 0.25${U}_{0}$', labelpos='E')



plt.ylim(H/H0,0)
plt.xlim(0.75,-0.75)
plt.xlabel('$y/{B}_{0}$')
plt.ylabel('$z/{H}_{0}$')
plt.yticks([-2,-1,0],fontsize=10)
plt.xticks([0.5,0,-0.5],fontsize=10)
plt.show()

