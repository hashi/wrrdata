"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename ='250'
if loadData==1:
   
    T = readscalar(sol, timename, 'TMean_w70T')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 200
ngridy = 400

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 3
yinterpmin = -4
yinterpmax =  4

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.04
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')
   
###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


#plt.savefig('z=-0.04uvw.png', bbox_inches = 'tight')

# Define plot parameters
fig = plt.figure(figsize=(4, 4), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.ylim(0,3)
plt.xlim(1.2,-1.2)
plt.ylabel('x(m)')
plt.xlabel('y(m)')

# Plots the contour of sediment concentration
levels = np.arange(0, 1.05, 0.01)
a=plt.contourf(yinterp, xinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),aspect=12,label='RDD')
cs=plt.contour(yinterp,xinterp,T_i/(8e-4),[0.5],colors='k')
p1=cs.collections[0].get_paths()[0]
coor_p1=p1.vertices
a=coor_p1
ay=a[:,0]
ax=a[:,1]
plt.scatter(ay,ax)
plt.show()


# Plots the contour of sediment concentration
fig = plt.figure(figsize=(4, 4), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.ylim(0,3)
plt.xlim(1.5,-1.5)
plt.ylabel('x(m)')
plt.xlabel('y(m)')


levels = np.arange(0, 1.05, 0.01)
a=plt.contourf(yinterp, xinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),aspect=12,label='RDD')
###point on right
yright=[]
xright=[]
for i in range (0,len(ay)):
    fillx=[0]
    filly=[0]
    if ay[i]<=0  :
        fillx[0]=ax[i]
        filly[0]=ay[i]
        xright=xright+fillx
        yright=yright+filly

plt.scatter(yright[::5],xright[::5],s=10)
fright=np.polyfit(yright,xright,3)
plt.scatter(-0.95,1,color='black')
pright=np.poly1d(fright)
xvals = pright(yright)
plt.plot(yright,xvals,color='black')  



# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html