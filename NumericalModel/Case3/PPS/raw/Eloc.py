

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 10:39:33 2020

@author: haoran
"""

"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec


def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    
    return ys

def calL (y,z):
    L=0
    for i in range (1,len(y)):
        dL=sqrt((y[i]-y[i-1])**2+(z[i]-z[i-1])**2)
        L=L+dL
    return L
H=0.643
H0=0.08
L=2
W=2
u0=0.075
Q0=H0*W*u0

sol = '../'
loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables0'

timename = '210'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')  

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

#import griddata from scipy package

# Number of division for linear interpolation
ngridy = 1200
ngridz = 643
# Interpolation grid dimensions
yinterpmin = -6
yinterpmax = 6
zinterpmin = -0.643
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)

L=np.zeros(60)
xc=np.zeros(60)
for n in range (1,61):
    xc[n-1]=0.05*n
    Xplane=0.05*n
    H=-Xplane*tan(8/180*pi)-H0
    dx=0.02
    Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))
    if interpolate==1:
         # Interpolation of scalra fields and vector field components
          
      
       T_i = griddata((y[Iplane], z[Iplane]), np.transpose(T[Iplane]), (yinterp, zinterp), method='linear')
    for i in range (0,len(T_i)):
        for j in range (0,len(T_i[0])):
            if zinterp[i][j]<H+0.001:
                T_i[i][j]=np.nan
    plt.figure(figsize=(10, 3),dpi=300)


    levels = np.arange(0, 1.02, 0.01)
    a=plt.contourf(yinterp, zinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
    cs=plt.contour(yinterp, zinterp, T_i/(8e-4),[0.2],color='white')
    
    for m in range (0,len(cs.collections[0].get_paths())):
        
        p1=cs.collections[0].get_paths()[m]
        coor_p1=p1.vertices
        a=coor_p1
        ay0=a[:,0]
        az=a[:,1]
        plt.plot(ay0,az,color='green')
        
        if len(ay0)>20:
           ay=smooth(ay0,7)
        
           L[n-1]=L[n-1]+calL(ay,az)
# Calculation of the streamline width as a function of the velociy magnitude
    plt.ylim(H,0)
    plt.xlim(1.5,-1.5)
    plt.xlabel('${y}$($m$)')
    plt.ylabel('${z}$($m$)')
    plt.show()

import pandas as pd 
pd.DataFrame(L).to_csv("/media/haoran/DATA/4mcases/controlFrd/E/case3/L.csv", header=None,index=None)


