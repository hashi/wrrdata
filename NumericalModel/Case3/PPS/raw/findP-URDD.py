"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from pylab import *
#from pylab import *
#import matplotlib.gridspec as gridspec


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '250'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w70U')
    T = readscalar(sol, timename, 'TMean_w70T')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 401
ngridz = 80

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 4
zinterpmin = -0.642
zinterpmax = 0

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
xinterp, zinterp = np.meshgrid(xi, zi)

Yplane=0
dy=0.01

Iplane=np.where(np.logical_and(y>=Yplane-dy,y<=Yplane+dy))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], z[Iplane]), T[Iplane], (xinterp, zinterp), method='linear')
    Ux_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (xinterp, zinterp), method='linear')
    Uz_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (xinterp, zinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

#find boundary of plunge region and underflow region based on U
NUMx=400

XprofileList2=np.arange(0,NUMx,1)
m=0


T=np.zeros((80,401))
for i in range (0,80):
    for j in range (0,401):
      c=T_i[i][j]
      if str(c)=='[nan]' or zinterp[i][j]<(-0.08-xinterp[i][j]*tan(8/180*pi)):
          T[i][j]=0
          
      else:
          T[i][j]=c

Ts=np.zeros((NUMx,80))
detaz=0.642/80
          
for Xprofile in XprofileList2:
    for j in range (0,80):
        if (T[j][Xprofile])>0*max(T[:,Xprofile]):
            Ts[m][j]=(T[j][Xprofile])
        else:
            Ts[m][j]=0
        Ts[m][79]=Ts[m][78]

    m=m+1
T2all=np.zeros(NUMx)
Tall=np.zeros(NUMx)
xm=np.zeros(NUMx)
hc=np.zeros(NUMx)
for m in range (0,NUMx):
    for j in range (0,80):
        T2all[m]+=(Ts[m][j])**2*detaz
        Tall[m]+=(Ts[m][j])*detaz
    hc[m]=(Tall[m])**2/T2all[m]
hcx=np.zeros(NUMx)
for n in range (0,NUMx):
    xm[n]=0.01*n

fig = plt.figure(figsize=(5, 2), dpi=300)
plt.rcParams.update({'font.size': 10.5,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :10.5,
}
plt.plot(xm,hc,'-.',color='black')

plt.xlabel('$x$(m)')
plt.ylabel('$h_{c}$ (cm)')

def Frdp (num):
    Uline=np.zeros(80)
    for i in range (0,80):
         if zinterp[i][num]>=(-0.08-xinterp[i][j]*tan(8/180*pi)):
              Uline[i]=Ux_i[i][num]
    Uline[79]=Uline[78]
    Um=0
    count=0
    for i in range (0,80):
         if zinterp[i][num]>=(-0.08-xinterp[i][j]*tan(8/180*pi)):
              Um=Um+Uline[i]
              count+=1
    Um=Um/count
    Tline=Ts[num]
    Tm=0
    count=0
    for i in range (0,80):
        if zinterp[i][num]>=(-0.08-xinterp[i][j]*tan(8/180*pi)):
             Tm=Tm+Tline[i]
             count+=1
    Tm=Tm/count        
    Hp=xm[num]*tan(8/180*pi)+0.08
    Frdp=Um/(9.81*Hp*Tm)**0.5
    return (Frdp)


Frdps=[]
xps=[]
fillx=[0]
fillFrd=[0]
for i in range (0,NUMx):
    if hc[i]>0.999*max(hc):
        fillx[0]=xm[i]
        fillFrd[0]=Frdp(i)
        xps=xps+fillx
        Frdps=Frdps+fillFrd


###########################
##calculate Frd
###########################
nonhc=hc/max(hc)
import pandas as pd 
pd.DataFrame(xm).to_csv("/media/haoran/DATA/4mcases/controlFrd/hc/case3/xm.csv", header=None,index=None)
pd.DataFrame(nonhc).to_csv("/media/haoran/DATA/4mcases/controlFrd/hc/case3/nonhc.csv", header=None,index=None)




# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html