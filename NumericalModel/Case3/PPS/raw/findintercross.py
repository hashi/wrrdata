"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar,  readfield
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '210'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')
    '''
    Tensor=readfield(sol, timename, 'UPrime2Mean_w30U')
    uu=Tensor[0]
    '''
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

Xplane=0.1
H0=0.08
H=-Xplane*tan(8/180*pi)-H0
dx=0.01
# Number of division for linear interpolation
ngridy = 200
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -2
yinterpmax = 2
zinterpmin = H
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)




Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
    '''
    Uy_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[1, Iplane]), (yinterp, zinterp), method='linear')
    Uz_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (yinterp, zinterp), method='linear')
    Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')
    uu_i = griddata((y[Iplane], z[Iplane]), np.transpose(uu[Iplane]), (yinterp, zinterp), method='linear')
    '''
###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline



# Plots the contour of sediment concentration
levels = np.arange(0, 1.02, 0.01)
plt.figure(figsize=(10, 1.5),dpi=300)
plt.rcParams.update({'font.size': 15,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :15,
}


a=plt.contourf(yinterp, zinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.5),aspect=7.5,label=r'$\overline{RDD}$/${RDD}_{0}$')
cs=plt.contour(yinterp, zinterp, T_i,[4e-4],linewidths=1,colors='blue')
# Calculation of the streamline width as a function of the velociy magnitude

plt.ylim(H,0)
plt.xlim(2,-2)
plt.xlabel('${y}$(m)',fontdict=font)
plt.ylabel('${z}$(m)',fontdict=font)


plt.show()


plt.figure(figsize=(5, 1.5),dpi=300)
a=plt.contourf(yinterp, zinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.5),aspect=7.5,label=r'$\overline{RDD}$/${RDD}_{0}$')

p1=cs.collections[0].get_paths()[1]
coor_p1=p1.vertices
a=coor_p1[::10]
ax=a[:,0]     ###actually y
ay=a[:,1]    ####actually z 
#plt.plot(ax,ay)
axnew=[]
aynew=[]
for i in range (0,len(ax)):
    if ay[i]>=H+0.001:
        fillx=[0]
        filly=[0]
        fillx[0]=ax[i]
        filly[0]=ay[i]
        axnew=axnew+fillx
        aynew=aynew+filly
    else:
        break
f2=np.polyfit(axnew,aynew,3)
p2 = np.poly1d(f2)
yvals2 = p2(axnew) 
plt.plot(axnew,yvals2,color='green',linewidth=2)
plt.ylim(H,0)
plt.xlim(2,-2)

####write the formula of the interface curve 
pd.DataFrame(f2).to_csv("./sideinterface/f%dcm.csv"%(Xplane*100), header=None,index=None)



# Plots the contour of U





    
    




