"""
This code is used to plot T-ynew-Utang contour

"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar, readfield
import matplotlib.pyplot as plt
import pandas as pd
from pylab import *


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)
 

timename = '240'
if loadData==1:    
    Tensor=readfield(sol, timename, 'UPrime2Mean_w30U')
    U = readvector(sol, timename, 'UMean_w30U')
    uu=Tensor[0]
    vv=Tensor[3]
    #k=readfield(sol, timename, 'kMean_w30k')
    K=uu+vv   ###turbulent kinetic energy


# Number of division for linear interpolation
ngridx = 251
ngridy = 301

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 2.5
yinterpmin = -1.2
yinterpmax =  1.2

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)


#############################################
###near surface
Zvert=-0.04
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    K_i = griddata((x[Iplane], y[Iplane]), K[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]),  np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')

plt.rcParams.update({'font.size': 9,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :9,
}
fig = plt.figure(figsize=(3,3),dpi=500)
levels = np.arange(0,6.1,0.01)
a=plt.contourf(xinterp,yinterp,K_i*10000,cmap='coolwarm',levels=levels,extend='both')
plt.colorbar(a,ticks=np.arange(0,6.1,2),label='${K}$ (${cm}^{2}$ ${s}^{-2}$)')
plt.xlabel('2x/B')
plt.xlim(0,2)
plt.ylabel('2y/B')
plt.ylim(-1.2,1.2)

n=30
c=15   
deta=5
jmax=np.zeros(n)
maxK=np.zeros(n)
jmax=np.zeros(n)
maxK=np.zeros(n)
for i in range (0,n):
    for j in range (0,len(K_i[:,int(i*deta+c)])):  #"15,20,25,30,35,40,45,50,55,60..."
        if yinterp[j][int(i*deta+c)]<0:
            if K_i[j][int(i*deta+c)]>maxK[i]:
                maxK[i]=K_i[j][int(i*deta+c)]
                jmax[i]=j                    
for i in range (0,n):
    plt.scatter(xinterp[0][int(i*deta+c)],yinterp[int(jmax[i])][0],color='black',s=1)  
x_1=np.zeros(n+1)
y_1=np.zeros(n+1)
x_1[0]=0
y_1[0]=-1
for i in range (0,n):
    x_1[i+1]=xinterp[0][int(i*deta+c)]
    y_1[i+1]=yinterp[int(jmax[i])][0]

f1=np.polyfit(x_1,y_1,4)
p1 = np.poly1d(f1)
y_1 = p1(x_1) 
plt.plot(x_1,y_1,color='black',linewidth=1)
plt.plot(x_1,-y_1,color='black',linewidth=1)
plt.show()



#############################################
###z=-8cm

Zvert=-0.075
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    K_i = griddata((x[Iplane], y[Iplane]), K[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]),  np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')

plt.rcParams.update({'font.size': 9,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :9,
}
fig = plt.figure(figsize=(3,3),dpi=500)
levels = np.arange(0,6.1,0.01)
a=plt.contourf(xinterp,yinterp,K_i*10000,cmap='coolwarm',levels=levels,extend='both')
plt.colorbar(a,ticks=np.arange(0,6.1,2),label='${K}$ (${cm}^{2}$ ${s}^{-2}$)')
plt.xlabel('2x/B')
plt.xlim(0,2)
plt.ylabel('2y/B')
plt.ylim(-1.2,1.2)

n=30
c=15   
deta=5
jmax=np.zeros(n)
maxK=np.zeros(n)
jmax=np.zeros(n)
maxK=np.zeros(n)
for i in range (0,n):
    for j in range (0,len(K_i[:,int(i*deta+c)])):  #"15,20,25,30,35,40,45,50,55,60..."
        if yinterp[j][int(i*deta+c)]<0:
            if K_i[j][int(i*deta+c)]>maxK[i]:
                maxK[i]=K_i[j][int(i*deta+c)]
                jmax[i]=j                    
for i in range (0,n):
    plt.scatter(xinterp[0][int(i*deta+c)],yinterp[int(jmax[i])][0],color='black',s=1)  
x_2=np.zeros(n+1)
y_2=np.zeros(n+1)
x_2[0]=0
y_2[0]=-1
for i in range (0,n):
    x_2[i+1]=xinterp[0][int(i*deta+c)]
    y_2[i+1]=yinterp[int(jmax[i])][0]

f2=np.polyfit(x_2,y_2,4)
p2 = np.poly1d(f2)
y_2 = p2(x_2) 
plt.plot(x_2,y_2,color='black',linewidth=1)
plt.plot(x_2,-y_2,color='black',linewidth=1)
plt.show()


#############################################
###z=-12cm

Zvert=-0.12
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    K_i = griddata((x[Iplane], y[Iplane]), K[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]),  np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')

plt.rcParams.update({'font.size': 9,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :9,
}
fig = plt.figure(figsize=(3,3),dpi=500)
levels = np.arange(0,6.1,0.01)
a=plt.contourf(xinterp,yinterp,K_i*10000,cmap='coolwarm',levels=levels,extend='both')
plt.colorbar(a,ticks=np.arange(0,6.1,2),label='${K}$ (${cm}^{2}$ ${s}^{-2}$)')
plt.xlabel('2x/B')
plt.xlim(0,2)
plt.ylabel('2y/B')
plt.ylim(-1.2,1.2)

n=25
c=40
deta=5
jmax=np.zeros(n)
maxK=np.zeros(n)
jmax=np.zeros(n)
maxK=np.zeros(n)
for i in range (0,n):
    for j in range (0,len(K_i[:,int(i*deta+c)])):  #"15,20,25,30,35,40,45,50,55,60..."
        if yinterp[j][int(i*deta+c)]<0:
            if K_i[j][int(i*deta+c)]>maxK[i]:
                maxK[i]=K_i[j][int(i*deta+c)]
                jmax[i]=j                    
for i in range (0,n):
    plt.scatter(xinterp[0][int(i*deta+c)],yinterp[int(jmax[i])][0],color='black',s=1)  
x_3=np.zeros(n)
y_3=np.zeros(n)

for i in range (0,n):
    x_3[i]=xinterp[0][int(i*deta+c)]
    y_3[i]=yinterp[int(jmax[i])][0]

f3=np.polyfit(x_3,y_3,3)
p3 = np.poly1d(f3)
y_3 = p3(x_3) 
plt.plot(x_3,y_3,color='black',linewidth=1)
plt.plot(x_3,-y_3,color='black',linewidth=1)
plt.show()

#########################
####export interface data
f="points"

with open (f,'w') as file:
    for i in range (0,len(x_1)):  
        file.write('(%.2f '%x_1[i]+'%.2f -0.04)'%y_1[i]+'\n')
    for i in range (0,len(x_2)):  
        file.write('(%.2f '%x_2[i]+'%.2f -0.08)'%y_2[i]+'\n')
    for i in range (0,len(x_3)):  
        file.write('(%.2f '%x_3[i]+'%.2f -0.12)'%y_3[i]+'\n')
                 
           





#################################
#### (x_1,yvals) is the surface triangle

##################################
####export points at the interface





        
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html