"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd
sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1
afa=8/180*pi
H0=0.08
if loadMesh==1:
   
    x, y, z = readmesh(sol)
    xcl=x*cos(afa)-z*sin(afa)
    ycl=y
    zcl=x*sin(afa)+z*cos(afa)+H0*cos(afa)
###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '300'
if loadData==1:   
    T = readscalar(sol, timename, 'TMean_w90T')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')  
    
    

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 200
ngridy = 400

# Interpolation grid dimensions
xclinterpmin = 0
xclinterpmax = 2.5
yclinterpmin = -2
yclinterpmax = 2


# Interpolation grid
xcli = np.linspace(xclinterpmin, xclinterpmax, ngridx)
ycli = np.linspace(yclinterpmin, yclinterpmax, ngridy)

# Structured grid creation
xclinterp, yclinterp = np.meshgrid(xcli, ycli)

Zclvert=0.04
dzcl=0.005

Iplane=np.where(np.logical_and(zcl>=Zclvert-dzcl,zcl<=Zclvert+dzcl))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((xcl[Iplane], ycl[Iplane]), T[Iplane], (xclinterp, yclinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

# Define plot parameters
fig = plt.figure(figsize=(4, 2), dpi=100)



# Plots the contour of sediment concentration
levels = np.arange(0, 1.02, 0.01)
a=plt.contourf(yclinterp, xclinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),label='RDD')
cs=plt.contour(yclinterp, xclinterp, T_i/(8e-4),[0.9],colors='k')
p1=cs.collections[0].get_paths()[0]
coor_p1=p1.vertices
a=coor_p1
ay=a[:,0]
ax=a[:,1]

p2=cs.collections[0].get_paths()[1]
coor_p2=p2.vertices
a2=coor_p2
ay2=a2[:,0]
ax2=a2[:,1]



plt.show()


plt.plot(ax,ay)
plt.plot(ax2,ay2)

pd.DataFrame(ax).to_csv("/media/haoran/DATA/4mcases/controlFrd/bot/RDD/case3/x1.csv", header=None,index=None)
pd.DataFrame(ay).to_csv("/media/haoran/DATA/4mcases/controlFrd/bot/RDD/case3/y1.csv", header=None,index=None)

pd.DataFrame(ax2).to_csv("/media/haoran/DATA/4mcases/controlFrd/bot/RDD/case3/x2.csv", header=None,index=None)
pd.DataFrame(ay2).to_csv("/media/haoran/DATA/4mcases/controlFrd/bot/RDD/case3/y2.csv", header=None,index=None)


# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html