"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '240'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

Xplane=0.74
H0=0.08
H=-Xplane*tan(8/180*pi)-H0
dx=0.01
# Number of division for linear interpolation
ngridy = 401
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -2
yinterpmax = 2
zinterpmin = H
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)




Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
    Uy_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[1, Iplane]), (yinterp, zinterp), method='linear')
    Uz_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (yinterp, zinterp), method='linear')
    Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')

###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


# Plots the contour of sediment concentration

plt.plot(yinterp[11],Uy_i[11])
plt.xlim(-1.5,0)
pd.DataFrame(yinterp[11]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP29/I_14/Vat0.7m/NUM/y.csv", header=None,index=None)
pd.DataFrame(Uy_i[11]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP29/I_14/Vat0.7m/NUM/V.csv", header=None,index=None)
pd.DataFrame(Uz_i[11]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP29/I_14/Vat0.7m/NUM/W.csv", header=None,index=None)



plt.plot(yinterp[78],Uy_i[78])

pd.DataFrame(yinterp[78]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/H_1/DATA_PLOT/x=0.5xup/NUM/y.csv", header=None,index=None)
pd.DataFrame(Uy_i[78]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/H_1/DATA_PLOT/x=0.5xup/NUM/V.csv", header=None,index=None)
pd.DataFrame(Uz_i[78]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/H_1/DATA_PLOT/x=0.5xup/NUM/W.csv", header=None,index=None)

pd.DataFrame(Ux_i[78]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/H_1/DATA_PLOT/x=0.5xup/NUM/U.csv", header=None,index=None)



pd.DataFrame(yinterp[22]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/I_12/DATA_PLOT/x=0.5xup/NUM/y.csv", header=None,index=None)
pd.DataFrame(Ux_i[22]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/I_12/DATA_PLOT/x=0.5xup/NUM/U.csv", header=None,index=None)
