"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '240'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

Xplane=1.2
H0=0.08
H=-Xplane*tan(8/180*pi)-H0
dx=0.02
# Number of division for linear interpolation
ngridy = 401
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -2
yinterpmax = 2
zinterpmin = H
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)




Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
    Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')

###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


# Plots the contour of sediment concentration

levels = np.arange(0, 1.02, 0.01)
plt.figure(figsize=(10, 3),dpi=300)
plt.rcParams.update({'font.size': 22,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :22,
}


a=plt.contourf(yinterp, zinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
cs=plt.contour(yinterp, zinterp, T_i/(8e-4),[0.5])

p1=cs.collections[0].get_paths()[1]
coor_p1=p1.vertices
a=coor_p1
ay=a[:,0]
az=a[:,1]

plt.show()


#####
###The interface is ay,az

# Plots the contour of U
Ut=np.zeros((100,401))
for i in range (0,100):
    for j in range (0,401):
      c=Ux_i[i][j]
      if str(c)=='[nan]':
          Ut[i][j]=0
      else:
          Ut[i][j]=c[0]
print('U(min)= %f ' % Ut.min())
print('U(max)= %f ' % Ut.max())
levels = np.arange(-1, 8.1,0.01)     
plt.figure(figsize=(10, 3),dpi=300)
a=plt.contourf(yinterp, zinterp, Ut*100, cmap='jet', levels=levels)
plt.colorbar(a,ticks=np.arange(-1,8.1,3),aspect=8,label='$U$ ($cm$ ${s}^{-1}$)')

plt.show()


plt.plot(ay,az)
###fit the interface
finter=np.polyfit(ay,az,3)    ###formula of interface
pinter=np.poly1d(finter)
zvals = pinter(ay)
plt.plot(ay,zvals,color='green',linewidth=2,label='formula')  
plt.xlabel('y')
plt.ylabel('x')
plt.show()
#####calculate the angle

finter_grad=np.zeros(3)

for i in range (0,3):
    finter_grad[i]=(3-i)*finter[i]   ### function dzcl/dycl 
pinter_grad = np.poly1d(finter_grad)


ys=np.linspace(min(ay),max(ay),20)

NUM=3 #which point do you want to calculate

###reference point
y0=ys[NUM]
z0=pinter(y0)
beta=arctan(1/pinter_grad(y0))

yoo=y0*cos(beta)-z0*sin(beta)
zoo=z0*cos(beta)+y0*sin(beta)

print('the reference point is adopted into:')
print(yoo,zoo)

##adapt the coordinates 
yall=np.zeros(int(len(yinterp)*len(yinterp[0])))
zall=np.zeros(int(len(zinterp)*len(zinterp[0]))) 
Uall=np.zeros(int(len(Ut)*len(Ut[0]))) 
Tall=np.zeros(int(len(T_i)*len(T_i[0]))) 

num=0

for i in range (0,len(yinterp)):
    for j in range (0,len(yinterp[0])):
        yall[num]=yinterp[i][j]
        zall[num]=zinterp[i][j]
        Uall[num]=Ut[i][j]
        Tall[num]=T_i[i][j]
        num=num+1
        



####rebuild mesh (interpolation)
ynew=yall*cos(beta)-zall*sin(beta)
znew=zall*cos(beta)+yall*sin(beta)
##define a function to adapt coordinate for single points
def changecoordinate (yb,zb):  
    ya=yb*cos(beta)-zb*sin(beta)
    za=zb*cos(beta)+yb*sin(beta)
    return (ya,za)

########################################################
#calculate the adapted coordinates for the four boundary 
#points
########################################################

leftup=changecoordinate(0,0)
leftdown=changecoordinate(0,zinterpmin)
rightup=changecoordinate(yinterpmin,0)
rightdown=changecoordinate(yinterpmin,zinterpmin)

yboun=np.zeros(4)
yboun[0]=leftup[0]
yboun[1]=leftdown[0]
yboun[2]=rightup[0]
yboun[3]=rightdown[0]

zboun=np.zeros(4)
zboun[0]=leftup[1]
zboun[1]=leftdown[1]
zboun[2]=rightup[1]
zboun[3]=rightdown[1]


######
print('max and mim ynew are:')
print(max(yboun),min(yboun))
print('max and mim znew are:')
print(max(zboun),min(zboun))

print('range of ynew is:')
print(max(yboun)-min(yboun))

print('range of znew is:')
print(max(zboun)-min(zboun))


ngridynew = int((max(yboun)-min(yboun))*300)
ngridznew = int((max(zboun)-min(zboun))*300)

ynewmax= max(yboun)
ynewmin = min(yboun)
znewmin = min(zboun)
znewmax = max(zboun)

ynewi = np.linspace(ynewmin, ynewmax, ngridynew)
znewi = np.linspace(znewmin, znewmax, ngridznew)

# Structured grid creation
ynewinterp, znewinterp = np.meshgrid(ynewi, znewi)

Unew = griddata((ynew, znew), Uall, (ynewinterp, znewinterp), method='linear')
Tnew = griddata((ynew, znew), Tall, (ynewinterp, znewinterp), method='linear')

####find the reference line (perpendicular to the interface)

a=plt.contourf(znewinterp, ynewinterp, Unew, cmap='jet')
plt.show()


####find the reference line (perpendicular to the interface)

err=1
for i in range (0,len(znewi)):
    if abs(znewi[i]-zoo)<err:
        err=abs(znewi[i]-zoo)
        m=i


#####get usful data

yline=[]
Uline=[]
Tline=[]
for i in range (0,len(ynewi)):
    fillU=[0]
    filly=[0]
    fillT=[0]
    if str(Unew[m][i])!='nan' :
        fillU[0]=Unew[m][i]
        filly[0]=ynewi[i]
        fillT[0]=Tnew[m][i]
        yline=yline+filly
        Uline=Uline+fillU
        Tline=Tline+fillT

Uin=[0,0]
Tin=[0,0]
yin=[yoo,yoo]
Uin[0]=min(Uline)
Uin[1]=max(Uline)
Tin[0]=min(Tline)
Tin[1]=max(Tline)
err=1
for i in range (0,len(yline)):
    if abs(yline[i]-yoo)<err:
        err=abs(yline[i]-yoo)
        numc=i
 
#numc=numc-5
dyline=yline[1]-yline[0]
space=int(0.01/dyline)

##########
###cut a small part of Uline
ycut=yline[numc-2:numc+3]
Ucut=Uline[numc-2:numc+3]
####fit this cut by a line
fcut=np.polyfit(ycut,Ucut,1)    ###formula of interface
pcut=np.poly1d(fcut)

plt.plot(ycut,pcut(ycut),'--',color='black')
plt.scatter(yline,Uline)
plt.plot(yin,Uin,'-.',color='black')

print('The gradient of U is: %.5f '%fcut[0])
plt.show()


Tcut=Tline[numc-2:numc+3]
fcut2=np.polyfit(ycut,Tcut,1)
pcut2=np.poly1d(fcut2)
print('The gradient of R is: %.5f '%fcut2[0])
plt.scatter(yline,Tline)
plt.plot(yin,[0,8e-4],'-.',color='black')
plt.plot(ycut,pcut2(ycut),'--',color='black')
plt.show()



print('The reference point is:')
print(y0,z0)

print('sin(beta) is %.3f'%sin(beta))