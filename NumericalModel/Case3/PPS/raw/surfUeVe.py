"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata
import pandas as pd
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '210'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
    #T = readscalar(sol, timename, 'TMean_w5T')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 301
ngridy = 201

# Interpolation grid dimensions
xinterpmin = -0
xinterpmax = 3
yinterpmin = -1
yinterpmax =  1

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    #T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')
    Uy_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[1, Iplane]), (xinterp, yinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline
#plot u
fig = plt.figure(figsize=(3, 3), dpi=300)
plt.rcParams.update({'font.size': 8,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :8,
}

Ut=np.zeros((201,301))
for i in range (0,201):
    for j in range (0,301):
        c=Ux_i[i][j]
        Ut[i][j]=c[0]
print('U(min)= %f ' % Ut.min())
print('U(max)= %f ' % Ut.max())
levels = np.arange(-0.1, 1.1,0.01)     
a=plt.contourf(xinterp, yinterp, Ut/0.075, cmap='jet', levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.1,0.25),label='$U/U_{0}$')
plt.xlim(0,2)
plt.ylim(-1.2,1.2)
plt.xlabel('$2x/B_{0}$')
plt.ylabel('$2y/B_{0}$')


nums=24  ##5,10,....
Ue=np.zeros(nums)
xe=np.zeros(nums)
Ve=np.zeros(nums)
for i in range (0,nums):
    numx=i*5+5
    for j in range (0,200):
        if float(Uy_i[j][numx])==float(max(Uy_i[:,numx])):
              plt.scatter(xi[numx],yi[j])
              Ue[i]=Ux_i[j][numx] 
              Ve[i]=Uy_i[j][numx] 
    xe[i]=numx          
              
plt.show()

pd.DataFrame(xe).to_csv("/media/haoran/DATA/4mcases/surUeVe/8degree/12_3/xe.csv", header = None,index=None)
pd.DataFrame(Ue).to_csv("/media/haoran/DATA/4mcases/surUeVe/8degree/12_3/Ue.csv", header = None,index=None)
pd.DataFrame(Ve).to_csv("/media/haoran/DATA/4mcases/surUeVe/8degree/12_3/Ve.csv", header = None,index=None)
plt.scatter(xe,Ve)
plt.show()
plt.scatter(xe,Ue)
plt.show()
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html