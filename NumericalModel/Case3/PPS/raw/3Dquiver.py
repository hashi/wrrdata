"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '240'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#


# import griddata from scipy package




H0=-0.08
xi=np.linspace(0,2,5)
yi = np.linspace(-2,2, 401)
zi = np.linspace(-0.28, 0, 57)

yinterp, zinterp = np.meshgrid(yi, zi)
dx=0.01


X=np.zeros([len(xi),len(yi),len(zi)])
Y=np.zeros([len(xi),len(yi),len(zi)])
Z=np.zeros([len(xi),len(yi),len(zi)])

for i in range (0,len(X)):
    for j in range (0,len(X[0])):
        for k in range (0,len(X[0][0])):
            X[i][j][k]=xi[i]
            Y[i][j][k]=yi[j]
            Z[i][j][k]=zi[k]
    

u=np.zeros([len(xi),len(yi),len(zi)])
v=np.zeros([len(xi),len(yi),len(zi)])
w=np.zeros([len(xi),len(yi),len(zi)])
R=np.zeros([len(xi),len(yi),len(zi)])
m=0
for Xplane in xi:
    Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))
    T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
    V_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[1, Iplane]), (yinterp, zinterp), method='linear')
    W_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (yinterp, zinterp), method='linear')
    U_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')
    for i in range (0,len(R[m])):
        R[m][i]=T_i[:,i]
        u[m][i]=U_i[:,i,0]
        v[m][i]=V_i[:,i,0]
        w[m][i]=W_i[:,i,0]
    m=m+1
fig = plt.figure(figsize=(15, 4), dpi=100)
ax = fig.add_subplot(111, projection='3d')
ax.set_title("3D Quiver plot")

#drawing quiver plot

ax.quiver(X[:,::20,::5], Y[:,::20,::5], Z[:,::20,::5], u[:,::20,::5], v[:,::20,::5], w[:,::20,::5], length=1.2)

'''
for i in range (0,len(X)):
    for j in range (0,len(X[0])):
        for k in range (0,len(X[0][0])):
            if R[i][j][k]<=0.2*8e-4:
                u[i][j][k]=np.nan
                v[i][j][k]=np.nan
                w[i][j][k]=np.nan
                
ax.quiver(X[:,::40,::5], Y[:,::40,::5], Z[:,::40,::5], u[:,::40,::5], v[:,::40,::5], w[:,::40,::5], length=1.2, color='red')
'''
#plot the slope
Xs = np.linspace(0,250,201)
Ys = np.linspace(-150,150,301)
Xs, Ys= np.meshgrid(Xs,Ys)
Zs = np.zeros((len(Xs),len(Xs[0])))
for i in range (0,len(Xs)):
    for j in range (0, len(Xs[0])):
         Zs[i][j] = -Xs[i][j]*tan(8/180*np.pi)-8
surf = ax.plot_surface(Xs/100, Ys/100, Zs/100, color='grey',  alpha=0.3)# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

ax.view_init(30,-65)
plt.xlim(0,2.5)
ax.set_yticks([-2,-1,0,1,2])
ax.set_zticks([-0.4,-0.3,-0.2,-0.1,0])
ax.set_xlabel('$x$ ($m$)')
ax.set_ylabel('$y$ ($m$)')
ax.set_zlabel('$z$ ($m$)')
plt.show()
# Structured grid creation


