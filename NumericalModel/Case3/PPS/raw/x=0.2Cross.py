"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '280'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridy = 200
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -1
yinterpmax = 1
zinterpmin = -0.08
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)


#H=0.75
H0=0.08
#L=4.75
#H=-Xplane*((H-H0)/L)-H0
H=-0.08
dx=0.01


for i in range (5,10):
    if i!=9:
       Xplane=-9+i*1
    else:
       Xplane=-0.2
    Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

    if interpolate==1:
        # Interpolation of scalra fields and vector field components
        T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
        Uy_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[1, Iplane]), (yinterp, zinterp), method='linear')
        Uz_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (yinterp, zinterp), method='linear')
        Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')
    Yprofile=100
    plt.plot(Ux_i[:,Yprofile,0],zinterp[:,Yprofile],label='x=%.2fm'%Xplane)     
#ADV measeured data
hm=[0.004,0.009,0.018,0.045]
hm=np.array(hm)
Um=[0.065,0.071,0.076,0.086]
Um=np.array(Um)
plt.scatter(Um*0.92,hm-0.08)



xlabel('U((m/s)')
ylabel('z(m)')
plt.legend()
plt.show() 


for i in range (5,10):
    
    if interpolate==1:
        # Interpolation of scalra fields and vector field components
        T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
        Uy_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[1, Iplane]), (yinterp, zinterp), method='linear')
        Uz_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (yinterp, zinterp), method='linear')
        Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')
    if i!=9:
       Xplane=-9+i*1
    else:
       Xplane=-0.2
    Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))
    Zprofile=50
    plt.plot(yinterp[Zprofile,:],Ux_i[Zprofile,:,0],label='x=%.2fm'%Xplane)



ylabel('U((m/s)')
xlabel('y(m)')
plt.legend()  
plt.xlim(-1,1)  
plt.ylim(0.05,0.09)
plt.show()

for i in range (8,10):
    if interpolate==1:
        # Interpolation of scalra fields and vector field components
        T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
        Uy_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[1, Iplane]), (yinterp, zinterp), method='linear')
        Uz_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (yinterp, zinterp), method='linear')
        Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')
    if i!=9:
       Xplane=-9+i*1
    else:
       Xplane=-0.2
    Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))
    Zprofile=50    

    plt.plot(yinterp[Zprofile,:],Uy_i[Zprofile,:,0],label='x=%.2fm'%Xplane)
ylabel('V((m/s)')
xlabel('y(m)')
plt.xlim(-1,1)
plt.ylim(-0.01,0.01)
plt.legend()
plt.show()    
    
  
    
    
#plt.savefig('crossUVW.png', bbox_inches = 'tight')



