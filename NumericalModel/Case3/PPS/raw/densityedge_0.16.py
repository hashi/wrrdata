"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename ='180'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 250
ngridy = 400

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 2.5
yinterpmin = -4
yinterpmax =  4

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.16
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')
    Uy_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[1, Iplane]), (xinterp, yinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


#plt.savefig('z=-0.04uvw.png', bbox_inches = 'tight')

# Define plot parameters
fig = plt.figure(figsize=(4, 4), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.ylim(0,2)
plt.xlim(-1.2,1.2)
plt.xlabel('x(m)')
plt.ylabel('y(m)')

# Plots the contour of sediment concentration
levels = np.arange(0, 1.05, 0.01)
a=plt.contourf(yinterp, xinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),aspect=12,label='RDD')
cs=plt.contour(yinterp,xinterp,T_i/(8e-4),[0.2],colors='k')
p1=cs.collections[0].get_paths()[0]
coor_p1=p1.vertices
a=coor_p1
ax=a[:,0]
ay=a[:,1]
plt.show()


plt.plot(ax,ay)


import pandas as pd
pd.DataFrame(a).to_csv("-0.16edge0.2.csv", header=None,index=None)


# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html