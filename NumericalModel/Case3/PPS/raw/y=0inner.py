"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
from pylab import *
#from pylab import *
#import matplotlib.gridspec as gridspec


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1
color=['b','yellow','pink','g','r','purple','orange']

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables
j=0


#plot the slope


for i in range (250,270,60):
    timename = str(i)
    if loadData==1:
        U = readvector(sol, timename, 'UMean_w70U')
        T = readscalar(sol, timename, 'TMean_w70T')
        #U = readvector(sol, timename, 'U')
        #T = readscalar(sol, timename, 'T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
    ngridx = 401
    ngridz = 80

# Interpolation grid dimensions
    xinterpmin = 0.
    xinterpmax = 4
    zinterpmin = -0.65
    zinterpmax = 0

# Interpolation grid
    xi = np.linspace(xinterpmin, xinterpmax, ngridx)
    zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
    xinterp, zinterp = np.meshgrid(xi, zi)

    Yplane=0
    dy=0.01

    Iplane=np.where(np.logical_and(y>=Yplane-dy,y<=Yplane+dy))

    if interpolate==1:
    # Interpolation of scalra fields and vector field components
        T_i = griddata((x[Iplane], z[Iplane]), T[Iplane], (xinterp, zinterp), method='linear')
        Ux_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (xinterp, zinterp), method='linear')
        Uz_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (xinterp, zinterp), method='linear')

for i in range (0,len(T_i)):
    for j in range (0,len(T_i[0])):
       if zinterp[i][j]<(-0.08-xinterp[i][j]*tan(8/180*pi)):
          T_i[i][j]=np.nan
          Ux_i[i][j]=np.nan
          Uz_i[i][j]=np.nan
          

# Plots the contour of U
fig = plt.figure(figsize=(8, 2), dpi=100)
plt.rcParams.update({'font.size': 15,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :15,
}

H=-(0.08+tan(8/180*np.pi)*xinterpmax)
Ut=np.zeros((80,401))
for i in range (0,80):
    for j in range (0,401):
       c=Ux_i[i][j]
       Ut[i][j]=c[0]
print('U(min)= %f ' % Ut.min())
print('U(max)= %f ' % Ut.max())
levels = np.arange(-0.1, 1.11,0.01)     

a=plt.contourf(xinterp, zinterp, Ut/0.075, cmap='jet', levels=levels)


plt.colorbar(a,ticks=np.arange(-0.1,1.11,0.6),aspect=8,label='${U}/U_{0}$')
plt.xticks([0,0.6,1.2,1.8,2.4])
plt.xlim(0,2.4)
plt.ylim(-2.4*np.tan(8/180*np.pi)-0.08,0)
plt.xlabel('${x}$(m)',fontdict=font)
plt.ylabel('${z}$(m)',fontdict=font)
plt.show()


cs=plt.contour(xinterp, zinterp, Ut/0.075,[0.6])
for i in range (0,len(cs.collections[0].get_paths())):
    if len(cs.collections[0].get_paths()[i])>10:
        print(i)

p1=cs.collections[0].get_paths()[14]
coor_p1=p1.vertices
a=coor_p1
az=a[:,1]
ax=a[:,0]
plt.plot(ax,az)

pd.DataFrame(ax).to_csv("/media/haoran/DATA/4mcases/controlFrd/inner/case3/xout.csv", header=None,index=None)
pd.DataFrame(az).to_csv("/media/haoran/DATA/4mcases/controlFrd/inner/case3/zout.csv", header=None,index=None)


