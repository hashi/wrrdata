
###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z
'''
This code calculates the interficial thickness 
'''

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
from pylab import *


zm=-0.01

#define input parameters
H0=0.08

S=tan(8/180*pi)   ##slope
######formula for the right edge
###x=fh(y)
#fh=array([1.24886913, 2.21476453, 2.46320545, 1.57121228]) #z=-0.02
#fh=array([1.40419651, 2.4028571 , 2.51551141, 1.61486469]) #z=-0.04
#fh=array([0.80280231, 1.17656895, 1.87150374, 1.66735169])   #z=-0.08
#fh=array([-14.7963398 , -40.08499826, -34.66796866,  -9.11154807,
 #        0.07709521,   0.83251051,   1.75818792])  #z=-0.16

fh=array([1.2825436 , 2.31712276, 2.52512032, 1.55112392])


ph = np.poly1d(fh)

fh_grad=np.zeros(3)

for i in range (0,3):
    fh_grad[i]=(3-i)*fh[i]   ### function dy/dx ----> x  meaning that afa=arctan(fc_grad(x))
ph_grad = np.poly1d(fh_grad)

ylist=np.linspace(-1,0,1001)
xlist=ph(ylist)



fig = plt.figure(figsize=(5,3), dpi=100)
plt.plot(ylist,xlist)
plt.plot(-ylist,xlist)
plt.xlim(1.5,-1.5)

####################
####################
####################
ytarget=-0.3
####################
####################
####################

xc=ph(ytarget)
yc=ytarget

plt.scatter(yc,xc,c='white',edgecolor='black',s=50)
plt.show()

print('the reference point on the horizontal plane is:')

print('yc=%.4fm'%yc +' ' + 'xc=%.4fm'%xc)

#, afa is the angle between x coordinate and tangent line
afa=arctan(1/ph_grad(yc))

xo=xc*cos(afa)+yc*sin(afa)
yo=-xc*sin(afa)+yc*cos(afa)

print('the adapted point on the horizontal plane is:')

print('yo=%.4fm'%yo +' ' + 'xo=%.4fm'%xo)


#find the file to read and load mesh
sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)
    #adapt the coordinate according to afa
    xcl=x*cos(afa)+y*sin(afa)
    ycl=-x*sin(afa)+y*cos(afa)
    zcl=z

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables

#define the time file to read, timename=390 means the mean of 360s-390s
timename = '210'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')
    Ux=U[0]
    Uy=U[1]
    Uz=U[2]
    #adapt velocity data to adapted coordinate
    Uxcl=Ux*cos(afa)+Uy*sin(afa)
    Uycl=-Ux*sin(afa)+Uy*cos(afa)
    Uzcl=Uz
    Ucl=array([Uxcl,Uycl,Uzcl])
   
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridycl = 800
ngridzcl = 160
# Interpolation grid dimensions
Xclplane=xo


yclinterpmax = 0
yclinterpmin=yo*2
zclinterpmin = -2*xc*S-H0
zclinterpmax = 0


# Interpolation grid
ycli = np.linspace(yclinterpmin, yclinterpmax, ngridycl)
zcli = np.linspace(zclinterpmin, zclinterpmax, ngridzcl)
# Structured grid creation
yclinterp, zclinterp = np.meshgrid(ycli, zcli)

###########
###########
dxcl=0.01
###########
###########
Iplane=np.where(np.logical_and(xcl>=Xclplane-dxcl,xcl<=Xclplane+dxcl))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    Tcl_i = griddata((ycl[Iplane], zcl[Iplane]), T[Iplane], (yclinterp, zclinterp), method='linear')
    Uycl_i = griddata((ycl[Iplane], zcl[Iplane]), np.transpose(Ucl[1, Iplane]), (yclinterp, zclinterp), method='linear')
    Uzcl_i = griddata((ycl[Iplane], zcl[Iplane]), np.transpose(Ucl[2, Iplane]), (yclinterp, zclinterp), method='linear')
    Uxcl_i = griddata((ycl[Iplane], zcl[Iplane]), np.transpose(Ucl[0, Iplane]), (yclinterp, zclinterp), method='linear')

Uycl=np.zeros((len(Uycl_i),len(Uycl_i[0])))
Uzcl=np.zeros((len(Uycl_i),len(Uycl_i[0])))
Uxcl=np.zeros((len(Uycl_i),len(Uycl_i[0])))

for i in range (0,len(Uycl_i)):
    for j in range (0,len(Uycl_i[0])):
        Uycl[i][j]=Uycl_i[i][j][0]
        Uzcl[i][j]=Uzcl_i[i][j][0]
        Uxcl[i][j]=Uxcl_i[i][j][0]

for i in range (0,len(Uycl_i)):
    for j in range (0,len(Uycl_i[0])):
        if (-(xo*cos(afa)-yclinterp[i][j]*sin(afa))*S-0.08)>zclinterp[i][j]:
            Uycl[i][j]=np.nan
            Uzcl[i][j]=np.nan
            Uxcl[i][j]=np.nan
            Tcl_i[i][j]=np.nan
###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

# Define plot parameters
fig = plt.figure(figsize=(10,2), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.xlabel('y-perp(m)')
plt.ylabel('z(m)')
# Plots the contour of sediment concentration
for i in range (0,len(yclinterp)):
    for j in range (0,len(yclinterp[0])):
        if zclinterp[i][j]>-0.005:
            Tcl_i[i][j]=np.nan
            Uycl[i][j]=np.nan
            Uzcl[i][j]=np.nan
            Uxcl[i][j]=np.nan
levels = np.arange(0, 1.02, 0.01)
a=plt.contourf(yclinterp, zclinterp, Tcl_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),label='RDD')
cs=plt.contour(yclinterp, zclinterp, Tcl_i/(8e-4), [0.5],color='black')


plt.xlim(0,yclinterpmin)
plt.ylim(zclinterpmin,0)
plt.show()


#####################plot shear velocity
fig = plt.figure(figsize=(10,2), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.xlabel('ycl(m)')
plt.ylabel('z(m)')

levels = np.arange(-0.1, 1.02, 0.01)
a=plt.contourf(yclinterp, zclinterp, Uxcl/(0.075), cmap='jet', levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),label='U/U0')

plt.xlim(0,yclinterpmin)
plt.ylim(zclinterpmin,0)
plt.show()

##############################
##from here, start to calculate shear thickness

######################

err=1
for i in range (0,len(zcli)):
    if abs(zcli[i]-zm)<err:
        err=abs(zcli[i]-zm)
        m=i


#####get usful data

yline=[]
Uline=[]
for i in range (0,len(ycli)):
    fillU=[0]
    filly=[0]
    if str(Uxcl[m][i])!='nan':
        fillU[0]=Uxcl[m][i]
        filly[0]=ycli[i]
        yline=yline+filly
        Uline=Uline+fillU
plt.scatter(yline,Uline)      

detaU=max(Uline)-min(Uline)
#detaU=max(Uline)-0
normalU=(Uline-min(Uline))/detaU   ###normalised velocity U/detaU
#normalU=(Uline)/detaU
for i in range (20,len(Uline)):
    if normalU[i]>=0.15:
        ylinelow=yline[i]
        plt.scatter(ylinelow,Uline[i],color='red',s=50)
        break
for i in range (0,len(Uline)):
    if normalU[i]>=0.85:
        ylinehigh=yline[i]
        plt.scatter(ylinehigh,Uline[i],color='orange',s=50)
        break

Uin=[0,0]
yin=[yo,yo]
Uin[0]=min(Uline)
Uin[1]=max(Uline)
plt.plot(yin,Uin,'-.',color='black')
plt.show()  
print('##################################################')
print('##################################################')
print('##################################################')
cita=(ylinehigh-ylinelow)/(0.85-0.15)  
print('interface thickness is: %.2f cm'%(cita*100))
print('the angel afa is: %.1f '%(afa*180/pi))  ##gnew=g'*sin(afa)
print('Uline max is %.2f cm/s'%(max(Uline)*100))
print('Uline min is %.2f cm/s'%(min(Uline)*100))

   