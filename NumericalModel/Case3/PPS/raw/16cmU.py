"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata
import pandas as pd
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '210'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
    #T = readscalar(sol, timename, 'TMean_w5T')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 251
ngridy = 401

# Interpolation grid dimensions
xinterpmin = -0
xinterpmax = 2.5
yinterpmin = -2
yinterpmax =  2

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.16
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    #T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')
    Uy_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[1, Iplane]), (xinterp, yinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline



#plot u
fig = plt.figure(figsize=(5, 3), dpi=300)
plt.rcParams.update({'font.size': 8,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :8,
}

plt.plot(xi,Ux_i[201]/0.075,color='red',label='NUM')
plt.xlim(-0.2,2)
plt.ylim(-0.1,1.2)
plt.xlabel('2x/B')
plt.ylabel('$U/{U}_{x=0}$')
plt.legend()


pd.DataFrame(xi).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/H_4/DATA_PLOT/y=0/NUM/x.csv", header = None,index=None)
pd.DataFrame(Ux_i[201]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/H_4/DATA_PLOT/y=0/NUM/U.csv", header = None,index=None)

# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html