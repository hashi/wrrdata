"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
from pylab import *
#from pylab import *
#import matplotlib.gridspec as gridspec


def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    
    return ys







sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1
color=['b','yellow','pink','g','r','purple','orange']

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables
j=0


#plot the slope


for i in range (300,330,60):
    timename = str(i)
    if loadData==1:
        U = readvector(sol, timename, 'UMean')
        T = readscalar(sol, timename, 'TMean')
        #U = readvector(sol, timename, 'U')
        #T = readscalar(sol, timename, 'T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
    ngridx = 401
    ngridz = 80

# Interpolation grid dimensions
    xinterpmin = 0.
    xinterpmax = 4
    zinterpmin = -0.65
    zinterpmax = 0

# Interpolation grid
    xi = np.linspace(xinterpmin, xinterpmax, ngridx)
    zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
    xinterp, zinterp = np.meshgrid(xi, zi)

    Yplane=0
    dy=0.01

    Iplane=np.where(np.logical_and(y>=Yplane-dy,y<=Yplane+dy))

    if interpolate==1:
    # Interpolation of scalra fields and vector field components
        T_i = griddata((x[Iplane], z[Iplane]), T[Iplane], (xinterp, zinterp), method='linear')
        Ux_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (xinterp, zinterp), method='linear')
        Uz_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (xinterp, zinterp), method='linear')

for i in range (0,len(T_i)):
    for j in range (0,len(T_i[0])):
       if zinterp[i][j]<(-0.08-xinterp[i][j]*tan(8/180*pi)):
          T_i[i][j]=np.nan
          Ux_i[i][j]=np.nan
          Uz_i[i][j]=np.nan
          

# Plots the contour of sediment concentration
fig = plt.figure(figsize=(2.9, 1.1), dpi=500)
plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

H=-(0.08+tan(8/180*np.pi)*xinterpmax)
H0=0.08
xs=np.array([0,4])
ys=np.array([-0.08,-0.642])
plt.plot(xs/2,ys/H0,c='black')
levels = np.arange(0,1.05, 0.01)
a=plt.contourf(xinterp/2, zinterp/(H0), T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
    
    #b=plt.contour(xinterp, zinterp, T_i*10000,[1.7],linewidths=1,colors=color[j])
    #j+=1
#plt.clabel(b,inline=True, fontsize=7)
cb=plt.colorbar(a,ticks=np.arange(0,1.01,0.5),aspect=8,label=r'$\overline{R}$/${R}_{0}$')

cb.ax.tick_params(labelsize=10)
cb.set_label(label=r'$\overline{R}$/${R}_{0}$')

plt.xlabel('${x}/{B}_{0}$')
plt.ylabel('${z}/{H}_{0}$')
plt.xlim(0,1.2)
plt.ylim((-2.4*np.tan(8/180*np.pi)-0.08)/0.08,0)
plt.yticks([0,-2.5,-5],fontsize=10)
plt.xticks([0,0.4,0.8,1.2],fontsize=10)
#plt.plot([1.02,1.02],[-1.02*np.tan(8/180*np.pi)-0.08,0],'-.',color='blue')
#plt.plot([1.46,1.46],[-1.46*np.tan(8/180*np.pi)-0.08,0],'-.',color='red')
#plt.plot([2.32,2.32],[-2.32*np.tan(8/180*np.pi)-0.08,0],'-.',color='black')
#plt.text(0.95,0.02,'$0.7x_{up}$')
#plt.text(1.4,0.02,'$x_{up}$')
#plt.text(2.25,0.02,'$x_{ud}$')
#plt.text(0.1,-0.35,'(c)',color='black')
#plt.savefig('y=0RDD.png', bbox_inches = 'tight')
plt.show()


# Plots the contour of U
fig = plt.figure(figsize=(2.85, 1.05), dpi=300)
plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

Ut=np.zeros((80,401))
for i in range (0,80):
    for j in range (0,401):
       c=Ux_i[i][j]
       if str(c)=='[nan]':           
          Ut[i][j]=np.nan
       else:
          Ut[i][j]=c[0]
print('U(min)= %f ' % Ut.min())
print('U(max)= %f ' % Ut.max())
levels = np.arange(-0.1, 1.10,0.01)     

a=plt.contourf(xinterp/2, zinterp/(H0), Ut/0.075, cmap='rainbow', levels=levels)
#cs=plt.contour(xinterp, zinterp, Ut*100, [0.075*0.6*100])
#p1=cs.collections[0].get_paths()[43]
#coor_p1=p1.vertices
#c=coor_p1
#ay=c[:,1]
#ax=c[:,0]
W=np.zeros((80,401))
for i in range (0,80):
    for j in range (0,401):
      c2=Uz_i[i][j]
      if str(c2)=='[nan]':
          W[i][j]=0
      else:
          W[i][j]=c2[0]
Uall = np.sqrt(Ut**2 + W**2)
lw = pow(Uall, 1.5)/Uall.max()
q=plt.quiver(xinterp[0][::10]/2, zinterp[:,0][::4]/H0, Ut[::4,::10],  W[::4,::10],width=0.008,scale=1.2,headwidth=2,color='black')
quiverkey(q, X=0.85, Y=-0.35, U=0.075,
            label='= ${U}_{0}$', labelpos='E')


plt.plot(xs/2,ys/(H0),c='black')
cb=plt.colorbar(a,ticks=np.arange(-0.,1.01,0.5),aspect=8,label='$\overline{u}/{U}_{0}$')

cb.ax.tick_params(labelsize=10)
cb.set_label(label='$\overline{u}/{U}_{0}$')


plt.yticks([0,-2.5,-5],fontsize=10)
plt.xticks([0,0.4,0.8,1.2],fontsize=10)
plt.xlim(0,1.2)
plt.ylim((-2.4*tan(8/180*pi)-0.08)/0.08,0)
plt.xlabel('${x}/{B}_{0}$')
plt.ylabel('${z}/{H}_{0}$')
#plt.plot([1.02,1.02],[-1.02*np.tan(8/180*np.pi)-0.08,0],'-.',color='blue')
plt.plot([0.74/2,0.74/2],[(-0.75*np.tan(8/180*np.pi)-0.08)/H0,0],'-.',color='black',linewidth=1)
plt.plot([1.48/2,1.48/2],[(-1.5*np.tan(8/180*np.pi)-0.08)/H0,0],'-.',color='black',linewidth=1)
plt.plot([1.97/2,1.97/2],[(-1.97*np.tan(8/180*np.pi)-0.08)/H0,0],'-.',color='black',linewidth=1)
plt.text(0.6/2,0.02/H0,'$0.5x_{up}$')
plt.text(1.4/2,0.02/H0,'$x_{up}$')
plt.text(1.9/2,0.02/H0,'$x_{ud}$')
#plt.text(0.1,-0.35,'(a)',color='black')
plt.show()


def addU (data):
    for i in range (0,len(data)):
        if str(data[i+1])!='nan':
            data[i]=0
            break
    return data

fig = plt.figure(figsize=(6, 4), dpi=100)

#####0.7xp
Uline=Ut[:,102]/0.075
Tline=T_i[:,102]/(8e-4)
zline=zinterp[:,102]
Uline=addU(Uline)
plt.plot(Uline,zline,'-.',color='blue',label='$0.7x_{up}$[$U/U_{0}$]',linewidth=2)
plt.plot(Tline,zline,'--',color='blue',label='$0.7x_{up}$'+r'[$\overline{R}/R_{0}$]',linewidth=2)
#####xp
Uline=Ut[:,146]/0.075
zline=zinterp[:,146]
Uline=addU(Uline)
Tline=T_i[:,146]/(8e-4)
plt.plot(Uline,zline,'-.',color='red',label='$x_{up}$[$U/U_{0}$]',linewidth=2)
plt.plot(Tline,zline,'--',color='red',label='$x_{up}$'+r'[$\overline{R}/R_{0}$]',linewidth=2)

#####xu
Uline=Ut[:,225]/0.075
zline=zinterp[:,225]
Tline=T_i[:,225]/(8e-4)
Uline=addU(Uline)
plt.plot(Uline,zline,'-.',color='black',label='$x_{ud}$[$U/U_{0}$]',linewidth=2)
plt.plot(Tline,zline,'--',color='black',label='$x_{ud}$'+r'[$\overline{R}/R_{0}$]',linewidth=2)



plt.xlabel('$U/U_{0}$ &' r'$\overline{R}$/${R}_{0}$',fontdict=font)
plt.ylabel('$z$ (m)',fontdict=font)
plt.xlim(-0.01,1.1)
plt.ylim(-0.41,0)
plt.yticks([-0.4,-0.3,-0.2,-0.1,0])


plt.legend(loc=(-0.15,-0.45),frameon=0,ncol=3)

plt.show()


# Plots the contour of sediment concentration
fig = plt.figure(figsize=(6, 2), dpi=300)
plt.rcParams.update({'font.size': 15,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :15,
}
xs=[0,4]
ys=[-0.08,-0.642]
plt.plot(xs,ys,c='black',linewidth=3)
levels = np.arange(0,1.05, 0.01)

a=plt.contourf(xinterp, zinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)

#plt.plot(ax[::5],ay[::5],'--',color='black')   
    #b=plt.contour(xinterp, zinterp, T_i*10000,[1.7],linewidths=1,colors=color[j])
    #j+=1
#plt.clabel(b,inline=True, fontsize=7)
plt.colorbar(a,ticks=np.arange(0,1.01,0.5),aspect=8,label=r'$\overline{R}$/${R}_{0}$')

plt.xticks([0,0.5,1,1.5,2])
plt.xlabel('${x}$($m$)',fontdict=font)
plt.ylabel('${z}$($m$)',fontdict=font)
plt.xlim(0,2.3)
plt.ylim(-2.3*np.tan(8/180*np.pi)-0.08,0)
#plt.plot([1.02,1.02],[-1.02*np.tan(8/180*np.pi)-0.08,0],'-.',color='blue')
#plt.plot([1.46,1.46],[-1.46*np.tan(8/180*np.pi)-0.08,0],'-.',color='red')
#plt.plot([2.32,2.32],[-2.32*np.tan(8/180*np.pi)-0.08,0],'-.',color='black')
#plt.text(0.95,0.02,'$0.7x_{up}$')
#plt.text(1.4,0.02,'$x_{up}$')
#plt.text(2.25,0.02,'$x_{ud}$')
plt.text(-0.2,0.05,'(c)',color='black')
#plt.savefig('y=0RDD.png', bbox_inches = 'tight')
plt.show()



cs=plt.contour(xinterp, zinterp, Ut/0.075,[0.6])

max1st=0
for i in range (0,len(cs.collections[0].get_paths())):
    if len(cs.collections[0].get_paths()[i])>max1st:
        max1st=len(cs.collections[0].get_paths()[i])
max2nd=0
for i in range (0,len(cs.collections[0].get_paths())):
    if max2nd<len(cs.collections[0].get_paths()[i])<max1st:
        max2nd=len(cs.collections[0].get_paths()[i])
        number=i
p1=cs.collections[0].get_paths()[number]
coor_p1=p1.vertices
a=coor_p1
ax=a[:,0]
az=a[:,1]
plt.show()

plt.plot(ax,az)
import pandas as pd
pd.DataFrame(ax).to_csv("/media/haoran/DATA/4mcases/controlFrd/LSZ/3/ax.csv", header = None,index=None)
pd.DataFrame(az).to_csv("/media/haoran/DATA/4mcases/controlFrd/LSZ/3/az.csv", header = None,index=None)
