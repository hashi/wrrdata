
###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z
'''
Complete calculation of interficial shear thickness. 
(1) create a 'cross-section' perpendicular to triangle edge based on RDD=0.5RDD0
(2) on the cross-section, find an interface based on RDD=0.5RDD0
(3) calculate interficial shear thickness
'''

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
from pylab import *


zm=-0.24 

#define input parameters
H0=0.08

S=tan(8/180*pi)   ##slope
######formula for the right edge
###x=fh(y)

#fh=array([1.14613876, 1.92853974, 2.29284383, 1.59923137]) #z=-0.04
#fh=array([1.31243625, 2.04560381, 2.26716473, 1.69764631])   #z=-0.08
#fh=array([-0.46681591, -0.7431331 ,  0.97547833,  1.70095784]) #z=-0.12

#fh=array([-1.2041395 , -2.18745954,  0.07116456,  1.74670778])
#fh=array([-0.53728945,  0.47270794,  1.92917668])
fh=array([-1.70308679, -3.7584136 , -1.49043294,  1.85161136])
ph = np.poly1d(fh)

fh_grad=np.zeros(3)

for i in range (0,3):
    fh_grad[i]=(3-i)*fh[i]   ### function dy/dx ----> x  meaning that afa=arctan(fc_grad(x))
ph_grad = np.poly1d(fh_grad)

ylist=np.linspace(-1,0,1001)
xlist=ph(ylist)



fig = plt.figure(figsize=(5,3), dpi=100)
plt.plot(ylist,xlist)
plt.plot(-ylist,xlist)
plt.xlim(1.5,-1.5)

####################
####################
####################
ytarget=-0.9
####################
####################
####################

xc=ph(ytarget)
yc=ytarget

plt.scatter(yc,xc,c='white',edgecolor='black',s=50)
plt.show()

print('the reference point on the horizontal plane is:')

print('yc=%.4fm'%yc +' ' + 'xc=%.4fm'%xc)

#, afa is the angle between x coordinate and tangent line
afa=arctan(1/ph_grad(yc))

xo=xc*cos(afa)+yc*sin(afa)
yo=-xc*sin(afa)+yc*cos(afa)

print('the adapted point on the horizontal plane is:')

print('yo=%.4fm'%yo +' ' + 'xo=%.4fm'%xo)


#find the file to read and load mesh
sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)
    #adapt the coordinate according to afa
    xcl=x*cos(afa)+y*sin(afa)
    ycl=-x*sin(afa)+y*cos(afa)
    zcl=z

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables

#define the time file to read, timename=390 means the mean of 360s-390s
timename = '250'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w70U')
    T = readscalar(sol, timename, 'TMean_w70T')
    Ux=U[0]
    Uy=U[1]
    Uz=U[2]
    #adapt velocity data to adapted coordinate
    Uxcl=Ux*cos(afa)+Uy*sin(afa)
    Uycl=-Ux*sin(afa)+Uy*cos(afa)
    Uzcl=Uz
    Ucl=array([Uxcl,Uycl,Uzcl])
   
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields ar6e interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridycl = 800
ngridzcl = 160
# Interpolation grid dimensions
Xclplane=xo


yclinterpmax = 0
yclinterpmin=yo*2
zclinterpmin = -2*xc*S-H0
zclinterpmax = 0


# Interpolation grid
ycli = np.linspace(yclinterpmin, yclinterpmax, ngridycl)
zcli = np.linspace(zclinterpmin, zclinterpmax, ngridzcl)
# Structured grid creation
yclinterp, zclinterp = np.meshgrid(ycli, zcli)

###########
###########
dxcl=0.01
###########
###########
Iplane=np.where(np.logical_and(xcl>=Xclplane-dxcl,xcl<=Xclplane+dxcl))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    Tcl_i = griddata((ycl[Iplane], zcl[Iplane]), T[Iplane], (yclinterp, zclinterp), method='linear')
    Uycl_i = griddata((ycl[Iplane], zcl[Iplane]), np.transpose(Ucl[1, Iplane]), (yclinterp, zclinterp), method='linear')
    Uzcl_i = griddata((ycl[Iplane], zcl[Iplane]), np.transpose(Ucl[2, Iplane]), (yclinterp, zclinterp), method='linear')
    Uxcl_i = griddata((ycl[Iplane], zcl[Iplane]), np.transpose(Ucl[0, Iplane]), (yclinterp, zclinterp), method='linear')

Uycl=np.zeros((len(Uycl_i),len(Uycl_i[0])))
Uzcl=np.zeros((len(Uycl_i),len(Uycl_i[0])))
Uxcl=np.zeros((len(Uycl_i),len(Uycl_i[0])))

for i in range (0,len(Uycl_i)):
    for j in range (0,len(Uycl_i[0])):
        Uycl[i][j]=Uycl_i[i][j][0]
        Uzcl[i][j]=Uzcl_i[i][j][0]
        Uxcl[i][j]=Uxcl_i[i][j][0]

for i in range (0,len(Uycl_i)):
    for j in range (0,len(Uycl_i[0])):
        if (-(xo*cos(afa)-yclinterp[i][j]*sin(afa))*S-0.08)>zclinterp[i][j]:
            Uycl[i][j]=np.nan
            Uzcl[i][j]=np.nan
            Uxcl[i][j]=np.nan
            Tcl_i[i][j]=np.nan
###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

# Define plot parameters
fig = plt.figure(figsize=(10,2), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.xlabel('y-perp(m)')
plt.ylabel('z(m)')
# Plots the contour of sediment concentration
for i in range (0,len(yclinterp)):
    for j in range (0,len(yclinterp[0])):
        if zclinterp[i][j]>-0.005:
            Tcl_i[i][j]=np.nan
            Uycl[i][j]=np.nan
            Uzcl[i][j]=np.nan
            Uxcl[i][j]=np.nan
levels = np.arange(0, 1.02, 0.01)
a=plt.contourf(yclinterp, zclinterp, Tcl_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),label='RDD')
cs=plt.contour(yclinterp, zclinterp, Tcl_i/(8e-4), [0.5],color='black')
p1=cs.collections[0].get_paths()[0]
coor_p1=p1.vertices
a=coor_p1
ayclo=a[:,0]
azclo=a[:,1]

aycl=[]
azcl=[]

for i in range (0,len(ayclo)):
    fillay=[0]
    fillaz=[0]
    if azclo[i]>-1:   ###change to -0.04 for upper layers
        fillay[0]=ayclo[i]
        fillaz[0]=azclo[i]
        aycl=aycl+fillay
        azcl=azcl+fillaz


'''
formula of interface
'''
finter=np.polyfit(aycl,azcl,3)    ###formula of interface
pinter=np.poly1d(finter)
zvals = pinter(aycl)
plt.plot(aycl,zvals,color='green',linewidth=2)  

plt.xlim(0,yclinterpmin)
plt.ylim(zclinterpmin,0)
plt.show()


#####################plot shear velocity
fig = plt.figure(figsize=(10,2), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.xlabel('ycl(m)')
plt.ylabel('z(m)')

levels = np.arange(-0.1, 1.02, 0.01)
a=plt.contourf(yclinterp, zclinterp, Uxcl/(0.075), cmap='jet', levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),label='U/U0')

plt.plot(aycl,zvals,color='red',linewidth=2)  
plt.xlim(0,yclinterpmin)
plt.ylim(zclinterpmin,0)


##############################
##from here, start to calculate shear thickness

for i in range (0,len(aycl)):
    if zvals[i]<=zm:
        ym=aycl[i]
        break



finter_grad=np.zeros(3)

for i in range (0,3):
    finter_grad[i]=(3-i)*finter[i]   ### function dzcl/dycl 
pinter_grad = np.poly1d(finter_grad)

rflycl=[0,0]
rflzcl=[0,0]
rflycl[0]=ym-0.05
rflycl[1]=ym+0.05
rflzcl[0]=zm+0.05/pinter_grad(ym)
rflzcl[1]=zm-0.05/pinter_grad(ym)

plt.plot(rflycl,rflzcl,color='black')   ######plot the reference line
plt.show()

######################



print('the 2nd order reference point is:')
print(ym,zm)

beta=arctan(1/pinter_grad(ym))

#reference point in new mesh
yoo=ym*cos(afa)-zm*sin(afa)
zoo=zm*cos(afa)+ym*sin(afa)

print('the 2nd order reference point is adopted into:')
print(yoo,zoo)



##adapt the coordinates 
yall=np.zeros(int(len(yclinterp)*len(yclinterp[0])))
zall=np.zeros(int(len(zclinterp)*len(zclinterp[0]))) 
Uall=np.zeros(int(len(Uxcl)*len(Uxcl[0]))) 
Vall=np.zeros(int(len(Uxcl)*len(Uxcl[0]))) 
Wall=np.zeros(int(len(Uxcl)*len(Uxcl[0]))) 
Tall=np.zeros(int(len(Tcl_i)*len(Tcl_i[0]))) 

num=0    

for i in range (0,len(yclinterp)):
    for j in range (0,len(yclinterp[0])):
        yall[num]=yclinterp[i][j]
        zall[num]=zclinterp[i][j]
        Uall[num]=Uxcl[i][j]
        Vall[num]=Uycl[i][j]
        Wall[num]=Uzcl[i][j]
        Tall[num]=Tcl_i[i][j]
        num=num+1


ynew=yall*cos(afa)-zall*sin(afa)
znew=zall*cos(afa)+yall*sin(afa)

####rebuild mesh (interpolation)

##define a function to adapt coordinate for single points
def changecoordinate (yb,zb):  
    ya=yb*cos(afa)-zb*sin(afa)
    za=zb*cos(afa)+yb*sin(afa)
    return (ya,za)

########################################################
#calculate the adapted coordinates for the four boundary 
#points
########################################################

leftup=changecoordinate(0,0)
leftdown=changecoordinate(0,zclinterpmin)
rightup=changecoordinate(yclinterpmin,0)
rightdown=changecoordinate(yclinterpmin,zclinterpmin)

yboun=np.zeros(4)
yboun[0]=leftup[0]
yboun[1]=leftdown[0]
yboun[2]=rightup[0]
yboun[3]=rightdown[0]

zboun=np.zeros(4)
zboun[0]=leftup[1]
zboun[1]=leftdown[1]
zboun[2]=rightup[1]
zboun[3]=rightdown[1]

#######
print('max and mim ynew are:')
print(max(yboun),min(yboun))
print('max and mim znew are:')
print(max(zboun),min(zboun))

print('range of ynew is:')
print(max(yboun)-min(yboun))

print('range of znew is:')
print(max(zboun)-min(zboun))

ngridynew = int((max(yboun)-min(yboun))*300)
ngridznew = int((max(zboun)-min(zboun))*300)

ynewmax= max(yboun)
ynewmin = min(yboun)
znewmin = min(zboun)
znewmax = max(zboun)

ynewi = np.linspace(ynewmin, ynewmax, ngridynew)
znewi = np.linspace(znewmin, znewmax, ngridznew)

# Structured grid creation
ynewinterp, znewinterp = np.meshgrid(ynewi, znewi)

Unew = griddata((ynew, znew), Uall, (ynewinterp, znewinterp), method='linear')
Tnew = griddata((ynew, znew), Tall, (ynewinterp, znewinterp), method='linear')

####find the reference line (perpendicular to the interface)

a=plt.contourf(ynewinterp, znewinterp, Unew, cmap='jet')
plt.show()

err=1
for i in range (0,len(znewi)):
    if abs(znewi[i]-zoo)<err:
        err=abs(znewi[i]-zoo)
        m=i


#####get usful data

yline=[]
Uline=[]
Tline=[]
for i in range (0,len(ynewi)):
    fillU=[0]
    filly=[0]
    fillT=[0]
    if str(Unew[m][i])!='nan' :
        fillU[0]=Unew[m][i]
        filly[0]=ynewi[i]
        fillT[0]=Tnew[m][i]
        yline=yline+filly
        Uline=Uline+fillU
        Tline=Tline+fillT


Uin=[0,0]
Tin=[0,0]
yin=[yoo,yoo]
Uin[0]=min(Uline)
Uin[1]=max(Uline)
Tin[0]=min(Tline)
Tin[1]=max(Tline)
err=1
for i in range (0,len(yline)):
    if abs(yline[i]-yoo)<err:
        err=abs(yline[i]-yoo)
        numc=i
 
############find maxU
plt.plot(Uline)
plt.show()

Umax=0
for i in range (0,len(yline)):
    if Uline[i]>Umax:
        Umax=Uline[i]
        nummax=i
############find start      
arU=Uline[numc]   ###(Umin+Umax)/2
Umin=arU*2-Umax

Ustart=10
for i in range (108,110):
    if Uline[i]<Ustart:
        Ustart=Uline[i]
        numstart=i       
        
ylshort=yline[numstart:(nummax+1)]
Ulshort=Uline[numstart:(nummax+1)]

y0=yline[numc]

####U=0.5Umax(1+tanh((y-y0)/deta))
deta=np.linspace(0.001,0.1,1001)
err=np.zeros(len(deta))
for i in range (0,len(deta)):    
    Ufit=Umin+0.5*(Umax-Umin)*(1+np.tanh((ylshort-y0)/deta[i]))
    err[i]=sum((Ufit-Ulshort)**2)
for i in range (0,len(err)):
    if err[i]==min(err):
        detac=deta[i]
Ufit=Umin+0.5*(Umax-Umin)*(1+np.tanh((yline-y0)/detac))
        
    
plt.plot(yline,Ufit,'--',color='black',linewidth=2)
plt.scatter(yline,Uline)
plt.scatter(y0,Uline[numc],color='red',s=50)
plt.plot(yin,Uin,'--',color='red')

plt.show()



Tmax=8e-4

############find start      
for i in range (0,len(yline)):
    if Tline[i]>8e-4*0.1:
        numsT=i
        break
#############find end
for i in range (0,len(yline)):
   #if Tline[i]>8e-4*0.9:
        numeT=i
      #  break

y0T=yline[numc]


ysT=yline[numsT:numeT]
Tshort=Tline[numsT:numeT]
ysT=np.array(ysT)
Tshort=np.array(Tshort)
####T=0.5Tmax(1+tanh((y-y0T)/detaT))
detaT=np.linspace(0.01,0.1,101)
errT=np.zeros(len(detaT))
for i in range (0,len(detaT)):    
    Tfit=0.5*Tmax*(1+np.tanh((ysT-y0T)/detaT[i]))
    errT[i]=sum((Tfit-Tshort)**2)
for i in range (0,len(errT)):
    if errT[i]==min(errT):
        detacT=detaT[i]
yline=np.array(yline)
Tfit=0.5*Tmax*(1+np.tanh((yline-y0T)/detacT))
        
plt.scatter(yline,Tline)
plt.plot(yline,Tfit,'-.',color='black',linewidth=2)
plt.plot(yin,Tin,'--',color='red')
plt.scatter(yline[numc],Tline[numc],color='red')

plt.show() 

dyline=yline[1]-yline[0]

def calRi (num):
    gradUfit=(Ufit[num+1]-Ufit[num-1])/2/dyline
    gradTfit=(Tfit[num+1]-Tfit[num-1])/2/dyline
    Ri=gradTfit/((gradUfit)**2)*9.81*sin(beta)
    return (Ri)

Ri=calRi(numc)


print('##################################################')
print('##################################################')
print('##################################################')

print('xtarget is: %.2f m'%(xc))
print('the angel afa is: %.1f '%(afa*180/pi))  ##gnew=g'*sin(afa)
print('the angel beta is: %.1f'% (beta*180/pi))  ##gnew=g'*sin(afa)
print('the local Richardson number is: %.3f'% (Ri))  ##gnew=g'*sin(afa)

###########################################
###########################################


#####################plot shear velocity





   