"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '300'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w90U')
    #T = readscalar(sol, timename, 'TMean_w30T')
    #U = readvector(sol, timename, 'U')
    T = readscalar(sol, timename, 'T')
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 200
ngridy = 400

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 4
yinterpmin = -4
yinterpmax =  4

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.04
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')
    Uy_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[1, Iplane]), (xinterp, yinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline
fig = plt.figure(figsize=(6, 6), dpi=100)
plt.rcParams.update({'font.size': 10})
XprofileList=np.arange(20,80,20)
for Xprofile in XprofileList:
    plt.subplot(311)
    plt.plot(yinterp[:,Xprofile], T_i[:,Xprofile],label=str(round(xinterp[0,Xprofile]*100)/100))
    plt.xlabel('y(m)')
    plt.ylabel('RDD')
    #plt.legend()
    plt.xlim(-1,1)
    plt.subplot(312)
    plt.plot(yinterp[:,Xprofile], Ux_i[:,Xprofile,0])
    plt.plot([yinterp[0,Xprofile], yinterp[ngridy-1,Xprofile]], [0, 0],'--k')
    plt.xlabel('y(m)')
    plt.ylabel('u(m/s)')
    plt.xlim(-1,1)
    plt.subplot(313)
    plt.plot(yinterp[:,Xprofile], Uy_i[:,Xprofile,0])
    plt.xlabel('y(m)')
    plt.ylabel('v(m/s)')
    plt.xlim(-1,1)

#plt.savefig('z=-0.04uvw.png', bbox_inches = 'tight')

# Define plot parameters
fig = plt.figure(figsize=(6, 4), dpi=300)
plt.rcParams.update({'font.size': 10})
plt.xlim(0,2)
plt.ylim(-1,1)
plt.xlabel('x(m)')
plt.ylabel('y(m)')

# Plots the contour of sediment concentration
levels = np.arange(0, 4.1e-4, 1e-5)
a=plt.contourf(xinterp, yinterp, T_i, cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,4.1e-4,2e-4),aspect=8,label='RDD')

    
#plt.plot(x1,y1,color="black",linewidth=2 )



# Calculation of the streamline width as a function of the velociy magnitude
U_i = np.sqrt(Ux_i**2 + Uy_i**2)
lw = pow(U_i, 1.5)/U_i.max()

#Plots the streamlines
q=plt.quiver(xi[::10], yi[::10], Ux_i[::10,::10,0], Uy_i[::10,::10,0],width=0.005,scale=0.5,headwidth=3)
quiverkey(q, X=0.75, Y=-0.13, U=0.03,
            label='= 0.03 m/s', labelpos='E')


for Xprofile in XprofileList:
    plt.plot([xinterp[0,Xprofile], xinterp[0,Xprofile]],[yinterp[0,Xprofile], yinterp[ngridy-1,Xprofile]])
plt.xlim(0,2)
plt.ylim(-1,1)
#plt.savefig('z=-0.04RDD.png', bbox_inches = 'tight')
plt.show()


#plot u

plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

Ut=np.zeros((400,200))
for i in range (0,400):
    for j in range (0,200):
      c=Ux_i[i][j]
      if str(c)=='[nan]':
          Ut[i][j]=0
      else:
          Ut[i][j]=c[0]

a=plt.contourf(xinterp, yinterp, Ut/0.075, cmap='rainbow')
cs=plt.contour(xinterp,yinterp,Ut/0.075,[0])
maxlen=0
for i in range (0,len(cs.collections[0].get_paths())):
    if len(cs.collections[0].get_paths()[i])>maxlen:
        maxlen=len(cs.collections[0].get_paths()[i])
        num=i
p1=cs.collections[0].get_paths()[num]
coor_p1=p1.vertices
a=coor_p1[::1]
ax=a[:,0]
ay=a[:,1]
cs2=plt.contour(xinterp,yinterp,Ut/0.075,[0.05])
maxlen=0
for i in range (0,len(cs2.collections[0].get_paths())):
    if len(cs2.collections[0].get_paths()[i])>maxlen:
        maxlen=len(cs2.collections[0].get_paths()[i])
        num=i
p1=cs2.collections[0].get_paths()[num]
coor_p1=p1.vertices
a=coor_p1[::1]
ax2=a[:,0]
ay2=a[:,1]
print('U(min)= %f ' % Ut.min())
print('U(max)= %f ' % Ut.max())



fig = plt.figure(figsize=(3.2, 1.7), dpi=500)
levels = np.arange(-0.2, 1.21,0.01)     
a=plt.contourf(xinterp/2, yinterp/2, Ut/0.075, cmap='rainbow', levels=levels)


#plt.colorbar(a,ticks=np.arange(-0.1,1.01,0.5),aspect=10,label='${U}/{U}_{0}$')
plt.xticks([])
plt.ylim(0,1.5/2)
plt.xlim(0,2/2)
plt.yticks([0,0.5/2,1/2,1.5/2],fontsize=10)
plt.plot([0.74/2,0.74/2],[0,1.5/2],'-.',color='black',linewidth=1.5)
plt.plot([1.48/2,1.48/2],[0,1.5/2],'-.',color='black',linewidth=1.5)
#plt.xlabel('$2x/B_{0}$',font)
#plt.ylabel('$2y/B_{0}$',font)
#plt.savefig('z=-0.04u.png', bbox_inches = 'tight')
#Plots the streamlines
q=plt.quiver(xi[::7]/2, yi[::7]/2, Ux_i[::7,::7,0], Uy_i[::7,::7,0],width=0.008,scale=1.1,headwidth=2)
#quiverkey(q, X=0.8, Y=1.06, U=0.075,
        #    label='= ${U}_{0}$', labelpos='E')

####################
####plot also interface based on dye image
import pandas as pd 

points= (pd.read_csv("/media/haoran/DATA/4mcases/controlFrd/sur/EXP/exp35_max.csv", header = None)).values
x_pixel=points[:,1]
y_pixel=points[:,0]
xdye=(x_pixel-1162)/884-1
ydye=(3000-y_pixel)/884

xdye=np.append(xdye[::-1],[1.25,0.02])
ydye=np.append(ydye[::-1],[0.02,1])
plt.scatter(xdye/2,ydye/2,marker='*',color='red')

plt.text(0.65/2,1.6/2,'0.5${x}_{up}$')
plt.text(1.4/2,1.6/2,'${x}_{up}$')

#plt.plot(ax,ay,'--',color='black')

#plt.plot(ax2,ay2,'--',color='black')

'''
tri_4m= (pd.read_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP35/Numtri/tri_final0.9.csv", header = None)).values

ymodel=-tri_4m[:,0]
xmodel=tri_4m[:,1]

plt.plot(xmodel,ymodel,'-.',color='black',label='$NUM$:0.9${R}_{0} $')
'''
#################################


##################################



#########################
#plot v
fig = plt.figure(figsize=(3, 3), dpi=300)
plt.rcParams.update({'font.size': 8,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :8,
}

Vt=np.zeros((400,200))
for i in range (0,400):
    for j in range (0,200):
      c=Uy_i[i][j]
      if str(c)=='[nan]':
          Vt[i][j]=0
      else:
          Vt[i][j]=c[0]
print('V(min)= %f ' % Vt.min())
print('V(max)= %f ' % Vt.max())
levels = np.arange(-0.02, 0.021,0.001)     

a=plt.contourf(xinterp, yinterp, Vt, cmap='coolwarm', levels=levels)
plt.colorbar(a,ticks=np.arange(-0.02, 0.021,0.02),label='v(m ${s^{-1}}$)')
plt.ylim(-2,2)
plt.xlim(0,3)
plt.xlabel('${x}$(m)',font)
plt.ylabel('${y}$(m)',font)
#plt.savefig('z=-0.04v.png', bbox_inches = 'tight')

# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html