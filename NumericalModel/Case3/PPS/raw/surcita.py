"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename ='210'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 200
ngridy = 400

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 2
yinterpmin = -4
yinterpmax =  4

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')

###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


#plt.savefig('z=-0.04uvw.png', bbox_inches = 'tight')

# Define plot parameters
fig = plt.figure(figsize=(4, 4), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.ylim(0,2)
plt.xlim(1.2,-1.2)
plt.ylabel('x(m)')
plt.xlabel('y(m)')

# Plots the contour of sediment concentration
levels = np.arange(0, 1.05, 0.01)
a=plt.contourf(yinterp, xinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),aspect=12,label='RDD')
cs=plt.contour(yinterp,xinterp,T_i/(8e-4),[0.5],colors='k')
p1=cs.collections[0].get_paths()[0]
coor_p1=p1.vertices
a=coor_p1
ay=a[:,0]
ax=a[:,1]
plt.scatter(ay,ax)
plt.show()


# Plots the contour of sediment concentration
fig = plt.figure(figsize=(4, 4), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.ylim(0,2)
plt.xlim(1.5,-1.5)
plt.ylabel('x(m)')
plt.xlabel('y(m)')


levels = np.arange(0, 1.05, 0.01)
a=plt.contourf(yinterp, xinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),aspect=12,label='RDD')
###point on right
yright=[]
xright=[]
for i in range (0,len(ay)):
    fillx=[0]
    filly=[0]
    if ay[i]<=0:
        fillx[0]=ax[i]
        filly[0]=ay[i]
        xright=xright+fillx
        yright=yright+filly
plt.scatter(yright[::5],xright[::5],s=10)
fright=np.polyfit(yright,xright,3)
pright=np.poly1d(fright)
xvals = pright(yright)
plt.plot(yright,xvals,color='black')  
plt.show()

###############################
'''change xxxxxxxxxxxxxxxxxxxxxxxxxx'''
'''change xxxxxxxxxxxxxxxxxxxxxxxxxx'''
'''change xxxxxxxxxxxxxxxxxxxxxxxxxx'''
'''change xxxxxxxxxxxxxxxxxxxxxxxxxx'''
'''change xxxxxxxxxxxxxxxxxxxxxxxxxx'''
############################################################################
ytarget=-0.25
xtarget=pright(ytarget)
############################################################################
xc=xtarget
yc=ytarget

####crossover point
print('the crossover point is:')
print(yc,xc)

f_grad=np.zeros(3)
for i in range (0,3):
    f_grad[i]=(3-i)*fright[i]   ### function dx/dy 
p_grad = np.poly1d(f_grad)

afa=arctan(1/p_grad(yc))

xcl=x*cos(afa)+y*sin(afa)
ycl=y*cos(afa)-x*sin(afa)
zcl=z

#reference point in new mesh
xo=xc*cos(afa)+yc*sin(afa)
yo=-xc*sin(afa)+yc*cos(afa)

print('the reference point is adopted into:')
print(yo,xo)

Uxcl=U[0]*cos(afa)+U[1]*sin(afa)
Uycl=U[1]*cos(afa)-U[0]*sin(afa)
Uzcl=U[2]
Ucl=array([Uxcl,Uycl,Uzcl])
Tcl=T
###########
###########
dzcl=0.005
Zclplane=-0.01
###########
###########
Iplane=np.where(np.logical_and(zcl>=Zclplane-dzcl,zcl<=Zclplane+dzcl))

# Number of division for linear interpolation
##define a function to adapt coordinate for single points
def changecoordinate (yb,xb):  
    xa=xb*cos(afa)+yb*sin(afa)
    ya=yb*cos(afa)-xb*sin(afa)
    return (ya,xa)
leftup=changecoordinate(1.5,2)
leftdown=changecoordinate(1.5,0)
rightup=changecoordinate(-1.5,2)
rightdown=changecoordinate(-1.5,0)

yboun=np.zeros(4)
yboun[0]=leftup[0]
yboun[1]=leftdown[0]
yboun[2]=rightup[0]
yboun[3]=rightdown[0]

xboun=np.zeros(4)
xboun[0]=leftup[1]
xboun[1]=leftdown[1]
xboun[2]=rightup[1]
xboun[3]=rightdown[1]

#######
print('max and mim ynew are:')
print(max(yboun),min(yboun))
print('max and mim xnew are:')
print(max(xboun),min(xboun))

print('range of ynew is:')
print(max(yboun)-min(yboun))

print('range of xnew is:')
print(max(xboun)-min(xboun))

ngridycl = int((max(yboun)-min(yboun))*300)
ngridxcl = int((max(xboun)-min(xboun))*300)
yclinterpmax = max(yboun)
yclinterpmin=min(yboun)
xclinterpmin = min(xboun)
xclinterpmax = max(xboun)


ycli = np.linspace(yclinterpmin, yclinterpmax, ngridycl)
xcli = np.linspace(xclinterpmin, xclinterpmax, ngridxcl)

xclinterp, yclinterp = np.meshgrid(xcli, ycli)

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    Tcl_i = griddata((xcl[Iplane], ycl[Iplane]), Tcl[Iplane], (xclinterp, yclinterp), method='linear')
    Uycl_i = griddata((xcl[Iplane], ycl[Iplane]), np.transpose(Ucl[1, Iplane]), (xclinterp, yclinterp), method='linear')
    Uzcl_i = griddata((xcl[Iplane], ycl[Iplane]), np.transpose(Ucl[2, Iplane]), (xclinterp, yclinterp), method='linear')
    Uxcl_i = griddata((xcl[Iplane], ycl[Iplane]), np.transpose(Ucl[0, Iplane]), (xclinterp, yclinterp), method='linear')

for i in range (0,len(Tcl_i)):
    for j in range (0,len(Tcl_i[0])):
        if (xclinterp[i][j]*cos(afa)-yclinterp[i][j]*sin(afa))<0:
            Tcl_i[i][j]=np.nan
            Uycl_i[i][j]=np.nan
            Uxcl_i[i][j]=np.nan
            Uzcl_i[i][j]=np.nan
    

# Plots the contour of sediment concentration
fig = plt.figure(figsize=(4, 4), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.xlim(yclinterpmax,yclinterpmin)
plt.ylim(xclinterpmin,xclinterpmax)
plt.ylabel('x(m)')
plt.xlabel('y(m)')


levels = np.arange(0, 1.05, 0.01)
a=plt.contourf(yclinterp, xclinterp, Tcl_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),aspect=12,label='RDD')

plt.show()

#####get usful data

err=1
for i in range (0,len(xcli)):
    if abs(xcli[i]-xo)<err:
        err=abs(xcli[i]-xo)
        m=i


yline=[]
Uline=[]
for i in range (0,len(ycli)):
    fillU=[0]
    filly=[0]
    if str(Uxcl_i[i][m])!='nan':
        fillU[0]=Uxcl_i[i][m]
        filly[0]=ycli[i]
        yline=yline+filly
        Uline=Uline+fillU
        
plt.plot(Uline)
plt.show()


Umax=0
Umin=5
for i in range (430,500):
   ####find max and min
   '''
   if Uline[i]>Umax:
       Umax=Uline[i]
       nummax=i
       '''
   if Uline[i]<Umin:
       Umin=Uline[i]
       nummin=i

plt.plot(yline,Uline)
#plt.scatter(yline[nummax],Umax,s=50, color='red')
plt.scatter(yline[nummin],Umin,s=50,color='blue')


#calculate gradient dUline/dyline
grad=np.zeros((len(yline)-1))
for i in range (0,len(yline)-1):
    grad[i]=(Uline[i+1]-Uline[i])/(yline[i+1]-yline[i])

####smooth grad

grads=np.zeros(len(grad)-5)

for i in range (0,len(grad)-5):
    for j in range (0,5):
        grads[i]=grad[i]+grad[i+j]
    grads[i]=grads[i]/5
    
gradsmax=0    
for i in range (nummin,len(grads)):
    if grads[i]>gradsmax:
        gradsmax=grads[i]
        num=i

for i in range (num,len(grads)):
    if grads[i]<0.1*gradsmax:
        nummax=i
        Umax=Uline[i]
        break
plt.scatter(yline[nummax],Umax,s=50, color='red')    



detaU=Umax-Umin
normalU=(Uline-Umin)/detaU   ###normalised velocity U/detaU

for i in range (300,len(Uline)):
    if normalU[i]>=0.15:
        ylinelow=yline[i]
        plt.scatter(ylinelow,Uline[i],color='green',s=50)
        break
for i in range (300,len(Uline)):
    if normalU[i]>=0.85:
        ylinehigh=yline[i]
        plt.scatter(ylinehigh,Uline[i],color='orange',s=50)
        break

Uin=[0,0]
yin=[yo,yo]
Uin[0]=Umax
Uin[1]=Umin
plt.plot(yin,Uin,'-.',color='black')
plt.xlim(-1.5,-0.5)
plt.show()  
print('##################################################')
print('##################################################')
print('##################################################')
cita=(ylinehigh-ylinelow)/(0.85-0.15)  
print('interface thickness is: %.2f cm'%(cita*100))
print('the angel afa is: %.1f '%(afa*180/pi))  ##gnew=g'*sin(afa)
print('Uline max is %.2f cm/s'%(Umax*100))
print('Uline min is %.2f cm/s'%(Umin*100))


# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html