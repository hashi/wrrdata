

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 10:39:33 2020

@author: haoran
"""

"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
H=0.643
H0=0.08
L=2
W=2
u0=0.075
Q0=H0*W*u0

sol = '../'
loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables0'

timename = '210'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')  

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

#import griddata from scipy package

# Number of division for linear interpolation
ngridy = 1200
ngridz = 643
# Interpolation grid dimensions
yinterpmin = -6
yinterpmax = 6
zinterpmin = -0.643
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)

Ep=np.zeros(60)
xc=np.zeros(60)
UA=np.zeros(60)
for n in range (1,61):
    xc[n-1]=0.05*n
    Xplane=0.05*n
    H=-Xplane*tan(8/180*pi)-H0
    dx=0.02
    Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))
    if interpolate==1:
         # Interpolation of scalra fields and vector field components
          
       Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')
       T_i = griddata((y[Iplane], z[Iplane]), np.transpose(T[Iplane]), (yinterp, zinterp), method='linear')
    u=np.zeros((643,1200))
    count=0
    for i in range (0,643):
         for j in range (0,1200):
             if   (float(T_i[i][j])>0.2*8e-4):
                u[i][j]=float(Ux_i[i][j])
                count=count+1
             else:
                u[i][j]=0
    Q=0
    for i in range (0,643):
        for j in range (0,1200):
             Q+=u[i][j]*12/1200*0.643/643
    Ep[n-1]=(Q-Q0)/(Q0)
    UA[n-1]=Q/count/(12/1200*0.643/643)
    print(n)
plt.plot(xc, Ep)


timename = '240'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')  

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

#import griddata from scipy package

# Number of division for linear interpolation
ngridy = 1200
ngridz = 643
# Interpolation grid dimensions
yinterpmin = -6
yinterpmax = 6
zinterpmin = -0.643
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)

Ep=np.zeros(60)
xc=np.zeros(60)
UA=np.zeros(60)
for n in range (1,61):
    xc[n-1]=0.05*n
    Xplane=0.05*n
    H=-Xplane*tan(8/180*pi)-H0
    dx=0.02
    Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))
    if interpolate==1:
         # Interpolation of scalra fields and vector field components
          
       Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')
       T_i = griddata((y[Iplane], z[Iplane]), np.transpose(T[Iplane]), (yinterp, zinterp), method='linear')
    u=np.zeros((643,1200))
    count=0
    for i in range (0,643):
         for j in range (0,1200):
             if   (float(T_i[i][j])>0.2*8e-4):
                u[i][j]=float(Ux_i[i][j])
                count=count+1
             else:
                u[i][j]=0
    Q=0
    for i in range (0,643):
        for j in range (0,1200):
             Q+=u[i][j]*12/1200*0.643/643
    Ep[n-1]=(Q-Q0)/(Q0)
    UA[n-1]=Q/count/(12/1200*0.643/643)
    print(n)
plt.plot(xc, Ep)



plt.legend(bbox_to_anchor=(1.05,0),loc=3,borderaxespad=0)
plt.xlim(0,2)
plt.ylim(0,1)
plt.xlabel('x(m)')
plt.ylabel('${E}$')


