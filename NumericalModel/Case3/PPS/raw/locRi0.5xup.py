"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '240'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

Xplane=0.75
H0=0.08
H=-Xplane*tan(8/180*pi)-H0
dx=0.001
# Number of division for linear interpolation
ngridy = 401
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -2
yinterpmax = 2
zinterpmin = H
zinterpmax = -0.01

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

dz=zi[1]-zi[0]
# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)




Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
    Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')


Tc=np.zeros((ngridz,ngridy))
Uc=np.zeros((ngridz,ngridy))
Vc=np.zeros((ngridz,ngridy))
Wc=np.zeros((ngridz,ngridy))
# Plots the contour of sediment concentration
for i in range (0,ngridz):
    for j in range (0,ngridy):
        if str(T_i[i][j])=='nan':
            Tc[i][j]=0
        else:
            Tc[i][j]=T_i[i][j]
for i in range (0,ngridz):
    for j in range (0,ngridy):
        a=Ux_i[i][j]
        if str(a[0])=='nan':
            Uc[i][j]=0
        else:
            Uc[i][j]=Ux_i[i][j]


    

gradT=np.zeros((ngridz,ngridy))
gradU=np.zeros((ngridz,ngridy))

for i in range (0,ngridy):
    #for j in range (0,ngridz-1):
        #gradT[j][i]=(Tc[j+1][i]-Tc[j][i])/dz
         gradT[:,i]=np.gradient(Tc[:,i])/dz
         gradU[:,i]=np.gradient(Uc[:,i])/dz
         
########################
##plot the gradient of T
########################
map_reversed = matplotlib.cm.get_cmap('Reds')    
plt.figure(figsize=(10, 3))
levels = np.arange(-0.015,0.015,0.001)     
a=plt.contourf(yinterp, zinterp, gradT,  cmap='coolwarm',levels=levels)   
plt.ylim(H+0.01,-0.01)


########################
##plot buoyant frequency N2
########################
  
plt.figure(figsize=(10, 3))
levels = np.arange(-0.1,0.,0.001)   
N2=-gradT*9.81  
a=plt.contourf(yinterp, zinterp, N2,  cmap='jet',levels=levels)   
plt.colorbar(a,ticks=np.arange(-0.1,0.,0.01),aspect=7.5,label='${N}^{2}$')

plt.ylim(H+0.01,-0.01)
plt.xlabel('y (m)')
plt.ylabel('z (m)')



########################
##plot the gradient of U
########################

plt.figure(figsize=(10, 3))
levels = np.arange(-1,1,0.1)     
a=plt.contourf(yinterp, zinterp, gradU,  cmap='coolwarm',levels=levels)   
plt.ylim(H+0.01,-0.01)


########################
##plot Ri
########################
plt.figure(figsize=(10, 2),dpi=300)
plt.rcParams.update({'font.size': 12,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :12,
}

Ri=np.zeros((ngridz,ngridy))

for i in range (0,ngridz):
    for j in range (0,ngridy):
        Ri[i][j]=-9.81*gradT[i][j]/((gradU[i][j])**2)*1/(1+Tc[i][j])

levels = np.arange(-3.2,0.01,0.1)     
a=plt.contourf(yinterp, zinterp, log(Ri),  cmap='hot',levels=levels)   
plt.colorbar(a,ticks=np.arange(-4,0.01,1),aspect=7.5,label='$log(Ri)$')
#cs=plt.contour(yinterp,zinterp,Tc/8e-4,[0.0001])
levels=np.arange(-100,0,1)     
#plt.contourf(yinterp, zinterp, (Ri),  cmap='Blues',levels=levels)   
cs=plt.contour(yinterp,zinterp,Tc/8e-4,[0.2],color='black')
plt.xlim(2,-2)
#cs=plt.contour(yinterp,zinterp,Tc/8e-4,[0.99])
plt.ylim(-0.18,-0.01)
plt.xlabel('$y$ ($m$)')
plt.ylabel('$z$ ($m$)')
plt.yticks([-0.18,-0.09,0])
plt.text(1.95,-0.03,'(b)')
#plt.text(-1.98,-0.03,'$x=0.5{x}_{up}$')

