"""
Un-complete calculation. This code reads the data in a cross-section, and calculate the interface
thickness based on RDD=0.5RDD0 countour. 

"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar,  readfield
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '210'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean_w30U')
   
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

Xplane=0.1
H0=0.08
H=-Xplane*tan(8/180*pi)-H0
dx=0.01
# Number of division for linear interpolation
ngridy = 200
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -2
yinterpmax = 2
zinterpmin = H
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)




Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components    
 
    Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')
 
###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

# Plots the contour of U
Ut=np.zeros((100,200))
for i in range (0,100):
    for j in range (0,200):
      c=Ux_i[i][j]
      Ut[i][j]=c[0]

levels = np.arange(-0.1, 1.11,0.001)     
plt.figure(figsize=(10, 2),dpi=300)
a=plt.contourf(yinterp, zinterp, Ut/0.075, cmap='jet', levels=levels)
plt.colorbar(a,ticks=np.arange(-0.1,1.11,0.6),aspect=8,label='$U/U_{0}$')
plt.ylim(H,0)
plt.xlim(2,-2)
plt.xlabel('$y$(m)',fontsize=15)
plt.ylabel('$z$(m)',fontsize=15)
plt.show()
fsur=array([-0.45318327,  1.05518559,  0.05423405, -1.00446622])
psur=np.poly1d(fsur)

ysur=psur(Xplane)

fread = (pd.read_csv("./sideinterface/f%dcm.csv"%(Xplane*100), header = None)).values
f=np.zeros(4)

for i in range (0,4):
    f[i]=fread[i][0]
p = np.poly1d(f)

f_grad=np.zeros(3)

for i in range (0,3):
    f_grad[i]=(3-i)*f[i]   ### function dy/dx ----> x  meaning that afa=arctan(fc_grad(x))
p_grad = np.poly1d(f_grad)



zpoint=-0.02
ys=np.linspace((ysur+0.01),-1.5,1000)
zs=p(ys)
for i in range (0,1000):
    if zs[i]<=-0.02:
        yc=ys[i]
        zc=zs[i]
        break
print('the reference point is:')
print(yc,zc)

afa=arctan(1/p_grad(yc))

#reference point in new mesh
yo=yc*cos(afa)-zc*sin(afa)
zo=zc*cos(afa)+yc*sin(afa)

print('the crossover point is adopted into:')
print(yo,zo)

print('the angel is: ')  ##gnew=g'*sin(afa)
print(afa*180/pi)

##adapt the coordinates 
yall=np.zeros(int(len(yinterp)*len(yinterp[0])))
zall=np.zeros(int(len(zinterp)*len(zinterp[0]))) 
Uall=np.zeros(int(len(Ut)*len(Ut[0]))) 
num=0    
for i in range (0,len(yinterp)):
    for j in range (0,len(yinterp[0])):
        yall[num]=yinterp[i][j]
        zall[num]=zinterp[i][j]
        Uall[num]=Ut[i][j]
        num=num+1


ycl=yall*cos(afa)-zall*sin(afa)
zcl=zall*cos(afa)+yall*sin(afa)

####rebuild mesh (interpolation)

def changecoordinate (y,z):
    ynew=y*cos(afa)-z*sin(afa)
    znew=z*cos(afa)+y*sin(afa)
    return (ynew,znew)

########################################################
#calculate the adapted coordinates for the four boundary 
#points
########################################################

leftup=changecoordinate(2,0)
leftdown=changecoordinate(2,H)
rightup=changecoordinate(-2,0)
rightdown=changecoordinate(-2,H)

yboun=np.zeros(4)
yboun[0]=leftup[0]
yboun[1]=leftdown[0]
yboun[2]=rightup[0]
yboun[3]=rightdown[0]

zboun=np.zeros(4)
zboun[0]=leftup[1]
zboun[1]=leftdown[1]
zboun[2]=rightup[1]
zboun[3]=rightdown[1]

#######
print('max and mim ynew are:')
print(max(yboun),min(yboun))
print('max and mim znew are:')
print(max(zboun),min(zboun))

print('range of ynew is:')
print(max(yboun)-min(yboun))

print('range of znew is:')
print(max(zboun)-min(zboun))

ngridycl = 600
ngridzcl = 400

yclinterpmax= max(yboun)
yclinterpmin = min(yboun)
zclinterpmin = min(zboun)
zclinterpmax = max(zboun)

ycli = np.linspace(yclinterpmin, yclinterpmax, ngridycl)
zcli = np.linspace(zclinterpmin, zclinterpmax, ngridzcl)

# Structured grid creation
yclinterp, zclinterp = np.meshgrid(ycli, zcli)

Utcli = griddata((ycl, zcl), Uall, (yclinterp, zclinterp), method='linear')
####find the reference line (perpendicular to the interface)

a=plt.contourf(yclinterp, zclinterp, Utcli, cmap='jet')
plt.show()

err=1
for i in range (0,len(zcli)):
    if abs(zcli[i]-zo)<err:
        err=abs(zcli[i]-zo)
        m=i


#####get usful data

yline=[]
Uline=[]
for i in range (0,len(ycli)):
    fillU=[0]
    filly=[0]
    if str(Utcli[m][i])!='nan':
        fillU[0]=Utcli[m][i]
        filly[0]=ycli[i]
        yline=yline+filly
        Uline=Uline+fillU
plt.scatter(yline,Uline)      

detaU=max(Uline)-min(Uline)
normalU=Uline/detaU   ###normalised velocity U/detaU
for i in range (0,len(Uline)):
    if normalU[i]>=0.15:
        ylinelow=yline[i]
        plt.scatter(ylinelow,Uline[i],color='red',s=50)
        break
for i in range (0,len(Uline)):
    if normalU[i]>=0.85:
        ylinehigh=yline[i]
        plt.scatter(ylinehigh,Uline[i],color='orange',s=50)
        break
    
cita=(ylinehigh-ylinelow)/(0.85-0.15)  
print('interface thickness is:')
print(cita)  
plt.show()


######define time-serise of ucl and vcl

    
    




