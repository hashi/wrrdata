"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from pylab import *
#from pylab import *
#import matplotlib.gridspec as gridspec


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '250'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w70U')
    T = readscalar(sol, timename, 'TMean_w70T')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 401
ngridz = 80

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 4
zinterpmin = -0.642
zinterpmax = 0

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
xinterp, zinterp = np.meshgrid(xi, zi)

Yplane=0
dy=0.01

Iplane=np.where(np.logical_and(y>=Yplane-dy,y<=Yplane+dy))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], z[Iplane]), T[Iplane], (xinterp, zinterp), method='linear')
    Ux_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (xinterp, zinterp), method='linear')
    Uz_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (xinterp, zinterp), method='linear')



Ut=np.zeros((80,401))
for i in range (0,80):
    for j in range (0,401):
      c=Ux_i[i][j]
      if str(c)=='[nan]' or zinterp[i][j]<(-0.08-xinterp[i][j]*tan(8/180*pi)):
          Ut[i][j]=0
          
      else:
          Ut[i][j]=c
###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

#find boundary of plunge region and underflow region based on U
XprofileList2=np.arange(0,400,1)
m=0

Us=np.zeros((400,80))

detaz=0.642/80


Um=np.zeros(400)
qc=np.zeros(400)
Rm=np.zeros(400)
Hs=np.zeros(400)
xs=np.zeros(400)
for i in range (0,400):
    Um[i]=0
    Rm[i]=0
    count=0
    Ut[79][i]=Ut[78][i]
    T_i[79][i]=T_i[78][i]
    for j in range (0,80):
        if zinterp[j][i]>=(-0.08-xinterp[j][i]*tan(8/180*pi)) and Ut[j][i]>0:
            Um[i]=Um[i]+Ut[j][i]
            Rm[i]=Rm[i]+T_i[j][i]
            count+=1
            '''
            if Ut[j][i]<0:
                break
        '''
    qc[i]=Um[i]*detaz
    Um[i]=Um[i]/count
    Rm[i]=Rm[i]/count
    xs[i]=0.01*i
    Hs[i]=xs[i]*tan(8/180*pi)+0.08

    
    

plt.plot(xs/1.4,qc/0.075/0.08)
plt.xlim(0,1.2)
plt.ylim(0.5,3)
plt.xlabel('$x/{x}_{up}$')
plt.ylabel('$q_{c}/q_{c0}$')


plt.plot(xs/1.4,Um/0.075)
plt.xlim(0,1.2)
plt.ylim(0.5,1)
plt.xlabel('$x/{x}_{up}$')
plt.ylabel('$q_{c}/q_{c0}$')


import pandas as pd
pd.DataFrame(xs).to_csv("../../comUc/Frd3/nonx.csv", header=None,index=None)

pd.DataFrame(qc/0.075/0.08).to_csv("../../comUc/Frd3/qc.csv", header=None,index=None)

pd.DataFrame(Um/0.075).to_csv("../../comUc/Frd3/Uc.csv", header=None,index=None)

pd.DataFrame(Rm*9.81).to_csv("../../comUc/Frd3/gc.csv", header=None,index=None)


plt.plot(xs,Rm)
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html