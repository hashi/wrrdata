"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '250'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    curl=  readvector(sol, timename, 'vorticity_1s')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

Xplane=0.75
H0=0.08
H=-Xplane*tan(8/180*pi)-H0
dx=0.01
# Number of division for linear interpolation
ngridy = 401
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -2
yinterpmax = 2
zinterpmin = H
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)




Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    curl_i = griddata((y[Iplane], z[Iplane]), np.transpose(curl[0, Iplane]), (yinterp, zinterp), method='linear')
    curl_j = griddata((y[Iplane], z[Iplane]), np.transpose(curl[1, Iplane]), (yinterp, zinterp), method='linear')
    curl_k = griddata((y[Iplane], z[Iplane]), np.transpose(curl[2, Iplane]), (yinterp, zinterp), method='linear')
    
    CURLi=np.zeros((len(curl_i),len(curl_i[0])))
    for i in range (0,len(curl_i)):
        for j in range (0,len(curl_i[0])):
            CURLi[i][j]=curl_i[i][j][0]
    CURLj=np.zeros((len(curl_j),len(curl_j[0])))
    for i in range (0,len(curl_j)):
        for j in range (0,len(curl_j[0])):
            CURLj[i][j]=curl_j[i][j][0]
    CURLk=np.zeros((len(curl_k),len(curl_k[0])))
    for i in range (0,len(curl_k)):
        for j in range (0,len(curl_k[0])):
            CURLk[i][j]=curl_k[i][j][0]
###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


# Plots the contour of sediment concentration

levels = np.arange(-1, 1.02, 0.01)
plt.figure(figsize=(10, 3),dpi=300)
plt.rcParams.update({'font.size': 12,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :12,
}

plt.figure(figsize=(10, 2),dpi=300)
a=plt.contourf(yinterp, zinterp, CURLi, cmap='coolwarm', levels=levels,extend='both')
plt.colorbar(a,ticks=np.arange(-1,1.01,0.5),aspect=10,label='${\u03C9}_{i}$ (${s}^{-1}$)')
# Calculation of the streamline width as a function of the velociy magnitude
plt.ylim(-0.18,0)
plt.xlim(2,-2)
plt.xlabel('${y}$($m$)')
plt.ylabel('${z}$($m$)')
plt.text(1.95,-0.03,'(d)')
#plt.plot([0,0],[H,-0.005],'-.',color='blue',linewidth=2)
#plt.plot([0.5,0.5],[H,-0.005],'-.',color='green',linewidth=2)
#plt.plot([1,1],[H,-0.005],'-.',color='orange',linewidth=2)
plt.yticks([-0.18,-0.09,0])
plt.show()



levels = np.arange(-1, 1.02, 0.01)
plt.figure(figsize=(10, 3),dpi=300)
plt.rcParams.update({'font.size': 12,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :12,
}


a=plt.contourf(yinterp, zinterp, CURLj, cmap='coolwarm', levels=levels,extend='both')
plt.colorbar(a,ticks=np.arange(-1,1.01,0.5),label='${\u03C9}_{j}$ (${s}^{-1}$)')
# Calculation of the streamline width as a function of the velociy magnitude
plt.ylim(-0.18,0)
plt.xlim(2,-2)
plt.xlabel('${y}$($m$)')
plt.ylabel('${z}$($m$)')
plt.text(1.95,-0.03,'(a)')
#plt.plot([0,0],[H,-0.005],'-.',color='blue',linewidth=2)
#plt.plot([0.5,0.5],[H,-0.005],'-.',color='green',linewidth=2)
#plt.plot([1,1],[H,-0.005],'-.',color='orange',linewidth=2)
plt.yticks([-0.18,-0.09,0])
plt.show()





levels = np.arange(-1, 1.02, 0.01)
plt.figure(figsize=(10, 2),dpi=300)
plt.rcParams.update({'font.size': 12,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :12,
}


a=plt.contourf(yinterp, zinterp, CURLk, cmap='coolwarm', levels=levels,extend='both')
plt.colorbar(a,ticks=np.arange(-1,1.01,0.5),aspect=10,label='${\u03C9}_{k}$ (${s}^{-1}$)')
# Calculation of the streamline width as a function of the velociy magnitude
plt.ylim(-0.18,0)
plt.xlim(2,-2)
plt.xlabel('${y}$($m$)')
plt.ylabel('${z}$($m$)')
plt.text(1.95,-0.03,'(a)')
#plt.plot([0,0],[H,-0.005],'-.',color='blue',linewidth=2)
#plt.plot([0.5,0.5],[H,-0.005],'-.',color='green',linewidth=2)
#plt.plot([1,1],[H,-0.005],'-.',color='orange',linewidth=2)
plt.yticks([-0.18,-0.09,0])
plt.show()



