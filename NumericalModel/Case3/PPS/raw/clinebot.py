"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd
sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1
afa=8/180*pi
H0=0.08
if loadMesh==1:
   
    x, y, z = readmesh(sol)
    xcl=x*cos(afa)-z*sin(afa)
    ycl=y
    zcl=x*sin(afa)+z*cos(afa)+H0*cos(afa)
###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '300'
if loadData==1:   
    T = readscalar(sol, timename, 'TMean_w90T')
    U = readvector(sol, timename, 'UMean_w90U')
    #T = readscalar(sol, timename, 'T')  
    
    

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 301
ngridy = 400

# Interpolation grid dimensions
xclinterpmin = 0
xclinterpmax = 3
yclinterpmin = -2
yclinterpmax = 2


# Interpolation grid
xcli = np.linspace(xclinterpmin, xclinterpmax, ngridx)
ycli = np.linspace(yclinterpmin, yclinterpmax, ngridy)

# Structured grid creation
xclinterp, yclinterp = np.meshgrid(xcli, ycli)

Zclvert=0.04
dzcl=0.005

Iplane=np.where(np.logical_and(zcl>=Zclvert-dzcl,zcl<=Zclvert+dzcl))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((xcl[Iplane], ycl[Iplane]), T[Iplane], (xclinterp, yclinterp), method='linear')
    U_i = griddata((xcl[Iplane], ycl[Iplane]), np.transpose(U[0, Iplane]), (xclinterp, yclinterp), method='linear')
    V_i = griddata((xcl[Iplane], ycl[Iplane]), np.transpose(U[1, Iplane]), (xclinterp, yclinterp), method='linear')
###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

# Define plot parameters
fig = plt.figure(figsize=(4, 2), dpi=100)



# Plots the contour of sediment concentration
levels = np.arange(0, 1.02, 0.01)
a=plt.contourf(yclinterp, xclinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),label='RDD')
cs=plt.contour(yclinterp, xclinterp, T_i/(8e-4),[0.1],colors='k')

plt.show()


fig = plt.figure(figsize=(4, 2), dpi=300)

U=np.zeros((len(U_i),len(U_i[0])))

for i in range (0,len(U)):
    for j in range (0,len(U[0])):
        U[i][j]=float(U_i[i][j])


V=np.zeros((len(U_i),len(U_i[1])))

for i in range (0,len(V)):
    for j in range (0,len(V[0])):
        V[i][j]=float(V_i[i][j])

pd.DataFrame(xclinterp[0]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/I_12/DATA_PLOT/y=0/NUM/x.csv", header=None,index=None)
pd.DataFrame(U[200]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/I_12/DATA_PLOT/y=0/NUM/U.csv", header=None,index=None)

pd.DataFrame(yclinterp[:,52]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/I_12/DATA_PLOT/x=0.35xup/NUM/y.csv", header=None,index=None)
pd.DataFrame(U[:,52]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/I_12/DATA_PLOT/x=0.35xup/NUM/U.csv", header=None,index=None)

pd.DataFrame(yclinterp[:,112]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/I_12/DATA_PLOT/x=0.7xup/NUM/y.csv", header=None,index=None)
pd.DataFrame(U[:,112]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/I_12/DATA_PLOT/x=0.7xup/NUM/U.csv", header=None,index=None)
pd.DataFrame(V[:,112]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/I_12/DATA_PLOT/x=0.7xup/NUM/V.csv", header=None,index=None)



# Plots the contour of sediment concentration
def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys

for i in range (0,len(U)):
    U[i]=smooth(U[i],5)
    V[i]=smooth(V[i],5)
    
plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}
fig = plt.figure(figsize=(5.9,1.9),dpi=500)
levels = np.arange(-0.2, 1.21, 0.01)
a=plt.contourf(xclinterp/2, yclinterp/2, U/0.075, cmap='rainbow',levels=levels)

q=plt.quiver(xcli[11::17]/2, ycli[2::17]/2, U[2::17,11::17], V[2::17,11::17],width=0.006,scale=1.5,headwidth=2)
#quiverkey(q, X=0.8, Y=1.06, U=0.075,
         #   label='= ${U}_{0}$', labelpos='E')


#plt.colorbar(a,ticks=np.arange(0,1.01,0.25),label=r'$\overline{{u}_{s}}$/${U}_{0}$')

plt.plot([0.74/2,0.74/2],[0,1.5/2],'-.',color='black',linewidth=1.5)
plt.plot([1.48/2,1.48/2],[0,1.5/2],'-.',color='black',linewidth=1.5)
plt.plot([1.97/2,1.97/2],[0,1.5/2],'-.',color='black',linewidth=1.5)

plt.text(0.34,1.6/2,'0.5${x}_{up}$')
plt.text(1.45/2,1.6/2,'${x}_{up}$')
plt.text(1.95/2,1.6/2,'${x}_{ud}$')
plt.xlim(0.1/2,3/2)
plt.ylim(0,1.5/2)
plt.yticks([0,0.5/2,1/2,1.5/2],fontsize=10)
plt.xticks([])
plt.show()




# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html