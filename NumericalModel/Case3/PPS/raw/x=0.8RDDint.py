"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '210'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

Xplane=0.8
H0=0.08
H=-Xplane*tan(8/180*pi)-H0
dx=0.01
# Number of division for linear interpolation
ngridy = 401
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -2
yinterpmax = 2
zinterpmin = H
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)




Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
    Uy_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[1, Iplane]), (yinterp, zinterp), method='linear')
    Uz_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (yinterp, zinterp), method='linear')
    Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')

###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


# Plots the contour of sediment concentration

levels = np.arange(0, 1.02, 0.01)
plt.figure(figsize=(10, 3),dpi=300)
plt.rcParams.update({'font.size': 22,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :22,
}


a=plt.contourf(yinterp, zinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
cs=plt.contour(yinterp, zinterp, T_i/(8e-4), [0.5])
p1=cs.collections[0].get_paths()[0]
coor_p1=p1.vertices
a=coor_p1
az=a[:,1]
ay=a[:,0]

p2=cs.collections[0].get_paths()[1]
coor_p2=p2.vertices
a2=coor_p2
az2=a2[:,1]
ay2=a2[:,0]


# Calculation of the streamline width as a function of the velociy magnitude
plt.ylim(H,0)
plt.xlim(2,-2)
plt.xlabel('${y}$(m)')
plt.ylabel('${z}$(m)')
'''
plt.text(1.95,-0.03,'(c)')
plt.plot([0,0],[H,-0.005],'-.',color='blue',linewidth=2)
plt.plot([0.5,0.5],[H,-0.005],'-.',color='green',linewidth=2)
plt.plot([1,1],[H,-0.005],'-.',color='orange',linewidth=2)
plt.yticks([-0.2,-0.1,0])
'''
plt.show()


plt.plot(ay,az)

plt.plot(ay2,az2)

pd.DataFrame(ay).to_csv("/media/haoran/DATA/4mcases/controlFrd/interf/case3/y.csv", header=None,index=None)
pd.DataFrame(az).to_csv("/media/haoran/DATA/4mcases/controlFrd/interf/case3/z.csv", header=None,index=None)

pd.DataFrame(ay2).to_csv("/media/haoran/DATA/4mcases/controlFrd/interf/case3/y2.csv", header=None,index=None)
pd.DataFrame(az2).to_csv("/media/haoran/DATA/4mcases/controlFrd/interf/case3/z2.csv", header=None,index=None)
    
    




