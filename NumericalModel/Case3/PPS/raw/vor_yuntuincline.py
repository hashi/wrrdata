"""
This code is used to plot T-ynew-Utang contour

"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar, readfield
import matplotlib.pyplot as plt
import pandas as pd
from pylab import *


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)
 

timename = '210'
if loadData==1:    
    Tensor=readfield(sol, timename, 'UPrime2Mean_w30U')
    uu=Tensor[0]
    vv=Tensor[3]
    K=uu+vv      ###turbulent kinetic energy


# Number of division for linear interpolation
ngridx = 200
ngridy = 300

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 2
yinterpmin = -1.2
yinterpmax =  1.2

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.035
dz=0.02

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    K_i = griddata((x[Iplane], y[Iplane]), K[Iplane], (xinterp, yinterp), method='linear')


plt.rcParams.update({'font.size': 9,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :9,
}
fig = plt.figure(figsize=(3,3),dpi=500)
levels = np.arange(0,15.01,0.01)
a=plt.contourf(xinterp,yinterp,K_i*10000,cmap='coolwarm',levels=levels)
plt.colorbar(a,ticks=np.arange(0,15.01,5),label='${K}$ (${cm}^{2}$ ${s}^{-2}$)')
plt.xlabel('2x/B')
plt.xlim(0,2)
plt.ylabel('2y/B')
plt.ylim(-1.2,1.2)

######plot numerical triangle
fc_num=np.array([0.187,-1.02,1.371,0.288,-1.017])
pc_num = np.poly1d(fc_num)
x_num=np.linspace(0.1,1.8,100)
y_num=pc_num(x_num)
plt.plot(x_num,y_num,'-.',color='black')
plt.plot(x_num,-y_num,'-.',color='black',label='NUM')
###############################################


def findmaxK (c,n,deta):   # c: starting point; n: number of points; deta: distance between points
    #n=25   c=15   deta=5
    jmax=np.zeros(n)
    maxK=np.zeros(n)
    for i in range (0,n):
        for j in range (0,len(K_i[:,int(i*deta+c)])):  #"15,20,25,30,35,40,45,50,55,60..."
            if yinterp[j][int(i*deta+c)]<0:
                if K_i[j][int(i*deta+c)]>maxK[i]:
                    maxK[i]=K_i[j][int(i*deta+c)]
                    jmax[i]=j                    
    for i in range (0,n):
        plt.scatter(xinterp[0][int(i*deta+c)],yinterp[int(jmax[i])][0],color='black',s=1)  
    x_1=np.zeros(n)
    y_1=np.zeros(n)
    for i in range (0,n):
        x_1[i]=xinterp[0][int(i*deta+c)]
        y_1[i]=yinterp[int(jmax[i])][0]
    f1=np.polyfit(x_1,y_1,4)
    p1 = np.poly1d(f1)
    yvals = p1(x_1) 
    plt.plot(x_1,yvals,color='black')
    plt.plot(x_1,-yvals,color='black')
    plt.show()
    return (f1)

fc=findmaxK (15,28,5)    ##fcurve shows maxuv fc[0]*x^4+fc[1]*x^3+fc[2]*x^2+fc[3]*x+fc[4]
pc = np.poly1d(fc)
fc_grad=np.zeros(4)

for i in range (0,4):
    fc_grad[i]=(4-i)*fc[i]   ### function dy/dx ----> x  meaning that afa=arctan(fc_grad(x))
pc_grad = np.poly1d(fc_grad)

############################################################################
x_get=0.6   ## set an x first, find the crossover point with maxuv curve
############################################################################

####crossover point
print('the crossover point is:')
print(x_get,pc(x_get))
afa=arctan(pc_grad(x_get))


#reference point in new mesh
xo=x_get*cos(afa)+pc(x_get)*sin(afa)
yo=-x_get*sin(afa)+pc(x_get)*cos(afa)

print('the crossover point is adopted into:')
print(xo,yo)

##adapt the coordinates 
xall=np.zeros(int(len(xinterp)*len(xinterp[0])))
yall=np.zeros(int(len(yinterp)*len(yinterp[0])))  
num=0    
for i in range (0,len(xinterp)):
    for j in range (0,len(xinterp[0])):
        xall[num]=xinterp[i][j]
        yall[num]=yinterp[i][j]
        num=num+1


xcl=xall*cos(afa)+yall*sin(afa)
ycl=-xall*sin(afa)+yall*cos(afa) 


####rebuild mesh (interpolation)

ngridycl = 300
ngridxcl = 400

yclinterpmax= -0.2
xclinterpmin = -0.5
yclinterpmin = -1.5
xclinterpmax = 1.5

ycli = np.linspace(yclinterpmin, yclinterpmax, ngridycl)
xcli = np.linspace(xclinterpmin, xclinterpmax, ngridxcl)

# Structured grid creation
xclinterp, yclinterp = np.meshgrid(xcli, ycli)

####find the reference line (perpendicular to the triangle curve)

err=1
for i in range (0,len(xcli)):
    if abs(xcli[i]-xo)<err:
        err=abs(xcli[i]-xo)
        m=i
        '''
    uline=Ucl_i[:,m]
    vline=Vcl_i[:,m] 
    yline=ycli
'''


######define time-serise of ucl and vcl

##########################################################
numT=61    ###0.5s 1s 1.5s .....   
#########################################################
uclTline=np.zeros((numT,ngridycl))
vclTline=np.zeros((numT,ngridycl))

#for noT in range (0,1):
def linevalue(noT):   ###No.T
    print(noT)    
    sol = '../'
    timename = '%d'%(120+noT)
    U = readvector(sol, timename, 'U')
    T = readscalar(sol, timename, 'T')

    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')
    u = griddata((x[Iplane], y[Iplane]), np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')
    v = griddata((x[Iplane], y[Iplane]), np.transpose(U[1, Iplane]), (xinterp, yinterp), method='linear')
 
    uall=np.zeros(int(len(u)*len(u[0])))
    vall=np.zeros(int(len(v)*len(v[0])))
    num=0
    for i in range (0,len(u)):
        for j in range (0,len(u[0])):
            uall[num]=u[i][j]
            vall[num]=v[i][j]
            num=num+1
    uclall=uall*cos(afa)+vall*sin(afa)
    vclall=-uall*sin(afa)+vall*cos(afa)
    ucl_i = griddata((xcl, ycl), uclall, (xclinterp, yclinterp), method='linear')
    vcl_i = griddata((xcl, ycl), vclall, (xclinterp, yclinterp), method='linear')
    for i in range (0,ngridycl):
        uclTline[noT][i]=ucl_i[i][m]
        vclTline[noT][i]=vcl_i[i][m]  
    
#####main
for i in range (0,numT):
    linevalue(i)
  

T=np.linspace(120,180,61)    
ycli,T=np.meshgrid(ycli,T)

fig = plt.figure(figsize=(5, 2), dpi=100)    
levels= np.arange(-2.1, 6.01,0.1)  
a=plt.contourf(T,ycli*100,uclTline*100,cmap='jet',levels=levels)
#plt.contour(T,ycli,uclTline,[7.45*0.5],color='black')
plt.xlabel('T (s)')
plt.ylabel('${y}_{new}$ (cm)')
plt.colorbar(a,ticks=np.arange(-2,6.1,2),label='${U}_{tang}$ (cm/s)')
plt.ylim((yo*100-10),(yo*100+10))
plt.xlim(120,165)   

fig = plt.figure(figsize=(5, 2), dpi=100)    
levels= np.arange(-5, 3.01,0.1)  
a=plt.contourf(T,ycli*100,vclTline*100,cmap='jet',levels=levels)
plt.contour(T,ycli*100,vclTline*100,[0],color='black')
plt.xlabel('T (s)')
plt.ylabel('${y}_{new}$ (cm)')
plt.colorbar(a,ticks=np.arange(-5,3.1,4),label='${U}_{norm}$ (cm/s)')
plt.ylim((yo*100-10),(yo*100+10))
plt.xlim(120,165) 



        
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html