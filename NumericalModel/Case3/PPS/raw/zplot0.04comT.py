"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '300'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w90U')
    #T = readscalar(sol, timename, 'TMean_w30T')
    #U = readvector(sol, timename, 'U')
    T = readscalar(sol, timename, 'TMean_w90T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 200
ngridy = 401

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 4
yinterpmin = -4
yinterpmax =  4

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.04
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')
    Uy_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[1, Iplane]), (xinterp, yinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

# Define plot parameters
fig = plt.figure(figsize=(3, 3), dpi=300)
plt.rcParams.update({'font.size': 11})
plt.xlim(0,2)
plt.ylim(-1,1)
plt.xlabel('$x$ (m)')
plt.ylabel('$y$ (m)')

# Plots the contour of sediment concentration
levels = np.arange(0, 1.02, 0.01)
a=plt.contourf(xinterp, yinterp, T_i/8e-4, cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),aspect=15,label='$\overline{R}/{R}_{0}$')

    
#plt.plot(x1,y1,color="black",linewidth=2 )



plt.xlim(0,2)
plt.ylim(-1.2,1.2)
#plt.savefig('z=-0.04RDD.png', bbox_inches = 'tight')
plt.show()


#plot u

plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

Ut=np.zeros((401,200))
for i in range (0,401):
    for j in range (0,200):
      c=Ux_i[i][j]
      if str(c)=='[nan]':
          Ut[i][j]=0
      else:
          Ut[i][j]=c[0]



fig = plt.figure(figsize=(3, 3), dpi=500)
levels = np.arange(-0.11, 1.1,0.01)     
a=plt.contourf(xinterp, yinterp, Ut/0.075, cmap='rainbow', levels=levels)
plt.colorbar(a,ticks=np.arange(-0.1,1.1,0.4),aspect=15,label='${U}/{U}_{0}$')

plt.ylim(-1.2,1.2)
plt.xlim(0,2)
plt.xlabel('$x$ (m)')
plt.ylabel('$y$ (m)')



# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html