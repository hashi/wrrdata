

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 10:39:33 2020

@author: haoran
"""

"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
H=0.643
H0=0.08
L=2
W=2
u0=0.075
Q0=H0*W*u0

sol = '../'
loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables0'

timename = '210'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')  

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

#import griddata from scipy package

# Number of division for linear interpolation
ngridy = 1200
ngridz = 643
# Interpolation grid dimensions
yinterpmin = -6
yinterpmax = 6
zinterpmin = -0.643
zinterpmax = 0

detaz=0.643/643

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)

Ep=np.zeros(60)
xc=np.zeros(60)
UA=np.zeros(60)
for n in range (1,61):
    xc[n-1]=0.05*n
    Xplane=0.05*n
    H=-Xplane*tan(8/180*pi)-H0
    dx=0.02
    Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))
    if interpolate==1:
         # Interpolation of scalra fields and vector field components
          
       Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')
       T_i = griddata((y[Iplane], z[Iplane]), np.transpose(T[Iplane]), (yinterp, zinterp), method='linear')
    
    hy=np.zeros(1200)
    for j in range (0,1200):
        U2all=0
        Uall=0
        for i in range (0,643):
            if zinterp[i][j]<(-0.08-Xplane*tan(8/180*pi)) or str(Ux_i[i][j][0])=='nan':
                Ux_i[i][j]=0
            else:
                U2all=U2all+Ux_i[i][j][0]**2*detaz
                Uall=Uall+Ux_i[i][j][0]*detaz
        if Uall<=0:
            hy[j]=0
        else:
            hy[j]=Uall**2/U2all
    
    u=np.zeros((643,1200))
    count=0
    for j in range (0,1200):
        for i in range (0,643):
            if zinterp[i][j]<((-0.08-Xplane*tan(8/180*pi))+hy[j]):
                u[i][j]=float(Ux_i[i][j])
            else:
                u[i][j]=0
    Q=0
    for i in range (0,643):
        for j in range (0,1200):
             Q+=u[i][j]*12/1200*0.643/643
    Ep[n-1]=(Q-Q0)/(Q0)
    UA[n-1]=Q/count/(12/1200*0.643/643)
    print(n)
    levels = np.arange(0, 1.1,0.01) 
    plt.contourf(yinterp,zinterp,T_i/8e-4,levels=levels,cmap='Reds')
    plt.contour(yinterp,zinterp,T_i/8e-4,[0.2])
    
    plt.plot(yi,hy+(-0.08-Xplane*tan(8/180*pi)))
    plt.xlim(-6,6)
    plt.ylim((-0.08-Xplane*tan(8/180*pi)),0)
    plt.show()
plt.plot(xc, Ep)

plt.xlim(0,2)
plt.ylim(0,1)
plt.xlabel('x(m)')
plt.ylabel('${E}$')


import pandas as pd
Qs=Ep*Q0+Q0
pd.DataFrame(xc).to_csv("/media/haoran/DATA/4mcases/controlFrd/E/case3/x_hc.csv", header=None,index=None)
pd.DataFrame(Ep).to_csv("/media/haoran/DATA/4mcases/controlFrd/E/case3/Ep_hc.csv", header=None,index=None)
pd.DataFrame((Qs)).to_csv("/media/haoran/DATA/4mcases/controlFrd/E/case3/Q_hc.csv", header=None,index=None)
pd.DataFrame(UA).to_csv("/media/haoran/DATA/4mcases/controlFrd/E/case3/U_hc.csv", header=None,index=None)

