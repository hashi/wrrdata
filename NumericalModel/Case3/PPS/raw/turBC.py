"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar,  readfield
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '210'
if loadData==1:
   R = readfield(sol, timename, 'UPrime2Mean_w30U')
   U = readvector(sol, timename, 'UMean_w30U')
# Number of division for linear interpolation
ngridy = 200
ngridz = 100


# Interpolation grid dimensions
yinterpmin = -0.98
yinterpmax = 0.98
zinterpmin = -0.079
zinterpmax =  -0.005

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)


Xplane=-9

dx=0.01

Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    
    Rxx = griddata((y[Iplane], z[Iplane]), np.transpose(R[0, Iplane]), (yinterp, zinterp), method='linear')
    Rxy = griddata((y[Iplane], z[Iplane]), np.transpose(R[1, Iplane]), (yinterp, zinterp), method='linear')
    Rxz = griddata((y[Iplane], z[Iplane]), np.transpose(R[2, Iplane]), (yinterp, zinterp), method='linear')
    Ryy = griddata((y[Iplane], z[Iplane]), np.transpose(R[3, Iplane]), (yinterp, zinterp), method='linear')
    Ryz = griddata((y[Iplane], z[Iplane]), np.transpose(R[4, Iplane]), (yinterp, zinterp), method='linear')
    Rzz = griddata((y[Iplane], z[Iplane]), np.transpose(R[5, Iplane]), (yinterp, zinterp), method='linear')
    Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')




f="points"

with open (f,'w') as file:
    file.write('('+'\n')
    for j in range (0,19):  
       for i in range (0,len(zi)):
           file.write('(-5'+' '+'%.1f'%((j-9)*0.1)+' '+'%.3e)' %zi[i]+"\n")
    file.write(')')                 
   
f="U"

with open (f,'w') as file:
    file.write('('+'\n')
    for j in range (0,19):  
       for i in range (0,len(zi)):
           file.write('(%.3e 0 0)'%Ux_i[:,int(j*10+10),0][i]+'\n')
    file.write(')')  
f="R"

with open (f,'w') as file:
    file.write('('+'\n')
    for j in range (0,19):  
       for i in range (0,len(zi)):
           file.write('(%.3e' %Rxx[:,int(j*10+10),0][i]+' '+'%.3e' %Rxy[:,int(j*10+10),0][i]+' '+'%.3e' %Rxz[:,int(j*10+10),0][i]+' '+'%.3e'  %Ryy[:,int(j*10+10),0][i]+' '+'%.3e' %Ryz[:,int(j*10+10),0][i]+' '+'%.3e)' %Rzz[:,int(j*10+10),0][i]+'\n')
    file.write(')')
    







plt.plot(Rxx[50])
plt.xlim(25,175)
plt.ylim(0,3e-5)
plt.show()


plt.plot(Ryy[50])
plt.xlim(25,175)
plt.ylim(0,3e-5)
plt.show()

plt.plot(Rxy[50])
plt.xlim(25,175)
plt.ylim(0,5e-6)
plt.show()


