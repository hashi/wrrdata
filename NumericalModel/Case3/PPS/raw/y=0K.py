"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""
'''
This program is used to plot the time-averaged (30s) surface plane (z=0)
'''

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar, readfield
import matplotlib.pyplot as plt


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '280'
if loadData==1:
    
    Kcom = readfield(sol, timename, 'UPrime2Mean_w30U')/2
    k = readfield(sol, timename, 'k')
    
K=Kcom[0]+Kcom[3]+Kcom[5]
M=k/(K+k)



# Number of division for linear interpolation
ngridx = 400
ngridz = 80

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 4
zinterpmin = -0.65
zinterpmax = 0

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
xinterp, zinterp = np.meshgrid(xi, zi)

Yplane=0
dy=0.03

Iplane=np.where(np.logical_and(y>=Yplane-dy,y<=Yplane+dy))

if interpolate==1:
    # Interpolation of scalra fields and vector field components

    M_i = griddata((x[Iplane], z[Iplane]), M[Iplane], (xinterp, zinterp), method='linear')




# Define plot parameters
fig = plt.figure(figsize=(8, 1.5), dpi=200)
plt.rcParams.update({'font.size': 8,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :8,
}


plt.xlabel('${x}$(m)')
plt.ylabel('${z}$(m)')

# Plots the contour of sediment concentration
levels = np.arange(0, 1.01, 0.001)
a=plt.contourf(xinterp, zinterp, M_i, cmap=plt.cm.Reds, levels=levels)
cs=plt.contour(xinterp, zinterp, M_i, [0.2],colors="green")
plt.colorbar(a,ticks=np.arange(0,1.01,0.2),label='M')
plt.xlim(0,2.5)
plt.ylim(-2.5*np.tan(8/190*np.pi)-0.08,0)
        
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html