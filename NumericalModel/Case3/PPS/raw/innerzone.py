"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '250'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean_w70U')
    T = readscalar(sol, timename, 'TMean_w70T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

Xplane=1.6
H0=0.08
H=-Xplane*tan(8/180*pi)-H0+0.000
dx=0.01
# Number of division for linear interpolation
ngridy = 401
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -0.25
yinterpmax = 0.25
zinterpmin = H
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)




Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
    Uy_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[1, Iplane]), (yinterp, zinterp), method='linear')
    Uz_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (yinterp, zinterp), method='linear')
    Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')

###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


# Plots the contour of U
Ut=np.zeros((100,401))
for i in range (0,100):
    for j in range (0,401):
      c=Ux_i[i][j]
      if str(c)=='[nan]':
          Ut[i][j]=0
      else:
          Ut[i][j]=c[0]
print('U(min)= %f ' % Ut.min())
print('U(max)= %f ' % Ut.max())
levels = np.arange(-0.1, 1.11,0.001)     
plt.figure(figsize=(10, 3),dpi=300)
a=plt.contourf(yinterp, zinterp, Ut/0.075, cmap='jet', levels=levels)
plt.colorbar(a,ticks=np.arange(-0.1,1.11,0.6),aspect=8,label='$U/U_{0}$')
cs=plt.contour(yinterp, zinterp, Ut/0.075,[0.68])


plt.ylim(H,0)
plt.xlim(1.5,-1.5)
plt.xlabel('$y$(m)')
plt.ylabel('$z$(m)')

plt.show()


p1=cs.collections[0].get_paths()[0]
coor_p1=p1.vertices
a=coor_p1
ay=a[:,0]
az=a[:,1]
yisz=[]
zisz=[]
filly=[0]
fillz=[0]
for i in range (0,len(ay)):    
    if -0.9<=ay[i]<=0.9:
        filly[0]=ay[i]
        fillz[0]=az[i]
        yisz=yisz+filly
        zisz=zisz+fillz
plt.plot(yisz,zisz)        


pd.DataFrame(yisz).to_csv("/media/haoran/DATA/4mcases/inner3D/case3/y%d7cm.csv"%int(Xplane*100), header=None,index=None)
pd.DataFrame(zisz).to_csv("/media/haoran/DATA/4mcases/inner3D/case3/z%d7cm.csv"%int(Xplane*100), header=None,index=None)



    
    




