"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
import pandas as pd
from pylab import *
import matplotlib.gridspec as gridspec

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '180'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
   
  
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 200
ngridy = 400

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 4
yinterpmin = -4
yinterpmax =  4

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.16
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
   
    Ux_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')


#plot u
fig = plt.figure(figsize=(3, 4), dpi=300)
plt.rcParams.update({'font.size': 8,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :8,
}

Ut=np.zeros((400,200))
for i in range (0,400):
    for j in range (0,200):
      c=Ux_i[i][j]
      Ut[i][j]=c[0]
print('U(min)= %f ' % Ut.min())
print('U(max)= %f ' % Ut.max())
levels = np.arange(0, 0.081,0.001)     
a=plt.contourf(xinterp, yinterp, Ut, cmap='jet', levels=levels)
plt.colorbar(a,ticks=np.arange(-0.08,0.081,0.04),label='u(m ${s^{-1}}$)')

plt.ylim(-1.5,1.5)
plt.xlim(0,2)
plt.xlabel('${x}$(m)',font)
plt.ylabel('${y}$(m)',font)

pd.DataFrame(xinterp*100).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/NUM_slices/H_4/x.csv", header=None,index=None)
pd.DataFrame(yinterp*100).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/NUM_slices/H_4/y.csv", header=None,index=None)
pd.DataFrame(Ut*100).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/NUM_slices/H_4/U.csv", header=None,index=None)



# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html