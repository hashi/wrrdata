"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""
'''
This program is used to plot the time-averaged (30s) surface plane (z=0)
'''

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '240'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 200
ngridy = 300

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 2
yinterpmin = -1.5
yinterpmax =  1.5

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.04
dz=0.01

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')
    Uy_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[1, Iplane]), (xinterp, yinterp), method='linear')
    Uh_i=np.zeros((300,200))
    for i in range (0,300):
        for j in range (0,200):
            Uh_i[i][j] = (Ux_i[i][j]**2+Uy_i[i][j]**2)**0.5
            if str(Uh_i[i][j]) == 'nan':
                 Uh_i[i][j]=0
    

###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline
fig = plt.figure(figsize=(6, 6), dpi=100)
plt.rcParams.update({'font.size': 10})
XprofileList=np.arange(20,160,20)
for Xprofile in XprofileList:
    plt.subplot(311)
    plt.plot(yinterp[:,Xprofile], T_i[:,Xprofile],label=str(round(xinterp[0,Xprofile]*100)/100))
    plt.xlabel('y(m)')
    plt.ylabel('RDD')
    plt.legend()
    plt.subplot(312)
    plt.plot(yinterp[:,Xprofile], Ux_i[:,Xprofile,0])
    plt.plot([yinterp[0,Xprofile], yinterp[ngridy-1,Xprofile]], [0, 0],'--k')
    plt.xlabel('y(m)')
    plt.ylabel('U(m/s)')
    plt.subplot(313)
    plt.plot(yinterp[:,Xprofile], Uy_i[:,Xprofile,0])
    plt.xlabel('y(m)')
    plt.ylabel('V(m/s)')
#plt.savefig('profile.png', bbox_inches = 'tight')

# Define plot parameters
fig = plt.figure(figsize=(4, 8), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.xlabel('x(m)')
plt.ylabel('y(m)')

# Plots the contour of horizontal velocity
levels = np.arange(-0.01, 0.04, 0.001)
plt.contourf(xinterp, yinterp, Uh_i, cmap='coolwarm', levels=levels)



plt.show()

# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html