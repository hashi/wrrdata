"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '180'
if loadData==1:
    #U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 200
ngridy = 400

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 3
yinterpmin = -4
yinterpmax =  4

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=0
dz=0.01

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


#plot u
fig = plt.figure(figsize=(4, 2), dpi=300)
plt.rcParams.update({'font.size': 10.5,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :10.5,
}


levels = np.arange(-0, 1.02,0.01)     
a=plt.contourf(xinterp, yinterp, T_i/8e-4, cmap='Reds', levels=levels)


plt.colorbar(a,ticks=np.arange(-0.1,1.01,0.5),aspect=10,label='${U}/{U}_{0}$')

plt.xticks([])
plt.ylim(0,1.5)
plt.xlim(0.6,2.5)
plt.yticks([0,0.5,1,1.5])
#plt.xlabel('$2x/B_{0}$',font)
plt.ylabel('$2y/B_{0}$',font)
#plt.savefig('z=-0.04u.png', bbox_inches = 'tight')


plt.ylim(-2,2)
plt.xlim(0,3)
plt.xlabel('${x}$(m)',font)
plt.ylabel('${y}$(m)',font)
#plt.savefig('z=-0.04v.png', bbox_inches = 'tight')


plt.show()
cs=plt.contour(xinterp,yinterp, T_i/8e-4,[0.3])
p1=cs.collections[0].get_paths()[0]
coor_p1=p1.vertices
a=coor_p1
ay=a[:,1]
ax=a[:,0]
import pandas as pd

pd.DataFrame(ax).to_csv("/media/haoran/DATA/4mcases/LFM/8degree/12_3/xsurR.csv", header=None,index=None)
pd.DataFrame(ay).to_csv("/media/haoran/DATA/4mcases/LFM/8degree/12_3/ysurR.csv", header=None,index=None)

plt.plot(ax,ay)


# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html