
###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z
'''
Complete calculation of interficial shear thickness. 
(1) create a 'cross-section' perpendicular to triangle edge based on RDD=0.5RDD0
(2) on the cross-section, find an interface based on RDD=0.5RDD0
(3) calculate interficial shear thickness
'''

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
from pylab import *


zm=-0.01

#define input parameters
H0=0.08

S=tan(8/180*pi)   ##slope
######formula for the right edge
###x=fh(y)
#fh=array([1.24886913, 2.21476453, 2.46320545, 1.57121228]) #z=-0.02
#fh=array([1.40419651, 2.4028571 , 2.51551141, 1.61486469]) #z=-0.04
#fh=array([0.80280231, 1.17656895, 1.87150374, 1.66735169])   #z=-0.08
#fh=array([-14.7963398 , -40.08499826, -34.66796866,  -9.11154807,
 #        0.07709521,   0.83251051,   1.75818792])  #z=-0.16
#fh=array([-1.07184256, -1.91901417,  0.37172948,  1.64656155])
#fh=array([-2.022009  , -3.83471716, -0.93109604,  1.70661318]) #z=-0.18b
#fh=array([-0.38280923, -0.92515814,  0.77127414,  1.61827272])  #z=-0.1
#fh=array([12.56069087, 37.99995363, 39.22021036, 15.30800594,  2.51954355,
 #       1.85478841])  #z=-0.18a
#fh=array([1.41191599, 2.31843035, 2.44166524, 1.65783413])
fh=array([1.21399747, 2.19298577, 2.47051287, 1.55969014]) #-0.01
ph = np.poly1d(fh)

fh_grad=np.zeros(3)

for i in range (0,3):
    fh_grad[i]=(3-i)*fh[i]   ### function dy/dx ----> x  meaning that afa=arctan(fc_grad(x))
ph_grad = np.poly1d(fh_grad)

ylist=np.linspace(-1,0,1001)
xlist=ph(ylist)



fig = plt.figure(figsize=(5,3), dpi=100)
plt.plot(ylist,xlist)
plt.plot(-ylist,xlist)
plt.xlim(1.5,-1.5)

####################
####################
####################
ytarget=-0.5

####################
####################
####################

xc=ph(ytarget)
yc=ytarget

plt.scatter(yc,xc,c='white',edgecolor='black',s=50)
plt.show()

print('the reference point on the horizontal plane is:')

print('yc=%.4fm'%yc +' ' + 'xc=%.4fm'%xc)

#, afa is the angle between x coordinate and tangent line
afa=arctan(1/ph_grad(yc))

xo=xc*cos(afa)+yc*sin(afa)
yo=-xc*sin(afa)+yc*cos(afa)

print('the adapted point on the horizontal plane is:')

print('yo=%.4fm'%yo +' ' + 'xo=%.4fm'%xo)


#find the file to read and load mesh
sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)
    #adapt the coordinate according to afa
    xcl=x*cos(afa)+y*sin(afa)
    ycl=-x*sin(afa)+y*cos(afa)
    zcl=z

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables

#define the time file to read, timename=390 means the mean of 360s-390s
timename = '210'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w30U')
    T = readscalar(sol, timename, 'TMean_w30T')
    Ux=U[0]
    Uy=U[1]
    Uz=U[2]
    #adapt velocity data to adapted coordinate
    Uxcl=Ux*cos(afa)+Uy*sin(afa)
    Uycl=-Ux*sin(afa)+Uy*cos(afa)
    Uzcl=Uz
    Ucl=array([Uxcl,Uycl,Uzcl])
   
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields ar6e interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridycl = 800
ngridzcl = 160
# Interpolation grid dimensions
Xclplane=xo


yclinterpmax = 0
yclinterpmin=yo*2
zclinterpmin = -2*xc*S-H0
zclinterpmax = 0


# Interpolation grid
ycli = np.linspace(yclinterpmin, yclinterpmax, ngridycl)
zcli = np.linspace(zclinterpmin, zclinterpmax, ngridzcl)
# Structured grid creation
yclinterp, zclinterp = np.meshgrid(ycli, zcli)

###########
###########
dxcl=0.01
###########
###########
Iplane=np.where(np.logical_and(xcl>=Xclplane-dxcl,xcl<=Xclplane+dxcl))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    Tcl_i = griddata((ycl[Iplane], zcl[Iplane]), T[Iplane], (yclinterp, zclinterp), method='linear')
    Uycl_i = griddata((ycl[Iplane], zcl[Iplane]), np.transpose(Ucl[1, Iplane]), (yclinterp, zclinterp), method='linear')
    Uzcl_i = griddata((ycl[Iplane], zcl[Iplane]), np.transpose(Ucl[2, Iplane]), (yclinterp, zclinterp), method='linear')
    Uxcl_i = griddata((ycl[Iplane], zcl[Iplane]), np.transpose(Ucl[0, Iplane]), (yclinterp, zclinterp), method='linear')

Uycl=np.zeros((len(Uycl_i),len(Uycl_i[0])))
Uzcl=np.zeros((len(Uycl_i),len(Uycl_i[0])))
Uxcl=np.zeros((len(Uycl_i),len(Uycl_i[0])))

for i in range (0,len(Uycl_i)):
    for j in range (0,len(Uycl_i[0])):
        Uycl[i][j]=Uycl_i[i][j][0]
        Uzcl[i][j]=Uzcl_i[i][j][0]
        Uxcl[i][j]=Uxcl_i[i][j][0]

for i in range (0,len(Uycl_i)):
    for j in range (0,len(Uycl_i[0])):
        if (-(xo*cos(afa)-yclinterp[i][j]*sin(afa))*S-0.08)>zclinterp[i][j]:
            Uycl[i][j]=np.nan
            Uzcl[i][j]=np.nan
            Uxcl[i][j]=np.nan
            Tcl_i[i][j]=np.nan
###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

# Define plot parameters
fig = plt.figure(figsize=(10,2), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.xlabel('y-perp(m)')
plt.ylabel('z(m)')
# Plots the contour of sediment concentration
for i in range (0,len(yclinterp)):
    for j in range (0,len(yclinterp[0])):
        if zclinterp[i][j]>-0.005:
            Tcl_i[i][j]=np.nan
            Uycl[i][j]=np.nan
            Uzcl[i][j]=np.nan
            Uxcl[i][j]=np.nan
levels = np.arange(0, 1.02, 0.01)
a=plt.contourf(yclinterp, zclinterp, Tcl_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),label='RDD')
cs=plt.contour(yclinterp, zclinterp, Tcl_i/(8e-4), [0.5],color='black')
p1=cs.collections[0].get_paths()[0]
coor_p1=p1.vertices
a=coor_p1
ayclo=a[:,0]
azclo=a[:,1]

aycl=[]
azcl=[]

for i in range (0,len(ayclo)):
    fillay=[0]
    fillaz=[0]
    if azclo[i]>-0.02:
        fillay[0]=ayclo[i]
        fillaz[0]=azclo[i]
        aycl=aycl+fillay
        azcl=azcl+fillaz


'''
formula of interface
'''
finter=np.polyfit(aycl,azcl,3)    ###formula of interface
pinter=np.poly1d(finter)
zvals = pinter(aycl)
plt.plot(aycl,zvals,color='green',linewidth=2)  

plt.xlim(0,yclinterpmin)
plt.ylim(zclinterpmin,0)
plt.show()


#####################plot shear velocity
fig = plt.figure(figsize=(10,2), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.xlabel('ycl(m)')
plt.ylabel('z(m)')

levels = np.arange(-0.1, 1.02, 0.01)
a=plt.contourf(yclinterp, zclinterp, Uxcl/(0.075), cmap='jet', levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),label='U/U0')

plt.plot(aycl,zvals,color='red',linewidth=2)  
plt.xlim(0,yclinterpmin)
plt.ylim(zclinterpmin,0)


##############################
##from here, start to calculate shear thickness

for i in range (0,len(aycl)):
    if zvals[i]<=zm:
        ym=aycl[i]
        break



finter_grad=np.zeros(3)

for i in range (0,3):
    finter_grad[i]=(3-i)*finter[i]   ### function dzcl/dycl 
pinter_grad = np.poly1d(finter_grad)

rflycl=[0,0]
rflzcl=[0,0]
rflycl[0]=ym-0.05
rflycl[1]=ym+0.05
rflzcl[0]=zm+0.05/pinter_grad(ym)
rflzcl[1]=zm-0.05/pinter_grad(ym)

plt.plot(rflycl,rflzcl,color='black')   ######plot the reference line
plt.show()

######################



print('the 2nd order reference point is:')
print(ym,zm)

beta=arctan(1/pinter_grad(ym))

#reference point in new mesh
yoo=ym*cos(afa)-zm*sin(afa)
zoo=zm*cos(afa)+ym*sin(afa)

print('the 2nd order reference point is adopted into:')
print(yoo,zoo)



##adapt the coordinates 
yall=np.zeros(int(len(yclinterp)*len(yclinterp[0])))
zall=np.zeros(int(len(zclinterp)*len(zclinterp[0]))) 
Uall=np.zeros(int(len(Uxcl)*len(Uxcl[0]))) 
Vall=np.zeros(int(len(Uxcl)*len(Uxcl[0]))) 
Wall=np.zeros(int(len(Uxcl)*len(Uxcl[0]))) 

num=0    
for i in range (0,len(yclinterp)):
    for j in range (0,len(yclinterp[0])):
        yall[num]=yclinterp[i][j]
        zall[num]=zclinterp[i][j]
        Uall[num]=Uxcl[i][j]
        Vall[num]=Uycl[i][j]
        Wall[num]=Uzcl[i][j]
        num=num+1


ynew=yall*cos(afa)-zall*sin(afa)
znew=zall*cos(afa)+yall*sin(afa)

####rebuild mesh (interpolation)

##define a function to adapt coordinate for single points
def changecoordinate (yb,zb):  
    ya=yb*cos(afa)-zb*sin(afa)
    za=zb*cos(afa)+yb*sin(afa)
    return (ya,za)

########################################################
#calculate the adapted coordinates for the four boundary 
#points
########################################################

leftup=changecoordinate(0,0)
leftdown=changecoordinate(0,zclinterpmin)
rightup=changecoordinate(yclinterpmin,0)
rightdown=changecoordinate(yclinterpmin,zclinterpmin)

yboun=np.zeros(4)
yboun[0]=leftup[0]
yboun[1]=leftdown[0]
yboun[2]=rightup[0]
yboun[3]=rightdown[0]

zboun=np.zeros(4)
zboun[0]=leftup[1]
zboun[1]=leftdown[1]
zboun[2]=rightup[1]
zboun[3]=rightdown[1]

#######
print('max and mim ynew are:')
print(max(yboun),min(yboun))
print('max and mim znew are:')
print(max(zboun),min(zboun))

print('range of ynew is:')
print(max(yboun)-min(yboun))

print('range of znew is:')
print(max(zboun)-min(zboun))

ngridynew = int((max(yboun)-min(yboun))*300)
ngridznew = int((max(zboun)-min(zboun))*300)

ynewmax= max(yboun)
ynewmin = min(yboun)
znewmin = min(zboun)
znewmax = max(zboun)

ynewi = np.linspace(ynewmin, ynewmax, ngridynew)
znewi = np.linspace(znewmin, znewmax, ngridznew)

# Structured grid creation
ynewinterp, znewinterp = np.meshgrid(ynewi, znewi)

Unew = griddata((ynew, znew), Uall, (ynewinterp, znewinterp), method='linear')

####find the reference line (perpendicular to the interface)

a=plt.contourf(ynewinterp, znewinterp, Unew, cmap='jet')
plt.show()

err=1
for i in range (0,len(znewi)):
    if abs(znewi[i]-zoo)<err:
        err=abs(znewi[i]-zoo)
        m=i


#####get usful data

yline=[]
Uline=[]
for i in range (0,len(ynewi)):
    fillU=[0]
    filly=[0]
    if str(Unew[m][i])!='nan' :
        fillU[0]=Unew[m][i]
        filly[0]=ynewi[i]
        yline=yline+filly
        Uline=Uline+fillU
plt.plot(Uline)      
plt.show()


###########find Umin
plt.scatter(yline,Uline)
Umin=1
for i in range (0,30):
    if Uline[i]<Umin:
        Umin=Uline[i]
        nummin=i
        
plt.scatter(yline[nummin],Uline[nummin],color='red',s=50)

Umax=0
for i in range (0,20):  #len(Uline)
    if Uline[i]>Umax:
        Umax=Uline[i] 
        nummax=i
       
plt.scatter(yline[nummax],Uline[nummax],color='blue',s=50)
    

detaU=Umax-Umin

normalU=(Uline-Umin)/detaU   ###normalised velocity U/detaU


for i in range (0,len(Uline)):
    if normalU[i]>=0.15:
        ylinelow=yline[i]
        plt.scatter(ylinelow,Uline[i],color='purple',s=50)
        break
for i in range (0,len(Uline)):
    if normalU[i]>=0.85:
        ylinehigh=yline[i]
        plt.scatter(ylinehigh,Uline[i],color='yellow',s=50)
        break


Uin=[0,0]
yin=[yoo,yoo]
Uin[0]=min(Uline)
Uin[1]=max(Uline)
plt.plot(yin,Uin,'-.',color='black')
plt.show()  
print('##################################################')
print('##################################################')
print('##################################################')
cita=(ylinehigh-ylinelow)/(0.85-0.15)  
print('xtarget is: %.2f m'%(xc))
print('interface thickness is: %.2f cm'%(cita*100))
print('the angel afa is: %.1f '%(afa*180/pi))  ##gnew=g'*sin(afa)
print('the angel beta is: %.1f'% (beta*180/pi))  ##gnew=g'*sin(afa)
print('Uline max is %.2f cm/s'%(Umax*100))
print('Uline min is %.2f cm/s'%(Umin*100))

   