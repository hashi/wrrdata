"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""
'''
This program is used to plot the time-averaged (30s) surface plane (z=0)
'''

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '250'
if loadData==1:
    #U = readvector(sol, timename, 'UMean_w30U')
    #T = readscalar(sol, timename, 'TMean_w30T')
    curl=  readvector(sol, timename, 'vorticity_1s')
   
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 200
ngridy = 300

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 3
yinterpmin = -1.5
yinterpmax =  1.5

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.04
dz=0.01

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    
    curl_k = griddata((x[Iplane], y[Iplane]), np.transpose(curl[2, Iplane]), (xinterp, yinterp), method='linear')
    CURL=np.zeros((len(curl_k),len(curl_k[0])))
    for i in range (0,len(curl_k)):
        for j in range (0,len(curl_k[0])):
            CURL[i][j]=curl_k[i][j][0]

    

###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


# Define plot parameters

# Define plot parameters
fig = plt.figure(figsize=(4, 4), dpi=100)
plt.rcParams.update({'font.size': 10})
plt.xlabel('x(m)')
plt.ylabel('y(m)')



n=23
c=0   
deta=5
jmax=np.zeros(n)
jmin=np.zeros(n)
maxCURL=np.zeros(n)
minCURL=np.zeros(n)

for i in range (0,n):
    for j in range (0,len(CURL[:,int(i*deta+c)])):  #"15,20,25,30,35,40,45,50,55,60..."
        if yinterp[j][int(i*deta+c)]>0:
            if CURL[j][int(i*deta+c)]>maxCURL[i]:
                maxCURL[i]=CURL[j][int(i*deta+c)]
                jmax[i]=j   
        else:
            if CURL[j][int(i*deta+c)]<minCURL[i]:
                minCURL[i]=CURL[j][int(i*deta+c)]
                jmin[i]=j  
                

x_1=np.zeros(n+1)
y_1=np.zeros(n+1)
x_1[0]=0
y_1[0]=1
for i in range (0,n):
    x_1[i+1]=xinterp[0][int(i*deta+c)]
    y_1[i+1]=yinterp[int(jmax[i])][0]

f1=np.polyfit(x_1,y_1,3)
p1 = np.poly1d(f1)
yvals1 = p1(x_1) 
plt.plot(x_1,yvals1,color='black')

x_2=np.zeros(n+1)
y_2=np.zeros(n+1)
x_2[0]=0
y_2[0]=-1
for i in range (0,n):
    x_2[i+1]=xinterp[0][int(i*deta+c)]
    y_2[i+1]=yinterp[int(jmin[i])][0]

f2=np.polyfit(x_2,y_2,3)
p2 = np.poly1d(f2)
yvals2 = p2(x_2) 
plt.plot(x_2,yvals2,color='black')

#############
##x=f(y)
fplane=np.polyfit(y_2,x_2,3)
pplane=np.poly1d(fplane)
xvals2 = pplane(y_2) 
plt.plot(xvals2,y_2,color='green')

####change to cm
f3=np.polyfit(x_2*100,y_2*100,3)





# Plots the contour of horizontal curl
levels = np.arange(-0.6, 0.61, 0.01)
a=plt.contourf(xinterp, yinterp, CURL, cmap='coolwarm',levels=levels)
plt.colorbar(a,ticks=np.arange(-1.2,1.21,0.6),label='CURL (1/s)')
for i in range (0,n):
    plt.scatter(xinterp[0][int(i*deta+c)],yinterp[int(jmax[i])][0],color='black',s=10)  
    plt.scatter(xinterp[0][int(i*deta+c)],yinterp[int(jmin[i])][0],color='black',s=10)  
plt.xlim(0,2.5)
plt.show()


fig = plt.figure(figsize=(4, 4), dpi=300)

plt.rcParams.update({'font.size': 12,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :12,
}
####plot half of the curl
levels = np.arange(-1, 1.01, 0.01)
a=plt.contourf(xinterp, yinterp, CURL, cmap='coolwarm',levels=levels,extend='both')
plt.colorbar(a,ticks=np.arange(-1,1.01,0.5),label='${\u03C9}_{k}$ (${s}^{-1}$)')
plt.xlim(0,2)
plt.ylim(-1.2,1.2)
plt.xlabel('$x$($m$)')
plt.ylabel('$y$($m$)')
plt.text(1.6,1,'NUM')
plt.text(1,1.25,'(b)')
plt.show()




# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html