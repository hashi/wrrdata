"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""
'''
This program is used to plot the time-averaged (30s) surface plane (z=0)
'''

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '270'
if loadData==1:
    #U = readvector(sol, timename, 'UMean_w30U')
    #T = readscalar(sol, timename, 'TMean_w30T')
    U = readvector(sol, timename, 'U')
    T = readscalar(sol, timename, 'T')
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 200
ngridy = 300

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 2.5
yinterpmin = -1.5
yinterpmax =  1.5

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.04
dz=0.01

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')
    Uy_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[1, Iplane]), (xinterp, yinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline
fig = plt.figure(figsize=(6, 6), dpi=100)
plt.rcParams.update({'font.size': 10})
XprofileList=np.arange(20,160,20)
for Xprofile in XprofileList:
    plt.subplot(311)
    plt.plot(yinterp[:,Xprofile], T_i[:,Xprofile],label=str(round(xinterp[0,Xprofile]*100)/100))
    plt.xlabel('y(m)')
    plt.ylabel('RDD')
    plt.legend()
    plt.subplot(312)
    plt.plot(yinterp[:,Xprofile], Ux_i[:,Xprofile,0])
    plt.plot([yinterp[0,Xprofile], yinterp[ngridy-1,Xprofile]], [0, 0],'--k')
    plt.xlabel('y(m)')
    plt.ylabel('U(m/s)')
    plt.subplot(313)
    plt.plot(yinterp[:,Xprofile], Uy_i[:,Xprofile,0])
    plt.xlabel('y(m)')
    plt.ylabel('V(m/s)')
#plt.savefig('figures/profile.png', bbox_inches = 'tight')

# Define plot parameters
fig = plt.figure(figsize=(3, 4), dpi=200)
plt.rcParams.update({'font.size': 8,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :8,
}


plt.xlabel('${x}$(m)')
plt.ylabel('${y}$(m)')

# Plots the contour of sediment concentration
levels = np.arange(0, 2.01, 0.01)
a=plt.contourf(xinterp, yinterp, T_i*1000, cmap=plt.cm.Reds, levels=levels)
plt.colorbar(a,ticks=np.arange(0,2.01,0.5),label='RDDx1000')

#obtain the coordinate of contour 
cs=plt.contour(xinterp, yinterp, T_i*1000,[0.38],linewidths=1,colors='blue')
p1=cs.collections[0].get_paths()[0]
coor_p1=p1.vertices
a=coor_p1[::10]
ax=a[:,0]
ay=a[:,1]



#plot x=0.8
xs=(0.8,0.8)
yx=(-1.5,1.5)
#plt.plot(xs,yx,color='blue')

#plt.scatter(ax,ay)
'''
probelist=[4,6,8,10,12,14,16,18,20,22]
for probe in probelist:
    plt.scatter(ax[probe],ay[probe],color='green')
'''
'''
wb=load_workbook('surfaceKH-rollup-0.9.xlsx',data_only=True)
ws=wb.active

probex=np.zeros(17)
probey=np.zeros(17)
for i in range (0,17):
    probex[i]=(ws.cell(row=i+2,column=8)).value
    probey[i]=(ws.cell(row=i+2,column=9)).value
#plt.scatter(probex,probey,color='green',s=3)
#plot analytical solution
RDD=0.00034
W=2
u01=0.033
g=9.81
g0= RDD*g
T1=0
H0=0.083
tan=(0.75-0.083)/4.75
W1=0
while W1<W:
    T1+=0.001   
    W1=(g0*H0+g0*u01*T1*tan)**(3/2)*(2/3)*(1/(g0*u01*tan))-(2/3)*(g0*H0)**(3/2)*(1/(g0*u01*tan))
xp1=u01*T1

  
import numpy as np
import matplotlib.pyplot as plt
x1=np.empty([200,1])
y1=np.empty([200,1])
t1=0
for i in range (0,100):
    t1=t1+T1/(100)
    x1[i]=u01*t1
    y1[i]=((g0*H0+g0*u01*t1*tan)**(3/2)*(2/3)*(1/(g0*u01*tan))-(2/3)*(g0*H0)**(3/2)*(1/(g0*u01*tan)))/2-W/2
t1=T1
for i in range (100,200):
    t1=t1-T1/(100)
    x1[i]=u01*t1
    y1[i]=W/2-((g0*H0+g0*u01*t1*tan)**(3/2)*(2/3)*(1/(g0*u01*tan))-(2/3)*(g0*H0)**(3/2)*(1/(g0*u01*tan)))/2
 
    
#plt.plot(x1,y1,color="black",linewidth=2 )



# Calculation of the streamline width as a function of the velociy magnitude
U_i = np.sqrt(Ux_i**2 + Uy_i**2)
lw = pow(U_i, 1.5)/U_i.max()

# Plots the streamlines
#q=plt.quiver(xi[::5], yi[::5], Ux_i[::5,::5,0], Uy_i[::5,::5,0])
#quiverkey(q, X=0.3, Y=1.1, U=0.03,
 #            label='= 0.03 m/s', labelpos='E')

#for Xprofile in XprofileList:
    #plt.plot([xinterp[0,Xprofile], xinterp[0,Xprofile]],[yinterp[0,Xprofile], yinterp[ngridy-1,Xprofile]])

#plt.savefig('figures/z=0RDD.png', bbox_inches = 'tight')
'''
plt.show()




# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html