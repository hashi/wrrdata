"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""
'''
This program is used to plot the time-averaged (30s) surface plane (z=0)
'''

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar, readfield
import matplotlib.pyplot as plt
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '280'
if loadData==1:
    
    Kcom = readfield(sol, timename, 'UPrime2Mean_w30U')/2
    k = readfield(sol, timename, 'kMean_w30k')
    
K=Kcom[0]+Kcom[3]+Kcom[5]
M=k/(K+k)

# Number of division for linear interpolation
ngridx = 200
ngridy = 300

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 4
yinterpmin = -1.5
yinterpmax =  1.5

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.04
dz=0.01

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    M_i = griddata((x[Iplane], y[Iplane]), M[Iplane], (xinterp, yinterp), method='linear')

# Define plot parameters
fig = plt.figure(figsize=(3, 4), dpi=200)
plt.rcParams.update({'font.size': 8,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :8,
}


plt.xlabel('x/B')
plt.ylabel('y/B')

# Plots the contour of sediment concentration
levels = np.arange(0, 1.01, 0.001)
a=plt.contourf(xinterp, yinterp, M_i, cmap=plt.cm.Reds, levels=levels)
cs=plt.contour(xinterp, yinterp, M_i, [0.2],colors="green")
plt.colorbar(a,ticks=np.arange(0,1.01,0.2),label='M')

x = (pd.read_csv("forM_x.csv", header = None)).values
y = (pd.read_csv("forM_y.csv", header = None)).values
plt.xlim(0,2)
plt.plot(x,y,'--', color='black')

        
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html