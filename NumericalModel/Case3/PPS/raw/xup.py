"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '300'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean_w90U')
    T = readscalar(sol, timename, 'TMean_w90T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

Xplane=1.46
H0=0.08
H=-Xplane*tan(8/180*pi)-H0
dx=0.01
# Number of division for linear interpolation
ngridy = 401
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -2.5
yinterpmax = 2.5
zinterpmin = H
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)




Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
    Uy_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[1, Iplane]), (yinterp, zinterp), method='linear')
    Uz_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (yinterp, zinterp), method='linear')
    Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')

###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


# Plots the contour of sediment concentration

levels = np.arange(0, 1.02, 0.01)
plt.figure(figsize=(10, 3),dpi=300)
plt.rcParams.update({'font.size': 22,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :22,
}


a=plt.contourf(yinterp, zinterp, T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
#plt.contour(yinterp, zinterp, T_i/(8e-4),[0.05])
plt.colorbar(a,ticks=np.arange(0,1.01,0.5),aspect=7.5,label=r'$\overline{R}$/${R}_{0}$')

# Calculation of the streamline width as a function of the velociy magnitude
plt.ylim(H,0)
plt.xlim(1.5,-1.5)
plt.xlabel('${y}$(m)')
plt.ylabel('${z}$(m)')
#plt.text(1.95,-0.03,'(c)')
#plt.plot([0,0],[H,-0.005],'-.',color='blue',linewidth=2)
#plt.plot([0.5,0.5],[H,-0.005],'-.',color='green',linewidth=2)
#plt.plot([1,1],[H,-0.005],'-.',color='orange',linewidth=2)
plt.yticks([-0.2,-0.1,0])
plt.show()




# Plots the contour of U
Ut=np.zeros((100,401))
for i in range (0,100):
    for j in range (0,401):
      c=Ux_i[i][j]
      if str(c)=='[nan]':
          Ut[i][j]=0
      else:
          Ut[i][j]=c[0]
print('U(min)= %f ' % Ut.min())
print('U(max)= %f ' % Ut.max())
levels = np.arange(-0.1, 1.11,0.001)     
plt.figure(figsize=(10, 3),dpi=300)
a=plt.contourf(yinterp, zinterp, Ut/0.075, cmap='jet', levels=levels)
plt.colorbar(a,ticks=np.arange(-0.1,1.11,0.6),aspect=8,label='$U/U_{0}$')

V=np.zeros((100,401))
W=np.zeros((100,401))
for i in range (0,100):
    for j in range (0,401):
      c=Uy_i[i][j]
      c2=Uz_i[i][j]
      if str(c)=='[nan]':
          V[i][j]=0
          W[i][j]=0
      else:
          W[i][j]=c2[0]
          V[i][j]=-c[0]
Uall = np.sqrt(V**2 + W**2)
lw = pow(Uall, 1.5)/Uall.max()
q=plt.quiver(yinterp[0][::10], zinterp[:,0][::10], V[::10,::10],  W[::10,::10],width=0.005,scale=0.5,headwidth=2,color='red')
quiverkey(q, X=1, Y=-0.2, U=0.02,
            label='= 2 cm/s', labelpos='E')

plt.ylim(H,0)
plt.xlim(1.5,-1.5)
plt.xlabel('$y$(m)')
plt.ylabel('$z$(m)')
plt.yticks([-0.2,-0.1,0])
plt.plot([0,0],[H,-0.005],'-.',color='blue',linewidth=2)
plt.plot([0.5,0.5],[H,-0.005],'-.',color='green',linewidth=2)
plt.plot([1,1],[H,-0.005],'-.',color='orange',linewidth=2)
plt.text(1.95,-0.03,'(d)',color='black')
plt.show()



fig = plt.figure(figsize=(6, 4), dpi=100)
plt.rcParams.update({'font.size': 15,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :15,
}



##############
##data smooth
#############
'''
def smooth (y,num):    
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if int((num+1)/2)<=i<len(y)-int((num+1)/2):
            for j in range (-int((num+1)/2),int((num+1)/2)+1):             
                ys[i]=ys[i]+y[i+j]      
            ys[i]=ys[i]/(num+1)
        else:            
            ys[i]=y[i]
    return ys
'''
################################
def addU (data):
    for i in range (0,len(data)):
        if str(data[i+1])!='nan':
            data[i]=0
            break
    return data
#####0
Uline=Ut[:,200]/0.075
Tline=T_i[:,200]/(8e-4)
zline=zinterp[:,200]

Uline=addU(Uline)
for i in range (0,len(Uline)):
    if zline[i]>-0.01:
        Uline[i]=np.nan
        

plt.plot(Uline,zline,'-.',color='blue',label='$y=0m$[$U/U_{0}$]',linewidth=2)
plt.plot(Tline,zline,'--',color='blue',label='$y=0m$'+r'[$\overline{R}/R_{0}$]',linewidth=2)
#####xp
Uline=Ut[:,250]/0.075
zline=zinterp[:,250]
Uline=addU(Uline)
Tline=T_i[:,250]/(8e-4)
plt.plot(Uline,zline,'-.',color='green',label='$y=0.5m$[$U/U_{0}$]',linewidth=2)
plt.plot(Tline,zline,'--',color='green',label='$y=0.5m$'+r'[$\overline{R}/R_{0}$]',linewidth=2)

#####xu
Uline=Ut[:,300]/0.075
zline=zinterp[:,225]
Tline=T_i[:,300]/(8e-4)
Uline=addU(Uline)
plt.plot(Uline,zline,'-.',color='orange',label='$y=1m$[$U/U_{0}$]',linewidth=2)
plt.plot(Tline,zline,'--',color='orange',label='$y=1m$'+r'[$\overline{R}/R_{0}$]',linewidth=2)

plt.xlabel('$U/U_{0}$ &' r'$\overline{R}$/${R}_{0}$',fontdict=font)
plt.ylabel('$z$ (m)',fontdict=font)

plt.legend(loc=(-0.3,-0.43),frameon=0,ncol=3)
plt.show()

'''
pd.DataFrame(yinterp[14]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/I_12/DATA_PLOT/x=xup/NUM/y.csv", header=None,index=None)
pd.DataFrame(Ux_i[14]).to_csv("/home/haoran/PhD-Haoran/Labresults/MEAN/EXP23/I_12/DATA_PLOT/x=xup/NUM/U.csv", header=None,index=None)

'''

