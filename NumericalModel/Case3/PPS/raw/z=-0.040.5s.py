"""

This code reads the time-averaged velocity data over 0.5s at the plane z=-0.04 and
write them into csv files

"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata
import pandas as pd
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables

for Time in range (211,251,1): 
    timename = str(Time)
    print(timename)
    
    if loadData==1:
        U = readvector(sol, timename, 'UMean_whalfU')
    
###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
    ngridx = 301
    ngridy = 401

# Interpolation grid dimensions
    xinterpmin = 0.
    xinterpmax = 3
    yinterpmin = -2
    yinterpmax =  2

# Interpolation grid
    xi = np.linspace(xinterpmin, xinterpmax, ngridx)
    yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
    xinterp, yinterp = np.meshgrid(xi, yi)

    Zvert=-0.04
    dz=0.005

    Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

    if interpolate==1:
    # Interpolation of scalra fields and vector field components
    #T_i = griddata((x[Iplane], y[Iplane]), T[Iplane], (xinterp, yinterp), method='linear')
        Ux_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')
        Uy_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[1, Iplane]), (xinterp, yinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline


    u=np.zeros((401,301))
    v=np.zeros((401,301))
    for i in range (0,401):
        for j in range (0,301):
            u[i][j]=Ux_i[i][j][0]
            v[i][j]=Uy_i[i][j][0]

    pd.DataFrame(u).to_csv("uv4cm/u%s.csv"%str(Time), header=None,index=None)
    pd.DataFrame(v).to_csv("uv4cm/v%s.csv"%str(Time), header=None,index=None)



# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html