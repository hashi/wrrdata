"""
This code is used to plot T-ynew-Utang contour

"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar, readfield
import matplotlib.pyplot as plt
import pandas as pd
from pylab import *


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)
 

timename = '240'
if loadData==1:    
    Tensor=readfield(sol, timename, 'UPrime2Mean_w30U')
    U = readvector(sol, timename, 'UMean_w30U')
    uu=Tensor[0]
    vv=Tensor[3]
    #k=readfield(sol, timename, 'kMean_w30k')
    K=uu+vv   ###turbulent kinetic energy


# Number of division for linear interpolation
ngridx = 251
ngridy = 301

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 2.5
yinterpmin = -1.2
yinterpmax =  1.2

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.04
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    K_i = griddata((x[Iplane], y[Iplane]), K[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]),  np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')
for i in range (0,len(K_i)):
    for j in range (0,len(K_i[0])):
        
        K_i[i][j]=K_i[i][j]
plt.rcParams.update({'font.size': 9,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :9,
}
fig = plt.figure(figsize=(3,3),dpi=500)
levels = np.arange(0,5.1,0.1)
a=plt.contourf(xinterp,yinterp,K_i*10000,cmap='coolwarm',levels=levels)
plt.colorbar(a,ticks=np.arange(0,5.1,1),label='${K}$ (${cm}^{2}$ ${s}^{-2}$)')
plt.xlabel('2x/B')
plt.xlim(0,2)
plt.ylabel('2y/B')
plt.ylim(-1.2,1.2)

n=22
c=15   
deta=5
jmax=np.zeros(n)
maxK=np.zeros(n)
jmax=np.zeros(n)
maxK=np.zeros(n)
for i in range (0,n):
    for j in range (0,len(K_i[:,int(i*deta+c)])):  #"15,20,25,30,35,40,45,50,55,60..."
        if yinterp[j][int(i*deta+c)]<0:
            if K_i[j][int(i*deta+c)]>maxK[i]:
                maxK[i]=K_i[j][int(i*deta+c)]
                jmax[i]=j                    
for i in range (0,n):
    plt.scatter(xinterp[0][int(i*deta+c)],yinterp[int(jmax[i])][0],color='black',s=1)  
x_1=np.zeros(n+2)
y_1=np.zeros(n+2)
x_1[0]=0
y_1[0]=-1
for i in range (0,n):
    x_1[i+1]=xinterp[0][int(i*deta+c)]
    y_1[i+1]=yinterp[int(jmax[i])][0]
x_1[n+1]=1.7
y_1[n+1]=0
f1=np.polyfit(x_1,y_1,4)
p1 = np.poly1d(f1)
yvals = p1(x_1) 
plt.plot(x_1,yvals,color='black')
plt.plot(x_1,-yvals,color='black')
plt.show()



'''
fig = plt.figure(figsize=(5,3),dpi=500)
for i in range (1,n+1):   
    plt.scatter(x_1[i]*100,maxK[i-1],color='black')
plt.xlabel('x (cm)')
plt.ylabel('TKE (${cm}^{2} {s}^{-2}$)')
'''


        
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html