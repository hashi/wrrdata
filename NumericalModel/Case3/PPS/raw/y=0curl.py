"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
from pylab import *
#from pylab import *
#import matplotlib.gridspec as gridspec


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1
color=['b','yellow','pink','g','r','purple','orange']

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables
j=0


#plot the slope


for i in range (250,260,60):
    timename = str(i)
    if loadData==1:
         #curl=  readvector(sol, timename, 'vorticity_Mean_w30')
         curl=  readvector(sol, timename, 'vorticity_1s')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
    ngridx = 401
    ngridz = 80

# Interpolation grid dimensions
    xinterpmin = 0.
    xinterpmax = 4
    zinterpmin = -0.65
    zinterpmax = 0

# Interpolation grid
    xi = np.linspace(xinterpmin, xinterpmax, ngridx)
    zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
    xinterp, zinterp = np.meshgrid(xi, zi)

    Yplane=0
    dy=0.03

    Iplane=np.where(np.logical_and(y>=Yplane-dy,y<=Yplane+dy))

    if interpolate==1:
    # Interpolation of scalra fields and vector field components                
        curl_j = griddata((x[Iplane], z[Iplane]), np.transpose(curl[1, Iplane]), (xinterp, zinterp), method='linear')
        CURL=np.zeros((len(curl_j),len(curl_j[0])))
        for i in range (0,len(curl_j)):
            for j in range (0,len(curl_j[0])):
               CURL[i][j]=curl_j[i][j][0]


# Plots the contour of sediment concentration
fig = plt.figure(figsize=(8, 2.5), dpi=300)
plt.rcParams.update({'font.size': 9,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :9,
}


for i in range (0,len(CURL)):
    for j in range (0,len(CURL[0])):
        if zinterp[i][j]<(-xinterp[i][j]*tan(8/180*pi)-0.08):
            CURL[i][j]=np.nan
            
xs=[0,4]
ys=[-0.08,-4*tan(8/180*pi)-0.08]
plt.plot(xs,ys,c='black')
levels = np.arange(-1,1.1, 0.01)
a=plt.contourf(xinterp, zinterp, CURL, cmap='coolwarm', levels=levels,extend='both')
plt.colorbar(a,ticks=np.arange(-1,1.1,0.5),aspect=15,label='${\u03C9}_{j}$ (${s}^{-1}$)')
plt.xlim(0,2)


plt.xlabel('${x}$($m$)',fontdict=font)
plt.ylabel('${z}$($m$)',fontdict=font)
#plt.xlim(0,2.4)
plt.ylim(-4*np.tan(8/180*np.pi)-0.08,0)
plt.yticks([-0.6,-0.4,-0.2,0])
plt.xlim(0,3)
plt.text(2.8,-0.07,'(c)',color='black')

plt.show()

