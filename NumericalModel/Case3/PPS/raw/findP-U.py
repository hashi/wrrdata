"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from pylab import *
#from pylab import *
#import matplotlib.gridspec as gridspec


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '250'
if loadData==1:
    U = readvector(sol, timename, 'UMean_w70U')
    T = readscalar(sol, timename, 'TMean_w70T')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 401
ngridz = 80

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 4
zinterpmin = -0.642
zinterpmax = 0

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
xinterp, zinterp = np.meshgrid(xi, zi)

Yplane=0
dy=0.01

Iplane=np.where(np.logical_and(y>=Yplane-dy,y<=Yplane+dy))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], z[Iplane]), T[Iplane], (xinterp, zinterp), method='linear')
    Ux_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (xinterp, zinterp), method='linear')
    Uz_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (xinterp, zinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

#find boundary of plunge region and underflow region based on U
XprofileList2=np.arange(0,401,1)
m=0

Us=np.zeros((401,80))

detaz=0.642/80




Ut=np.zeros((80,401))
for i in range (0,80):
    for j in range (0,401):
      c=Ux_i[i][j]
      if str(c)=='[nan]' or zinterp[i][j]<(-0.08-xinterp[i][j]*tan(8/180*pi)):
          Ut[i][j]=0
          
      else:
          Ut[i][j]=c
       
          
for Xprofile in XprofileList2:
    for j in range (0,80):
        if (Ut[j][Xprofile])>0*max(Ut[:,Xprofile]):
            Us[m][j]=(Ut[j][Xprofile])
        else:
            Us[m][j]=0
        Us[m][79]=Us[m][78]

    m=m+1
U2all=np.zeros(400)
Uall=np.zeros(400)
xm=np.zeros(400)
hc=np.zeros(400)
for m in range (0,400):
    for j in range (0,80):
        U2all[m]+=(Us[m][j])**2*detaz
        Uall[m]+=(Us[m][j])*detaz
    hc[m]=(Uall[m])**2/U2all[m]
    
for n in range (0,400):
    xm[n]=0.01*n 
    
hc_u=hc[:200]   
for n in range (0,200):
    if hc_u[n]==max(hc_u):
        num=n
hp=max(hc)


print('xup is %.2f'%xm[num])
for n in range (150,250):
    if hc[n]==min(hc[100:250]):
        print('xud is %.2f'%xm[n])
        num2=n

def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys

hcs=smooth(hc,5)
hc_u=hcs[:200]   
for n in range (0,200):
    if hc_u[n]==max(hc_u):
        num=n
for n in range (150,250):
    if hc[n]==min(hc[100:250]):
        print('xud is %.2f'%xm[n])
        num2=n

fig, ax1 = plt.subplots(figsize=(3,2),dpi=300)
plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}
H0=0.08

ax1.plot(xm/2,hcs/H0,'-.',color='black')
ax1.set_xlim(0,1.05)
ax1.set_ylim(0,4.2)
plt.scatter([xm[num]/2],[hcs[num]/H0],marker='x',color='red')
plt.scatter([xm[num2]/2],[hcs[num2]/H0],marker='x',color='blue')
plt.text(1.48/2,0.3/H0,'${x}_{up}$')
plt.text(1.9/2,0.20/H0,'${x}_{ud}$')

ax1.set_xlabel('$x/{B}_{0}$')
ax1.set_ylabel('$h_{c}/{H}_{0}$')
ax1.set_yticks([0,1,2,3,4])
ax1.tick_params(labelsize=10)
#plt.text(1.05,0.37,'(a)')

import pandas as pd 
'''
xexp=(pd.read_csv("/media/haoran/DATA/4mcases/comUc/Frd3/xexp.csv", header = None)).values
thick=(pd.read_csv("/media/haoran/DATA/4mcases/comUc/Frd3/thick.csv", header = None)).values
ax1.plot(xexp,thick,'-.',color='black')
'''





x=(pd.read_csv("/media/haoran/DATA/4mcases/comUc/Frd3/nonx.csv", header = None)).values
gc=(pd.read_csv("/media/haoran/DATA/4mcases/comUc/Frd3/gc.csv", header = None)).values
gc=smooth(gc,10)
#ax2 = ax1.twinx()
#ax2.plot(x,gc/8e-4/9.81,'--',color='blue',linewidth=1.5)
#ax2.set_ylim(0,1.1)
#ax2.set_ylabel("${g}_{c}'/{g}_{0}'$",color='blue')
#ax2.set_yticks([0,0.25,0.5,0.75,1])
#ax2.tick_params(labelsize=10)
#plt.text(1.45,0.7,"${g'}_{p}$")
#ax2.scatter([xm[num]],[0.8],c='',edgecolor='blue',s=40)
plt.show()





###########################
##calculate Frd
###########################
Ut=Us.T
Um=0
count=0
Ut[79][num]=Ut[78][num]
for i in range (0,80):
    if zinterp[i][num]>=(-0.08-xm[num]*tan(8/180*pi)) and Ut[i][num]>0:
        Um=Um+Ut[i][num]
        count+=1
Um=Um/count

Tm=0
count=0
T_i[79][num]=T_i[78][num]
for i in range (0,80):
    if zinterp[i][num]>=(-0.08-xm[num]*tan(8/180*pi)) and Ut[i][num]>0:
        Tm=Tm+T_i[i][num]
        count+=1
Tm=Tm/count
        
Hp=xm[num]*tan(8/180*pi)+0.08


Frdp=Um/(9.81*Hp*Tm)**0.5

print('the real Frdp is %.2f'%Frdp)
print(Tm)
###########################
##calculate Frd following Arita
###########################
Um=0
count=0
Ut[79][num]=Ut[78][num]
for i in range (0,80):
    if zinterp[i][num]>=(-0.08-xm[num]*tan(8/180*pi)) and Ut[i][num]>0:
        Um=Um+Ut[i][num]
        count+=1
Um=Um/count

Tm=8e-4

        
Hp=xm[num]*tan(8/180*pi)+0.08


Frdp=Um/(9.81*Hp*Tm)**0.5
print('Frd follow Arita is %.2f'%Frdp)


H0=0.08
F0=3
FrdpA=F0*(H0/Hp)**(3/2)

print('Frd follow Arita is %.2f'%FrdpA)



# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html