"""
This code is used to plot T-ynew-Utang contour

"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar, readfield
import matplotlib.pyplot as plt
import pandas as pd
from pylab import *


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)
 

timename = '210'
if loadData==1:    
    Tensor=readfield(sol, timename, 'UPrime2Mean_w30U')
    U = readvector(sol, timename, 'UMean_w30U')
    uv=Tensor[1]
   



# Number of division for linear interpolation
ngridx = 251
ngridy = 301

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 2.5
yinterpmin = -1.2
yinterpmax =  1.2

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.035
dz=0.01

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    uv_i = griddata((x[Iplane], y[Iplane]), uv[Iplane], (xinterp, yinterp), method='linear')
    Ux_i = griddata((x[Iplane], y[Iplane]),  np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')


plt.rcParams.update({'font.size': 9,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :9,
}
fig = plt.figure(figsize=(3,3),dpi=500)
levels = np.arange(-0.2,0.2,0.01)
a=plt.contourf(xinterp,yinterp,uv_i/((0.075)**2),cmap='coolwarm',levels=levels)
plt.colorbar(a,ticks=np.arange(-2,2.01,0.5),label='${K}$ (${cm}^{2}$ ${s}^{-2}$)')
plt.xlabel('2x/B')
plt.xlim(0,1.6)
plt.ylabel('2y/B')
plt.ylim(-1.2,1.2)




        
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html