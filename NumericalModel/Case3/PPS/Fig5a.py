"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z


from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '300'
if loadData==1:
    U = readvector(sol, timename, 'UMean')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 200
ngridy = 400

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 4
yinterpmin = -4
yinterpmax =  4

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
yi = np.linspace(yinterpmin, yinterpmax, ngridy)

# Structured grid creation
xinterp, yinterp = np.meshgrid(xi, yi)

Zvert=-0.04
dz=0.005

Iplane=np.where(np.logical_and(z>=Zvert-dz,z<=Zvert+dz))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
   
    Ux_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[0, Iplane]), (xinterp, yinterp), method='linear')
    Uy_i = griddata((x[Iplane], y[Iplane]), np.transpose(U[1, Iplane]), (xinterp, yinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

#plot u

plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

Ut=np.zeros((400,200))
for i in range (0,400):
    for j in range (0,200):
      c=Ux_i[i][j]
      if str(c)=='[nan]':
          Ut[i][j]=0
      else:
          Ut[i][j]=c[0]



fig = plt.figure(figsize=(3.2, 1.7), dpi=500)
levels = np.arange(-0.2, 1.21,0.01)     
a=plt.contourf(xinterp/2, yinterp/2, Ut/0.075, cmap='rainbow', levels=levels)

plt.xticks([])
plt.ylim(0,1.5/2)
plt.xlim(0,2/2)
plt.yticks([0,0.5/2,1/2,1.5/2],fontsize=10)
plt.plot([0.74/2,0.74/2],[0,1.5/2],'-.',color='black',linewidth=1.5)
plt.plot([1.48/2,1.48/2],[0,1.5/2],'-.',color='black',linewidth=1.5)

q=plt.quiver(xi[::7]/2, yi[::7]/2, Ux_i[::7,::7,0], Uy_i[::7,::7,0],width=0.008,scale=1.1,headwidth=2)

points= (pd.read_csv("../../../FigData/Fig5/Fig5a/exp35.csv", header = None)).values
x_pixel=points[:,1]
y_pixel=points[:,0]
xdye=(x_pixel-1162)/884-1
ydye=(3000-y_pixel)/884

xdye=np.append(xdye[::-1],[1.25,0.02])
ydye=np.append(ydye[::-1],[0.02,1])
plt.scatter(xdye/2,ydye/2,marker='*',color='red')



plt.text(0.65/2,1.6/2,'0.5${x}_{p}$')
plt.text(1.45/2,1.6/2,'${x}_{p}$')



# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html