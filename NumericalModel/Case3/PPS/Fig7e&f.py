"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
import pandas as pd

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '300'
if loadData==1:
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')
    U = readvector(sol, timename, 'UMean')
    T = readscalar(sol, timename, 'TMean')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

Xplane=1.97
H0=0.08
H=-Xplane*tan(8/180*pi)-H0
dx=0.01
# Number of division for linear interpolation
ngridy = 401
ngridz = 100
# Interpolation grid dimensions
yinterpmin = -2
yinterpmax = 2
zinterpmin = H
zinterpmax = 0

# Interpolation grid
yi = np.linspace(yinterpmin, yinterpmax, ngridy)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
yinterp, zinterp = np.meshgrid(yi, zi)




Iplane=np.where(np.logical_and(x>=Xplane-dx,x<=Xplane+dx))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((y[Iplane], z[Iplane]), T[Iplane], (yinterp, zinterp), method='linear')
    Uy_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[1, Iplane]), (yinterp, zinterp), method='linear')
    Uz_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (yinterp, zinterp), method='linear')
    Ux_i = griddata((y[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (yinterp, zinterp), method='linear')



plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

B0=2
plt.figure(figsize=(2.8, 1.1),dpi=300)
levels = np.arange(0, 1.02, 0.01)
a=plt.contourf(yinterp/B0, zinterp/(H0), T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
#plt.contour(yinterp, zinterp, T_i/(8e-4),[0.05])
cb=plt.colorbar(a,ticks=np.arange(0,1.01,0.5),aspect=7.5)
cb.ax.tick_params(labelsize=10)
cb.set_label(label=r'$\overline{R}$/${R}_{0}$')
# Calculation of the streamline width as a function of the velociy magnitude
plt.ylim(H/H0,0)
plt.xlim(0.75,-0.75)
plt.xlabel('${y}/{B}_{0}$')
plt.ylabel('$z/{H}_{0}$')
plt.yticks([-4,-2,0],fontsize=10)
plt.xticks([0.5,0,-0.5],fontsize=10)
plt.show()




# Plots the contour of U
Ut=np.zeros((100,401))
for i in range (0,100):
    for j in range (0,401):
      c=Ux_i[i][j]
      if str(c)=='[nan]':
          Ut[i][j]=0
      else:
          Ut[i][j]=c[0]
print('U(min)= %f ' % Ut.min())
print('U(max)= %f ' % Ut.max())
levels = np.arange(-0.2,1.21,0.01)   

  
plt.figure(figsize=(2.85, 1.15),dpi=300)
a=plt.contourf(yinterp/B0, zinterp/(H0), Ut/0.075, cmap='rainbow', levels=levels)
cb=plt.colorbar(a,ticks=np.arange(-0.1,1.21,0.4),aspect=8,label=r'$\overline{u}$/${U}_{0}$')
cb.ax.tick_params(labelsize=10)
cb.set_label(label=r'$\overline{u}$/${U}_{0}$')
V=np.zeros((100,401))
W=np.zeros((100,401))
for i in range (0,100):
    for j in range (0,401):
      c=Uy_i[i][j]
      c2=Uz_i[i][j]
      if str(c)=='[nan]':
          V[i][j]=0
          W[i][j]=0
      else:
          W[i][j]=c2[0]
          V[i][j]=-c[0]
Uall = np.sqrt(V**2 + W**2)
lw = pow(Uall, 1.5)/Uall.max()
q=plt.quiver(yinterp[0][::12]/B0, zinterp[:,0][::12]/(H0), V[::12,::12],  W[::12,::12],width=0.008,scale=0.3,headwidth=2,color='black')
quiverkey(q, X=0.9, Y=-0.3, U=0.01875,
            label='= 0.25${U}_{0}$', labelpos='E')

plt.ylim(H/H0,0)
plt.xlim(0.75,-0.75)
plt.xlabel('$y/B_{0}$')
plt.ylabel('$z/{H}_{0}$')
plt.yticks([-4,-2,0],fontsize=10)
plt.xticks([0.5,0,-0.5],fontsize=10)
plt.show()




