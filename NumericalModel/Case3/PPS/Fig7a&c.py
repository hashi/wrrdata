"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
from pylab import *

sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1
color=['b','yellow','pink','g','r','purple','orange']

if loadMesh==1:
    x, y, z = readmesh(sol)

for i in range (300,330,60):
    timename = str(i)
    if loadData==1:
        U = readvector(sol, timename, 'UMean')
        T = readscalar(sol, timename, 'TMean')
        
    ngridx = 401
    ngridz = 80

# Interpolation grid dimensions
    xinterpmin = 0.
    xinterpmax = 4
    zinterpmin = -0.65
    zinterpmax = 0

# Interpolation grid
    xi = np.linspace(xinterpmin, xinterpmax, ngridx)
    zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
    xinterp, zinterp = np.meshgrid(xi, zi)

    Yplane=0
    dy=0.01

    Iplane=np.where(np.logical_and(y>=Yplane-dy,y<=Yplane+dy))

    if interpolate==1:
    # Interpolation of scalra fields and vector field components
        T_i = griddata((x[Iplane], z[Iplane]), T[Iplane], (xinterp, zinterp), method='linear')
        Ux_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (xinterp, zinterp), method='linear')
        Uz_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (xinterp, zinterp), method='linear')

for i in range (0,len(T_i)):
    for j in range (0,len(T_i[0])):
       if zinterp[i][j]<(-0.08-xinterp[i][j]*tan(8/180*pi)):
          T_i[i][j]=np.nan
          Ux_i[i][j]=np.nan
          Uz_i[i][j]=np.nan
          

# Plots the contour of sediment concentration
fig = plt.figure(figsize=(2.9, 1.1), dpi=500)
plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

H=-(0.08+tan(8/180*np.pi)*xinterpmax)
H0=0.08
xs=np.array([0,4])
ys=np.array([-0.08,-0.642])
plt.plot(xs/2,ys/H0,c='black')
levels = np.arange(0,1.05, 0.01)
a=plt.contourf(xinterp/2, zinterp/(H0), T_i/(8e-4), cmap=plt.cm.Reds, levels=levels)
cb=plt.colorbar(a,ticks=np.arange(0,1.01,0.5),aspect=8,label=r'$\overline{R}$/${R}_{0}$')

cb.ax.tick_params(labelsize=10)
cb.set_label(label=r'$\overline{R}$/${R}_{0}$')

plt.xlabel('${x}/{B}_{0}$')
plt.ylabel('${z}/{H}_{0}$')
plt.xlim(0,1.2)
plt.ylim((-2.4*np.tan(8/180*np.pi)-0.08)/0.08,0)
plt.yticks([0,-2.5,-5],fontsize=10)
plt.xticks([0,0.4,0.8,1.2],fontsize=10)
plt.show()


# Plots the contour of U
fig = plt.figure(figsize=(2.85, 1.05), dpi=300)
plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

Ut=np.zeros((80,401))
for i in range (0,80):
    for j in range (0,401):
       c=Ux_i[i][j]
       if str(c)=='[nan]':           
          Ut[i][j]=np.nan
       else:
          Ut[i][j]=c[0]

levels = np.arange(-0.1, 1.10,0.01)     

a=plt.contourf(xinterp/2, zinterp/(H0), Ut/0.075, cmap='rainbow', levels=levels)
W=np.zeros((80,401))
for i in range (0,80):
    for j in range (0,401):
      c2=Uz_i[i][j]
      if str(c2)=='[nan]':
          W[i][j]=0
      else:
          W[i][j]=c2[0]
Uall = np.sqrt(Ut**2 + W**2)
lw = pow(Uall, 1.5)/Uall.max()
q=plt.quiver(xinterp[0][::10]/2, zinterp[:,0][::4]/H0, Ut[::4,::10],  W[::4,::10],width=0.008,scale=1.2,headwidth=2,color='black')
quiverkey(q, X=0.85, Y=-0.35, U=0.075,
            label='= ${U}_{0}$', labelpos='E')


plt.plot(xs/2,ys/(H0),c='black')
cb=plt.colorbar(a,ticks=np.arange(-0.,1.01,0.5),aspect=8,label='$\overline{u}/{U}_{0}$')

cb.ax.tick_params(labelsize=10)
cb.set_label(label='$\overline{u}/{U}_{0}$')


plt.yticks([0,-2.5,-5],fontsize=10)
plt.xticks([0,0.4,0.8,1.2],fontsize=10)
plt.xlim(0,1.2)
plt.ylim((-2.4*tan(8/180*pi)-0.08)/0.08,0)
plt.xlabel('${x}/{B}_{0}$')
plt.ylabel('${z}/{H}_{0}$')
#plt.plot([1.02,1.02],[-1.02*np.tan(8/180*np.pi)-0.08,0],'-.',color='blue')
plt.plot([0.74/2,0.74/2],[(-0.75*np.tan(8/180*np.pi)-0.08)/H0,0],'-.',color='black',linewidth=1)
plt.plot([1.48/2,1.48/2],[(-1.5*np.tan(8/180*np.pi)-0.08)/H0,0],'-.',color='black',linewidth=1)
plt.plot([1.97/2,1.97/2],[(-1.97*np.tan(8/180*np.pi)-0.08)/H0,0],'-.',color='black',linewidth=1)
plt.text(0.6/2,0.02/H0,'$0.5x_{p}$')
plt.text(1.4/2,0.02/H0,'$x_{p}$')
plt.text(1.9/2,0.02/H0,'$x_{ud}$')
plt.show()




