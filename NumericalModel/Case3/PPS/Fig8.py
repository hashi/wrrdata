"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from pylab import *
#from pylab import *
#import matplotlib.gridspec as gridspec


sol = '../'

loadMesh = 1
loadData = 1
interpolate = 1

if loadMesh==1:
    x, y, z = readmesh(sol)

###############################################################################
# Reads vector and scalar field
# -----------------------------
#
# .. note:: It reads vector and scalar field from an unstructured mesh
#           and stores them in vel and phi variables


timename = '300'
if loadData==1:
    U = readvector(sol, timename, 'UMean')
    T = readscalar(sol, timename, 'TMean')
    #U = readvector(sol, timename, 'U')
    #T = readscalar(sol, timename, 'T')

###############################################################################
# Interpolate the fields on a structured grid
# -------------------------------------------
#
# .. note:: The vector and scalar fields are interpolated on a specified
#           structured grid

# import griddata from scipy package

# Number of division for linear interpolation
ngridx = 401
ngridz = 80

# Interpolation grid dimensions
xinterpmin = 0.
xinterpmax = 4
zinterpmin = -0.642
zinterpmax = 0

# Interpolation grid
xi = np.linspace(xinterpmin, xinterpmax, ngridx)
zi = np.linspace(zinterpmin, zinterpmax, ngridz)

# Structured grid creation
xinterp, zinterp = np.meshgrid(xi, zi)

Yplane=0
dy=0.01

Iplane=np.where(np.logical_and(y>=Yplane-dy,y<=Yplane+dy))

if interpolate==1:
    # Interpolation of scalra fields and vector field components
    T_i = griddata((x[Iplane], z[Iplane]), T[Iplane], (xinterp, zinterp), method='linear')
    Ux_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[0, Iplane]), (xinterp, zinterp), method='linear')
    Uz_i = griddata((x[Iplane], z[Iplane]), np.transpose(U[2, Iplane]), (xinterp, zinterp), method='linear')


###############################################################################
# Plots the contour of the interpolted scalarfield phi, streamlines and a patch 
# -----------------------------------------------------------------------------
#
# .. note:: The scalar field phi reprensents the concentration of sediment in
#           in a 2D two-phase flow simulation of erosion below a pipeline

#find boundary of plunge region and underflow region based on U
XprofileList2=np.arange(0,401,1)
m=0

Us=np.zeros((401,80))

detaz=0.642/80




Ut=np.zeros((80,401))
for i in range (0,80):
    for j in range (0,401):
      c=Ux_i[i][j]
      if str(c)=='[nan]' or zinterp[i][j]<(-0.08-xinterp[i][j]*tan(8/180*pi)):
          Ut[i][j]=0
          
      else:
          Ut[i][j]=c
       
          
for Xprofile in XprofileList2:
    for j in range (0,80):
        if (Ut[j][Xprofile])>0*max(Ut[:,Xprofile]):
            Us[m][j]=(Ut[j][Xprofile])
        else:
            Us[m][j]=0
        Us[m][79]=Us[m][78]

    m=m+1
U2all=np.zeros(400)
Uall=np.zeros(400)
xm=np.zeros(400)
hc=np.zeros(400)
for m in range (0,400):
    for j in range (0,80):
        U2all[m]+=(Us[m][j])**2*detaz
        Uall[m]+=(Us[m][j])*detaz
    hc[m]=(Uall[m])**2/U2all[m]
    
for n in range (0,400):
    xm[n]=0.01*n 
    
hc_u=hc[:200]   
for n in range (0,200):
    if hc_u[n]==max(hc_u):
        num=n
hp=max(hc)

for n in range (150,250):
    if hc[n]==min(hc[100:250]):
        num2=n

def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys

hcs=smooth(hc,2)
hc_u=hcs[:200]   
for n in range (0,200):
    if hc_u[n]==max(hc_u):
        num=n
for n in range (150,200):
    if hc[n]==min(hc[100:200]):
        num2=n

fig, ax1 = plt.subplots(figsize=(3,2),dpi=300)
plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}
H0=0.08

ax1.plot(xm/2,hcs/H0,'-.',color='black')
ax1.set_xlim(0,1.04)
ax1.set_ylim(0,4.2)
plt.scatter([xm[num]/2],[hcs[num]/H0],marker='x',color='red')
plt.scatter([xm[num2]/2],[hcs[num2]/H0],marker='x',color='blue')
plt.text(1.48/2,0.3/H0,'${x}_{p}$')
plt.text(1.9/2,0.20/H0,'${x}_{ud}$')

ax1.set_xlabel('$x/{B}_{0}$')
ax1.set_ylabel('$h_{c}/{H}_{0}$')
ax1.set_yticks([0,1,2,3,4])
ax1.tick_params(labelsize=10)
#plt.text(1.05,0.37,'(a)')








# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html