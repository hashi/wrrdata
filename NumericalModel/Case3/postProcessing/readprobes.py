import numpy as np
import fluidfoam
from pylab import *
import matplotlib.gridspec as gridspec

import pandas as pd 


#
# Change fontsize
#
matplotlib.rcParams.update({'font.size': 20})
mpl.rcParams['lines.linewidth'] = 3
mpl.rcParams['lines.markersize'] = 5
mpl.rcParams['lines.markeredgewidth'] = 1
#
# Change subplot sizes
#
gs = gridspec.GridSpec(4,1)
gs.update(left=0.1, right=0.95, top=0.95,
          bottom=0.1, wspace=0.125, hspace=0.25)
#
# Figure size
#
figwidth = 10
figheight = 10



timeT,probesT=fluidfoam.readpostpro.readprobes('../', probes_name='probes', time_name='210', name='T')
timeU, probesU=fluidfoam.readpostpro.readprobes('../', probes_name='probes', time_name='210', name='U')

#
# Plot sections
#
figure(num=1, figsize=(figwidth, figheight),
       dpi=60, facecolor='w', edgecolor='w')

Noprobe=4 ###which probe you want to look at?

points = pd.read_csv('points', sep="\n", header=None)

print('The location of the selected point is:')
print(points[0][Noprobe])
#plot density difference
ax1 = subplot(gs[0, 0])
R=probesT[:,Noprobe,0]
ax1.plot(timeT[:]-210, R)
ax1.set_ylabel('$R_{0}$')
ax1.set_xlim(np.min(timeT), np.max(timeT))
plt.ylim(0,8e-4)
#plt.yticks([0,1e-3,8e-4])
ax1.set_xticklabels([''])
plt.xlim(0,50)

#plot u
ax2 = subplot(gs[1, 0])
u=probesU[:,Noprobe,0]*100
ax2.plot(timeU[:]-210, u)
ax2.set_ylabel(r'$u(cm/s)$')
ax2.set_xlim(np.min(timeT), np.max(timeT))
ax2.set_xticklabels([''])

plt.ylim(0,6)
plt.xlim(0,40)

#plotv
ax3 = subplot(gs[2, 0])
v=probesU[:,Noprobe,1]
ax3.plot(timeU[:]-210, v)
ax3.set_ylabel(r'$v(m/s)$')
ax3.set_xlim(np.min(timeT), np.max(timeT))
ax3.set_xticklabels([''])

#plt.ylim(-0.02,0)
plt.xlim(0,40)

#plotw
ax4 = subplot(gs[3, 0])
w=probesU[:,Noprobe,2]
ax4.plot(timeU[:]-210, w)
ax4.set_ylabel(r'$w(m/s)$')
ax4.set_xlim(np.min(timeT), np.max(timeT))
ax4.set_xlabel(r't (s)')

#plt.ylim(-1e-3,1e-3)
plt.xlim(0,40)

plt.show()

#calculate direction

#first point

averu=np.mean(u)
averv=np.mean(v)
detau=np.zeros(len(u))
detav=np.zeros(len(u))
direc=np.zeros(len(u))
for i in range(0,len(u)):
    detau[i]=u[i]-averu
    detav[i]=v[i]-averv
for i in range(0,len(u)):
    if (detau[i]>=0) & (detav[i]>=0):
       direc[i]=arctan(detav[i]/detau[i])*180/np.pi 
    elif (detau[i]<0) & (detav[i]>=0):
        direc[i]=180-(arctan(-detav[i]/detau[i])*180/np.pi)   
    elif (detau[i]<=0) & (detav[i]<0):
        direc[i]=180+(arctan(detav[i]/detau[i])*180/np.pi)
    elif (detau[i]>0) & (detav[i]<0):
        direc[i]=360-(arctan(-detav[i]/detau[i])*180/np.pi)
plt.plot(timeU[:]-210,direc)
plt.xlabel('t (s)',fontsize=14)
plt.ylabel('Direction',fontsize=14)
plt.tick_params(axis='both',which='major',labelsize=14)
ax=plt.gca()


plt.xlim(0,40)
plt.ylim(0,360)



#######################################
########FFT energy spectrum
#######################################
from scipy import signal
fs=50
f_x, Pxx_x = signal.periodogram(u, fs, scaling='density')
fig, ax = plt.subplots()
ax.loglog(f_x, Pxx_x, color='orange')

plt.ylim(1e-9,1e3)

#plot -5/3 law
m=0
for i in range (0,len(f_x)):
    if f_x[i]>3:
        m+=1
x_x=np.zeros(m)
y_y=np.zeros(m)
j=0
for i in range (0,len(f_x)):
    if f_x[i]>3:
        x_x[j]=f_x[i]
        y_y[j]=(f_x[i])**(-5/3)/(10**6)
        j+=1
ax.loglog(x_x, y_y, color='black')  
#find main frequency value    
for i in range (0,len(f_x)):
    if f_x[i]<0.1:
       Pxx_x[i]=0
for i in range (0,len(f_x)):       
    if Pxx_x[i]==max(Pxx_x):
        print('The main period calculated from FFT is:')
        print(1/f_x[i])
plt.xlabel('frequency',fontsize=14)
plt.ylabel('PSD (${cm^2}$/${s^2}$)',fontsize=14)
plt.tick_params(axis='both',which='major',labelsize=14)
plt.show()

#############################################
##########wavelet analysis
#############################################
import pycwt as wavelet
u_fluc=u-averu
u_norm = u_fluc / (u_fluc.std())
mother = wavelet.Morlet(6)
dt = 0.02  # in seconds
s0 = 2 * dt  # Starting scale
dj = 0.05 # Twelve sub-octaves per octaves


    
N=len(u_fluc)
J = 1/dj*log2(N*dt/s0)

wave, scales, _, coi, _, _ = wavelet.cwt(u_norm,
                                         dt,
                                         dj,
                                         s0,
                                         J,
                                         mother)

power = (np.abs(wave)) ** 2
power /= scales[:, None]


scale_grid, time_grid = np.meshgrid(scales,
                                    timeU[:])
plt.show()

plt.rcParams.update({'font.size': 12,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :12,
}
levels=np.arange(0,1.01,0.01)
fig = plt.figure(figsize=(4,2),dpi=500)
a=plt.contourf(time_grid-210,scale_grid,power.T/np.max(power),cmap='coolwarm',levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),aspect=10,label='power')
plt.xlim(0,40)
plt.ylim(1,15)
plt.xlabel('Time $(s)$')
plt.ylabel('Period ($s$)')
plt.text(30,13,'(6k,3)')
plt.show()

#############################################
##########wavelet analysis using RDD
#############################################
import pycwt as wavelet
R_fluc=R-np.mean(R)
R_norm = R_fluc / (R_fluc.std())
mother = wavelet.Morlet(6)
dt = 0.02  # in seconds
s0 = 2 * dt  # Starting scale
dj = 0.05 # Twelve sub-octaves per octaves


    
N=len(R_fluc)
J = 1/dj*log2(N*dt/s0)

wave, scales, _, coi, _, _ = wavelet.cwt(R_norm,
                                         dt,
                                         dj,
                                         s0,
                                         J,
                                         mother)

power = (np.abs(wave)) ** 2
power /= scales[:, None]


scale_grid, time_grid = np.meshgrid(scales,
                                    timeU[:])
plt.show()

plt.rcParams.update({'font.size': 12,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :12,
}
levels=np.arange(0,1.01,0.01)
fig = plt.figure(figsize=(4,2),dpi=500)
a=plt.contourf(time_grid-210,scale_grid,power.T/np.max(power),cmap='coolwarm',levels=levels)
plt.colorbar(a,ticks=np.arange(0,1.01,0.25),aspect=10,label='power')
plt.xlim(0,40)
plt.ylim(1,15)
plt.xlabel('Time $(s)$')
plt.ylabel('Period ($s$)')
plt.text(30,13,'(6k,3)')
