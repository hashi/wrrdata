Data reported in the manuscript "Unconfined plunging of a hyperpycnal river plume over a sloping bed and its lateral spreading: Laboratory experiments and numerical modeling" can be downloaded here. 

In order to run the numerical model successufully, the following softwares are necessary:

(1) OpenFOAM with paraview  (I am using OpenFOAMv1912)
(2) swak4foam

In order to run the python codes to do the postprocessing, the following softwares are necessary:

(1) anaconda
(2) matplotlib
(3) fluidfoam

###other packages required for specific python files are identified in corresponding python files 
