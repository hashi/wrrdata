#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:32:42 2020

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate
from pylab import * 
from scipy.interpolate import griddata

def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys




markers=['<','o','s','>','<','o','s','>','o','o','<','o','s','>']

labels=['(3k, 2)','(6k, 2)','(9k, 2)','(12k, 2)','(3k, 3)','(6k, 3)','(9k, 3)','(12k, 3)','(6k, 4)','(6k, 5)','(3k, 6)','(6k, 6)',
        '(9k, 6)', '(12k, 6)']
plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

fig = plt.figure(figsize=(5,3),dpi=500)



#################
####flat case1

x = (pd.read_csv("./case16/x95.csv", header = None)).values

y = (pd.read_csv("./case16/y95.csv", header = None)).values

plt.plot(x/2,y/2,'--',color='blue',label='$\u03B2$ = 0\u00B0')





x = (pd.read_csv("./case3/x95.csv", header = None)).values
y = -(pd.read_csv("./case3/y95.csv", header = None)).values

plt.plot(x/2,y/2,'--',color='red',label='$\u03B2$ = 8\u00B0')





x = (pd.read_csv("./case15/x95.csv", header = None)).values
y = -(pd.read_csv("./case15/y95.csv", header = None)).values
 
plt.plot(x/2,y/2,'--',color='green',label='$\u03B2$ = 4\u00B0')



#################
##Analytical model

U0=0.0925
RDD=2e-3
g=9.81
B0=2
H0=0.08

xc=np.linspace(0,4.67,61)
yc=B0/2-xc*(RDD*g*H0)**0.5/2/U0
plt.plot(xc/2,yc/2,'-.',color='black',label='Eq. 9')
plt.plot(xc/2,-yc/2,'-.',color='black')

plt.legend(frameon=0,ncol=2,columnspacing=0.5,labelspacing=0.4,borderaxespad=0.2,handletextpad=0.2,fontsize=10)
plt.xlabel('$x/{B}_{0}$')
plt.ylabel('$y/{B}_{0}$')
#plt.text(0.18,-0.12,'(a)',fontsize=22)
plt.ylim(-0.6,0.6)
plt.xticks([0,0.5,1,1.5,2,2.5],fontsize=10)
plt.yticks([-0.5,-0.25,0,0.25,0.5],fontsize=10)
plt.xlim(0,2.5)
plt.show()



