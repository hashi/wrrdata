#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:32:42 2020

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate



plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}



fig = plt.figure(figsize=(3.7, 1.8), dpi=300)
#################
###case1
#################

xexp=(pd.read_csv("./case1/EXP/x.csv", header = None)).values
Uexp=(pd.read_csv("./case1/EXP/U.csv", header = None)).values
plt.scatter(xexp[::10]/2,Uexp[::10],c='',edgecolor='red',s=15,label='(1, 0.08, 2)')

xnum=(pd.read_csv("./case1/NUM/x.csv", header = None)).values
Unum=(pd.read_csv("./case1/NUM/U.csv", header = None)).values

plt.plot(xnum/2,Unum,'--',color='red',label='(1, 0.08, 2)',linewidth=1.5)

#################
###case2
#################

xexp=(pd.read_csv("./case2/EXP/x.csv", header = None)).values
Uexp=(pd.read_csv("./case2/EXP/U.csv", header = None)).values
plt.scatter(xexp[::10]/2,Uexp[::10],marker='s',c='',edgecolor='blue',s=15,label='(2, 0.13, 2)')

xnum=(pd.read_csv("./case2/NUM/x.csv", header = None)).values
Unum=(pd.read_csv("./case2/NUM/U.csv", header = None)).values
plt.plot(xnum/2,Unum,'-.',color='blue',label='(2, 0.13, 2)',linewidth=1.5)
###############################
plt.xlim(-0.09,0.66)
plt.ylim(-0.1,1.5)
plt.ylabel(r'$\overline{u}$/${U}_{0}$')
#plt.text(0.7,1.6,"${x}_{up}$")
#plt.text(0.05,1.3,'(a)')
plt.xticks([0,0.2,0.4,0.6],fontsize=10)
plt.yticks([0,0.5,1,1.5],fontsize=10)
plt.xlabel('$x/{B}_{0}$')
plt.text(0.32,1.3,'(Case, ${Fr}_{0}$, ${Fr}_{d-0}$)',fontsize=9)
plt.legend(loc=(0.37,0.6),fontsize=9,frameon=0,ncol=2,labelspacing=0.2,handletextpad=0.2,columnspacing=0.3)

plt.plot([0.4775,0.4775],[-0.1,0.7],linestyle='dotted',color='black',linewidth=1.5)
plt.text(0.46,0.76,'${x}_{p}$',fontsize=9)
plt.show()


fig = plt.figure(figsize=(3.7, 1.8), dpi=300)
#################
###case3
#################

xexp=(pd.read_csv("./case3/EXP/x.csv", header = None)).values
Uexp=(pd.read_csv("./case3/EXP/U.csv", header = None)).values
plt.scatter(xexp[::15]/2,Uexp[::15],c='',edgecolor='red',s=15,label='(3, 0.08, 3)')

xnum=(pd.read_csv("./case3/NUM/x.csv", header = None)).values
Unum=(pd.read_csv("./case3/NUM/U.csv", header = None)).values

plt.plot(xnum/2,Unum,'--',color='red',label='(3, 0.08, 3)',linewidth=1.5)

#################
###case4
#################

xexp=(pd.read_csv("./case4/EXP/x.csv", header = None)).values
Uexp=(pd.read_csv("./case4/EXP/U.csv", header = None)).values
plt.scatter(xexp[::15]/2,Uexp[::15],marker='s',c='',edgecolor='blue',s=15,label='(4, 0.13, 3)')

xnum=(pd.read_csv("./case4/NUM/x.csv", header = None)).values
Unum=(pd.read_csv("./case4/NUM/U.csv", header = None)).values
plt.plot(xnum/2,Unum,'-.',color='blue',label='(4, 0.13, 3)',linewidth=1.5)
###############################


plt.xlim(-0.09,1.05)
plt.ylim(-0.1,1.5)
plt.ylabel(r'$\overline{u}$/${U}_{0}$')

#plt.text(0.7,1.6,"${x}_{up}$")
#plt.text(0.05,1.3,'(a)')
plt.xticks([0,0.3,0.6,0.9],fontsize=10)
plt.yticks([0,0.5,1,1.5],fontsize=10)

plt.plot([0.71,0.71],[-0.1,0.7],linestyle='dotted',color='black',linewidth=1.5)
plt.text(0.69,0.76,'${x}_{p}$',fontsize=9)
#plt.plot([0.74,0.74],[-0.1,0.7],linestyle='dotted',color='black',linewidth=1.5)
#plt.plot([0.68,0.68],[-0.1,0.7],linestyle='dotted',color='black',linewidth=1.5)
plt.xlabel('$x/{B}_{0}$')
plt.text(0.55,1.3,'(Case, ${Fr}_{0}$, ${Fr}_{d-0}$)',fontsize=9)
plt.legend(loc=(0.37,0.6),fontsize=9,frameon=0,ncol=2,labelspacing=0.2,handletextpad=0.2,columnspacing=0.3)
plt.show()
