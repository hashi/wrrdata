#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:32:42 2020

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate
 


from pylab import *


x = (pd.read_csv("x.csv", header = None)).values
y = (pd.read_csv("y.csv", header = None)).values
U = (pd.read_csv("U.csv", header = None)).values
V = (pd.read_csv("V.csv", header = None)).values
#numc = (pd.read_csv("Numcontour.csv", header = None)).values



fig = plt.figure(figsize=(5.9,3),dpi=500)

levels = np.arange(-0.2,1.21,0.01)
a=plt.contourf(x/100/2,y/100/2,U,cmap='rainbow', levels=levels)
q=plt.quiver(x[13::17,4::17]/100/2, y[13::17,4::17]/100/2, U[13::17,4::17], V[13::17,4::17],width=0.006,scale=20,headwidth=2)
quiverkey(q, X=0.9, Y=-0.22, U=1,label='= ${U}_{0}$', labelpos='E')
plt.plot([0.74/2,0.74/2],[-1.5/2,0],'-.',color='black',linewidth=1.5)
plt.plot([1.48/2,1.48/2],[-1.5/2,0],'-.',color='black',linewidth=1.5)
plt.plot([1.97/2,1.97/2],[-1.5/2,0],'-.',color='black',linewidth=1.5)
    
plt.xlabel('${x}_{s}/{B}_{0}$',color='black')
#plt.ylabel('$y$ (m)')
plt.xlim(0.1/2,3/2)
plt.xticks([0.5/2,1.0/2,1.5/2,2.0/2,2.5/2,3.0/2])
plt.ylim(-1.5/2,0)

cb=plt.colorbar(a,ticks=np.arange(-0.1,1.21,0.3),aspect=30, pad=0.22, orientation="horizontal")
cb.ax.tick_params(labelsize=10)
cb.set_label(label=r'${\overline{u}}_{s}$/${U}_{0}$',fontsize=11)

#yticks = [-1.5, -1.0, -0.5, 0]
yticks = [0,-0.5/2,-1/2,-1.5/2]
plt.yticks(yticks,fontsize=10)
#plt.plot(numc[:,0]*100,numc[:,1]*100)
plt.xticks([0.5/2,1/2,1.5/2,2/2,2.5/2,3/2],fontsize=10)
plt.text(-0.3/2,0.15/2,'$y/{B}_{0}$',rotation=90)

plt.show()


