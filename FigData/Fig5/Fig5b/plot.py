#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code is used to plot Fig5b.

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate
from pylab import *

x = (pd.read_csv("x.csv", header = None)).values
y = (pd.read_csv("y.csv", header = None)).values
U = (pd.read_csv("U.csv", header = None)).values   ##normalized by U0
V = (pd.read_csv("V.csv", header = None)).values   ##normalized by U0
#numc = (pd.read_csv("Numcontour.csv", header = None)).values

plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

fig = plt.figure(figsize=(3.2, 2.8), dpi=500)
levels = np.arange(-0.2,1.21,0.01)
a=plt.contourf(x/100/2,y/100/2,U,cmap='rainbow', levels=levels)
#contour=plt.contour(x, y, U,[0.5*12],linewidths=1,colors='red')
cb=plt.colorbar(a,ticks=np.arange(-0.1,1.11,0.3),aspect=20,pad=0.25,orientation='horizontal')
cb.ax.tick_params(labelsize=10)
cb.set_label(label=r'$\overline{u}$/${U}_{0}$',fontsize=11)


plt.xlabel('$x/{B}_{0}$')
plt.xlim(0.6/2,2/2)
plt.xticks([0.6/2,1.0/2,1.5/2,2.0/2],fontsize=10)

#plt.ylabel('$y$ (m)')
plt.ylim(-1.5/2,0)
plt.yticks([-1.5/2,-1/2,-0.5/2,0],fontsize=10)
#plt.plot(numc[:,0]*100,numc[:,1]*100)
plt.plot([0.74/2,0.74/2],[-1.5/2,0],'-.',color='black',linewidth=1.5)
plt.plot([1.48/2,1.48/2],[-1.5/2,0],'-.',color='black',linewidth=1.5)
q=plt.quiver(x[12::14,1::14]/100/2, y[12::14,1::14]/100/2, U[12::14,1::14], V[12::14,1::14],width=0.008,scale=15,headwidth=2)
quiverkey(q, X=0.8, Y=-0.24, U=1,
            label='= ${U}_{0}$', labelpos='E')
plt.text(0.14,0.1/2,'$y/{B}_{0}$',rotation=90)
#plt.contour(x/100,y/100,U/8.2,[0.6])
plt.show()


