#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

This code is used to plot Fig.5a.

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate
 
from fluidfoam import readmesh
from scipy.interpolate import griddata

import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt

from pylab import *
import matplotlib.gridspec as gridspec
x = (pd.read_csv("x.csv", header = None)).values
y = (pd.read_csv("y.csv", header = None)).values
U = (pd.read_csv("U.csv", header = None)).values   ###normalized by U0
V = (pd.read_csv("V.csv", header = None)).values   ###normalized by U0
#numc = (pd.read_csv("Numcontour.csv", header = None)).values


rc = {'font.size': 11,
      "font.family" : "serif", 
      "mathtext.fontset" : "stix"}
plt.rcParams.update(rc)
plt.rcParams["font.serif"] = ["Times New Roman"] + plt.rcParams["font.serif"]
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'

fig = plt.figure(figsize=(3.2, 2.8), dpi=500)
levels = np.arange(-0.2,1.21,0.01)
a=plt.contourf(x/100/2,y/100/2,U,cmap='rainbow', levels=levels)

#contour=plt.contour(x, y, U,[0.5*12],linewidths=1,colors='red')
cb=plt.colorbar(a,ticks=np.arange(-0.1,1.11,0.3),aspect=20,pad=0.25,orientation='horizontal')
cb.ax.tick_params(labelsize=10)
cb.set_label(label=r'$\overline{u}$/${U}_{0}$')


plt.xlabel('$x/{B}_{0}$')
plt.xlim(0,2/2)
plt.xticks([0,0.5/2,1/2,1.5/2,2/2],fontsize=10)
#$plt.ylabel('$2y/B_{0}$')
plt.ylim(-1.5/2,0)
plt.yticks([-1.5/2,-1/2,-0.5/2,0],fontsize=10)
q=plt.quiver(x[11::14,10::14]/100/2, y[11::14,10::14]/100/2, U[11::14,10::14], V[11::14,10::14],width=0.008,scale=15,headwidth=2)
quiverkey(q, X=0.8, Y=-0.25, U=1,
            label='= ${U}_{0}$', labelpos='E')

import pandas as pd 

points= (pd.read_csv("./exp35.csv", header = None)).values
x_pixel=points[:,1]
y_pixel=points[:,0]
xdye=(x_pixel-1162)/884-1
ydye=(3000-y_pixel)/884

xdye=np.append(xdye[::-1],[1.25,0.02])
ydye=np.append(ydye[::-1],[0.02,1])
plt.scatter(xdye/2,-ydye/2,marker='*',color='red')

plt.plot([0.74/2,0.74/2],[-1.5/2,0],'-.',color='black',linewidth=1.5)
plt.plot([1.48/2,1.48/2],[-1.5/2,0],'-.',color='black',linewidth=1.5)
plt.text(-0.21,0.1/2,'$y/{B}_{0}$',rotation=90)


plt.show()


