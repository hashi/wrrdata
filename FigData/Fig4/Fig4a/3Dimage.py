#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 15:45:16 2020

This code is used to plot Fig.4a using the Rhodaminde images.

The images have already been converted into grey scales (T_1, T_16, T_31.csv)


@author: haoran
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from pylab import *
from PIL import Image
from scipy.interpolate import griddata
from scipy.signal import savgol_filter
from openpyxl import load_workbook


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import matplotlib
import matplotlib.colors
import plotly.graph_objects as go
from plotly.offline import plot
import plotly.express as px

plt.rcParams.update({'font.size': 15,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :15,
}

x_pixel=np.linspace(0,3071,3072)
y_pixel=np.linspace(0,4095,4096)
y_cm=-231.6+(y_pixel-0)*(232+231.6)/4095
x_cm=334.2+(x_pixel-0)*(-13.54-334.2)/3071




def get_the_slice(x,y,z, surfacecolor):
    return go.Surface(x=x,
                      y=y,
                      z=z,
                      surfacecolor=surfacecolor,
                      coloraxis='coloraxis')
def get_lims_colors(surfacecolor):# color limits for a slice
    return np.min(surfacecolor), np.max(surfacecolor)

def colorax(vmin, vmax):
    return dict(cmin=vmin,
                cmax=vmax)


def readdata (levels): 
   
    C = (pd.read_csv("./T_%d.csv"%levels, header = None)).values
    C = C[::10,::10]
    C = C/np.max(C)    
    y=-231.6+(y_pixel-0)*(232+231.6)/4095
    x=334.2+(x_pixel-0)*(-13.54-334.2)/3071
    zb=-4-(levels-1)/2 
    x[x<0]= np.nan
    x[x<((-zb-8)/np.tan(8/180*np.pi)+10)]= np.nan
    x[x>250]= np.nan
    y[y<-150]=np.nan
    y[y>150]=np.nan
    y,x=np.meshgrid(-y[::10],x[::10])
    z=np.zeros((len(C),len(C[0])))
    for i in range (0,len(z)):
        for j in range (0,len(z[0])):
            z[i][j]=-4-(levels-1)/2     
    surfcolor_z = C
    slice_z = get_the_slice(x/100/1.3, y/100/1.3/1.1, z/100/0.08, surfcolor_z)
    return slice_z

Cmin, Cmax =0,1.01
fig1=go.Figure(data=[readdata(1),readdata(16),readdata(31)])
    
    
camera = dict(
    up=dict(x=-100, y=0, z=20),
    center=dict(x=0, y=0, z=0),
    eye=dict(x=-2, y=-1.8, z=1.1)
)

fig1.update_layout(title={
       
        'xanchor': 'center',
        'yanchor': 'top'},
         width=600,
         scene_camera=camera,
         height=800,
         



         scene_zaxis_range=[-3,-0],
         
         scene_xaxis_range=[0,2.10],
         scene_xaxis_nticks=0,
         scene_yaxis_range=[-1.20,1.2],
         
         scene_xaxis_title='',
         scene_yaxis_title='',
         scene_zaxis_title='',
         #coloraxis_colorbar=dict(title=''),
         coloraxis=dict(colorscale='viridis',
                        colorbar_thickness=15,
                        colorbar_len=0.5,
                        colorbar_tickmode='linear',
                        colorbar_tick0 = -0.,
                        colorbar_dtick = 0.25,
                        colorbar_ticks='inside',
                    
                        **colorax(Cmin, Cmax)),
                  font_family="Times New Roman",
         font_color="black",
         )


         
import plotly.io as pio

pio.renderers.default='svg' 

plot(fig1)






