"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from pylab import *
#from pylab import *
#import matplotlib.gridspec as gridspec
import pandas as pd


plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}
fig = plt.figure(figsize=(5, 2.5), dpi=300)

def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys


##############Frd2
x= (pd.read_csv("./Frd2/x.csv", header = None)).values
qc=(pd.read_csv("./Frd2/qc.csv", header = None)).values
qc=smooth(qc,3)
plt.scatter(x[3::4]/1,qc[3::4],marker='o',color='',edgecolor='red',label='${Fr}_{d-0}$ = 2',s=10)



########Frd3
qc=(pd.read_csv("./Frd3/qc.csv", header = None)).values
x= (pd.read_csv("./Frd3/nonx.csv", header = None)).values
qc=smooth(qc,3)
plt.scatter(x[2::6]/1.48,qc[2::6],marker='o',color='',edgecolor='orange',label='${Fr}_{d-0}$ = 3',s=10)



########Frd4
qc=(pd.read_csv("./Frd4/qc.csv", header = None)).values
x= (pd.read_csv("./Frd4/nonx.csv", header = None)).values
qc=smooth(qc,5)
plt.scatter(x[3::8]/1.9,qc[3::8],marker='o',color='', edgecolor='green',label='${Fr}_{d-0}$ = 4',s=10)

########Frd5
qc=(pd.read_csv("./Frd5/qc.csv", header = None)).values
x= (pd.read_csv("./Frd5/nonx.csv", header = None)).values
qc=smooth(qc,7)
plt.scatter(x[6::10]/2.4,qc[6::10],marker='o',color='',edgecolor='blue',label='${Fr}_{d-0}$ = 5',s=10)





##############Frd6
x= (pd.read_csv("./Frd6/nonx.csv", header = None)).values
qc=(pd.read_csv("./Frd6/qc.csv", header = None)).values
qc=smooth(qc,15)
plt.scatter(x[::12]/2.81,qc[::12],marker='o',color='',edgecolor='purple',label='${Fr}_{d-0}$ = 6',s=10)


##############Frd6
x= (pd.read_csv("./Frd6confined/nonx.csv", header = None)).values
qc=(pd.read_csv("./Frd6confined/qc.csv", header = None)).values
qc=smooth(qc,15)
plt.scatter(x[::12]/2.81,qc[::12],marker='o',color='',edgecolor='black',label='confined',s=10)


plt.plot([0,1],[1,1],'-.',color='black')
plt.xlim(0,1)
plt.ylim(0.5,4)
plt.yticks([1,2,3,4],fontsize=10)
plt.xticks([0,0.2,0.4,0.6,0.8,1],fontsize=10)
plt.xlabel('$x/{x}_{p}$')
plt.ylabel('$q_{c}/q_{c-0}$')
#plt.text(0.48,4.2,'(c)')
#plt.legend(frameon=0,loc=(0,0.65),ncol=2,labelspacing=0.3,borderaxespad=0.2,columnspacing=0.2,handletextpad=0,fontsize=9)
plt.text(0.08,3.6,'Case',fontsize=9)
plt.text(0.15,3.6,'${Fr}_{d-0}$',fontsize=9)
plt.text(0.1,3.3,'1',fontsize=9)
plt.text(0.18,3.3,'2',fontsize=9)
plt.text(0.23,3.3,'unconfined',fontsize=9)
plt.text(0.1,3.0,'3',fontsize=9)
plt.text(0.18,3.0,'3',fontsize=9)
plt.text(0.23,3.0,'unconfined',fontsize=9)
plt.text(0.1,2.7,'9',fontsize=9)
plt.text(0.18,2.7,'4',fontsize=9)
plt.text(0.23,2.7,'unconfined',fontsize=9)
plt.text(0.09,2.4,'10',fontsize=9)
plt.text(0.18,2.4,'5',fontsize=9)
plt.text(0.23,2.4,'unconfined',fontsize=9)
plt.text(0.09,2.1,'12',fontsize=9)
plt.text(0.18,2.1,'6',fontsize=9)
plt.text(0.23,2.1,'unconfined',fontsize=9)
plt.text(0.09,1.8,'21',fontsize=9)
plt.text(0.18,1.8,'6',fontsize=9)
plt.text(0.23,1.8,'confined',fontsize=9)

plt.scatter([0.04],[3.4],marker='o',color='',edgecolor='red',s=10)
plt.scatter([0.04],[3.1],marker='o',color='',edgecolor='orange',s=10)
plt.scatter([0.04],[2.8],marker='o',color='',edgecolor='green',s=10)
plt.scatter([0.04],[2.5],marker='o',color='',edgecolor='blue',s=10)
plt.scatter([0.04],[2.2],marker='o',color='',edgecolor='purple',s=10)
plt.scatter([0.04],[1.9],marker='o',color='',edgecolor='black',s=10)

# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html