"""
Read and Plot a contour of OpenFoam output from an unstructured mesh
====================================================================

This example reads and plots a contour of an OpenFoam vector field from an
unstructured mesh by interpolation on a structured grid
"""

###############################################################################
# Reads the mesh
# --------------
#
# .. note:: It reads the mesh coordinates and stores them in variables x, y
#           and z

from fluidfoam import readmesh
from scipy.interpolate import griddata
import numpy as np
from fluidfoam import readvector, readscalar
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from pylab import *
#from pylab import *
#import matplotlib.gridspec as gridspec
import pandas as pd

def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys


colors=['red','orange','green','blue','purple']

U0=0.075

fig = plt.figure(figsize=(5, 2.5), dpi=300)
plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

cases=[2,3,4,5,6]
xup=[66,130,176,204,252]

i=0
for case in cases:

   x= (pd.read_csv("./Frd%dconfined/xc.csv"%cases[i], header = None)).values
   #Uc=(pd.read_csv("./Frd6/Uc.csv", header = None)).values
   Frd=(pd.read_csv("./Frd%dconfined/Frc.csv"%cases[i], header = None)).values
   Frd=smooth(Frd,10)/1.0
   plt.scatter(x[0:xup[i]:10]/2,Frd[0:xup[i]:10],linewidth=1.5,color='',edgecolor=colors[i],s=10,label='${Fr}_{d-0}=%d$'%case)
   plt.scatter(x[xup[i]]/2,Frd[xup[i]],marker='*',s=100,color=colors[i])
   plt.plot(x[0:xup[i]]/2,Frd[0]*0.08**1.5/((x[0:xup[i]]*tan(8/180*pi)+0.08))**1.5,'--',color=colors[i])
   i=i+1
   
plt.xlim(0,1.5)
plt.ylim(0,6.5)
plt.xticks([0,0.5,1,1.5],fontsize=10)
plt.yticks([0,2,4,6],fontsize=10)
plt.xlabel('$x/{B}_{0}$')
plt.ylabel('${Fr}_{d}$')
#plt.legend(frameon=0,loc=(0.5,0.5),ncol=2,labelspacing=0.5,borderaxespad=0.4,columnspacing=0.5,handletextpad=0.5,fontsize=9)

plt.scatter([1],[5.5],marker='o',color='',edgecolor='red',s=10)
plt.scatter([1],[5],marker='o',color='',edgecolor='orange',s=10)
plt.scatter([1],[4.5],marker='o',color='',edgecolor='green',s=10)
plt.scatter([1],[4],marker='o',color='',edgecolor='blue',s=10)
plt.scatter([1],[3.5],marker='o',color='',edgecolor='purple',s=10)
plt.text(1.07,6,'Case',fontsize=9)
plt.text(1.25,6,'${Fr}_{d-0}$',fontsize=9)
#plt.plot([1.05,1.4],[5.25,5.25],color='black',linewidth=0.5)
#plt.plot([1.05,1.4],[4.75,4.75],color='black',linewidth=0.5)
#plt.plot([1.05,1.4],[4.25,4.25],color='black',linewidth=0.5)
#plt.plot([1.05,1.4],[3.75,3.75],color='black',linewidth=0.5)
#plt.plot([1.05,1.4],[3.25,3.25],color='black',linewidth=0.5)
plt.text(1.09,5.35,'17',fontsize=9)
plt.text(1.3,5.35,'2',fontsize=9)
plt.text(1.09,4.85,'18',fontsize=9)
plt.text(1.3,4.85,'3',fontsize=9)
plt.text(1.09,4.35,'19',fontsize=9)
plt.text(1.3,4.35,'4',fontsize=9)
plt.text(1.09,3.85,'20',fontsize=9)
plt.text(1.3,3.85,'5',fontsize=9)
plt.text(1.09,3.35,'21',fontsize=9)
plt.text(1.3,3.35,'6',fontsize=9)
plt.text(0.2,6,'confined cases')
plt.plot([0,3],[0.5,0.5],'--',color='grey')
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html