#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:32:42 2020

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate
from pylab import * 
from scipy.interpolate import griddata

def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys

colors=['red','orange','green','blue','purple']

rc = {'font.size': 11,
      "font.family" : "serif", 
      "mathtext.fontset" : "stix"}
plt.rcParams.update(rc)
plt.rcParams["font.serif"] = ["Times New Roman"] + plt.rcParams["font.serif"]
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'

plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

fig = plt.figure(figsize=(6,2),dpi=500)

i=0


cases=[2,3,4,5,6]
for case in cases:
    
  x = (pd.read_csv("%s/ax.csv"%case, header = None)).values
  y = (pd.read_csv("%s/az.csv"%case, header = None)).values
  y=smooth(y,20)
  plt.plot(x/2,y/0.08,'--',color=colors[i],label='${Fr}_{d-0}=%d$'%case)
  i=i+1
plt.xlim(0,1.25)
plt.ylim(-5.375,0)
plt.xticks([0,0.5/2,1/2,1.5/2,2/2,2.5/2])
plt.yticks([-5,-2.5,0])

plt.plot([0,2.5/2],[-0.08/0.08,-0.43/0.08],color='black')

plt.xlabel('$x/{B}_{0}$')
plt.ylabel('$z/{H}_{0}$')

plt.text(0.1,-2.9,'1',fontsize=9)
plt.text(0.18,-2.9,'2',fontsize=9)
plt.text(0.1,-3.4,'3',fontsize=9)
plt.text(0.18,-3.4,'3',fontsize=9)
plt.text(0.1,-3.9,'9',fontsize=9)
plt.text(0.18,-3.9,'4',fontsize=9)
plt.text(0.09,-4.4,'10',fontsize=9)
plt.text(0.18,-4.4,'5',fontsize=9)
plt.text(0.09,-4.9,'12',fontsize=9)
plt.text(0.18,-4.9,'6',fontsize=9)
plt.text(0.07,-2.3,'Case',fontsize=9)
plt.text(0.15,-2.3,'${Fr}_{d-0}$',fontsize=9)
plt.plot([0.02,0.08],[-2.75,-2.75],'--',color='red')
plt.plot([0.02,0.08],[-3.25,-3.25],'--',color='orange')
plt.plot([0.02,0.08],[-3.75,-3.75],'--',color='green')
plt.plot([0.02,0.08],[-4.25,-4.25],'--',color='blue')
plt.plot([0.02,0.08],[-4.75,-4.75],'--',color='purple')
#plt.legend(frameon=0,ncol=2,loc='lower left',fontsize=9,borderpad=0.2,handletextpad=0.2,columnspacing=0.4,labelspacing=0.3)
