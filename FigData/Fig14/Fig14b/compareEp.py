#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:32:42 2020

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate
from pylab import * 
from scipy.interpolate import griddata


plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

fig = plt.figure(figsize=(3,2),dpi=500)
######################################################
###unconfined 
Q0=12
Fr0=np.array([2,3,4,5,6])
xu=np.array([1.6,2.0,2.4,2.99,3.38])
markers=['^','s','*','o','>','*']
cases=['3DFr2','3DFr3','3DFr4','3DFr5','3DFr6']

Ep=np.zeros(5)

i=0
for case in cases:
     x = (pd.read_csv("%s/x.csv"%case, header = None)).values
     y = (pd.read_csv("%s/Ep.csv"%case, header = None)).values
     for j in range (0,len(x)):
         if x[j][0]<xu[i]<=x[j+1][0]:
            
             Qp=y[j+1]*Q0+Q0
             Q0c=y[0]*Q0+Q0
             Ep[i]=(Qp-Q0c)/Q0c             
             Ep[i]=y[j]
     i=i+1
             
plt.plot(Fr0,Ep,color='red',marker='s',mfc='None',label='unconfined')            




######################################################
###confined 
Q0=12
Fr0=np.array([2,3,4,5,6])
xu=np.array([0.93,1.86,2.48,2.96,3.56])
cases=['2DFr2','2DFr3','2DFr4','2DFr5','2DFr6']
Ep=np.zeros(5)

i=0
for case in cases:
     x = (pd.read_csv("%s/x.csv"%case, header = None)).values
     y = (pd.read_csv("%s/Ep.csv"%case, header = None)).values
     for j in range (0,len(x)):
         if x[j][0]<=xu[i]<x[j+1][0]:            
             Qp=y[j]*Q0+Q0
             Q0c=y[0]*Q0+Q0
             Ep[i]=(Qp-Q0c)/Q0c             
             Ep[i]=y[j]
     i=i+1
plt.plot(Fr0,Ep,color='blue',marker='^',mfc='None',label='confined')  
plt.ylim(0,0.8)
plt.xlabel('${Fr}_{d-0}$')
plt.ylabel('${E}$')
plt.yticks([0,0.2,0.4,0.6,0.8],fontsize=10)
plt.xticks([2,3,4,5,6],fontsize=10)
plt.legend(frameon=0,fontsize=9,labelspacing=0.3)
#plt.text(2,0.85,'(b)')