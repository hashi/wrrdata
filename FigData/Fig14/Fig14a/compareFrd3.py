#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:32:42 2020

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate
from pylab import * 
from scipy.interpolate import griddata


xu=2
xp=1.1
markers=['<','>','s','o']
cases=['case5','case3','case6','case7']
labels=['$Re$ = 3k','$Re$ = 6k',
        '$Re$ = 9k', '$Re$ = 12k']
colors=['red','orange','blue','darkgreen']
plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

fig = plt.figure(figsize=(3,2),dpi=500)

i=0
for case in cases:
    
  x = (pd.read_csv("%s/x.csv"%case, header = None)).values
  y = (pd.read_csv("%s/Ep.csv"%case, header = None)).values
  y=y-y[0]+1
  for j in range (0,len(x)):
      if x[j]>xu*1:
          x[j]=np.nan
          y[j]=np.nan
  
  
  xplot=np.zeros(len(x)+1)
  yplot=np.zeros(len(y)+1)
  for j in range (1,len(xplot)):
      xplot[j]=x[j-1]
      yplot[j]=y[j-1]
  xplot[0]=0
  yplot[0]=1
  plt.scatter(xplot[::3]/2,yplot[::3],marker=markers[i],label=labels[i],color='',edgecolor=colors[i],s=25)
  i=i+1
#plt.legend(frameon=0,loc=(-0.03,0.75),ncol=2,columnspacing=0.1,borderaxespad=0.3,handletextpad=0.05,labelspacing=0.2,fontsize=9)

plt.plot([1.48/2,1.48/2],[0,1.33],'--',color='black',linewidth=1.5)
plt.plot([1.97/2,1.97/2],[0,1.43],'--',color='black',linewidth=1.5)
plt.ylim(0.9,1.6)
plt.yticks([1,1.3,1.6],fontsize=10)
plt.xticks([0,0.5,1,1.5,2],fontsize=10)
plt.xlim(-0.05,1.05)
plt.ylabel('$Q/{Q}_{0}$')
plt.xlabel('$x/{B}_{0}$')
plt.text(1.3/2,1.1,'${x}_{p}$',rotation=90)
plt.text(1.8/2,1.1,'${x}_{ud}$',rotation=90)

plt.scatter([0.05],[1.5],marker='<',edgecolor='red',color='',s=20)
plt.scatter([0.05],[1.43],marker='>',edgecolor='orange',color='',s=20)
plt.scatter([0.05],[1.36],marker='s',edgecolor='green',color='',s=20)
plt.scatter([0.05],[1.29],marker='o',edgecolor='blue',color='',s=20)
plt.text(0.12,1.55,'Case',fontsize=9)
plt.text(0.28,1.55,'${Fr}_{0}$',fontsize=9)
plt.text(0.16,1.48,'7',fontsize=9)
plt.text(0.16,1.41,'3',fontsize=9)
plt.text(0.16,1.34,'4',fontsize=9)
plt.text(0.16,1.27,'8',fontsize=9)
plt.text(0.28,1.48,'0.04',fontsize=9)
plt.text(0.28,1.41,'0.08',fontsize=9)
plt.text(0.28,1.34,'0.13',fontsize=9)
plt.text(0.28,1.27,'0.17',fontsize=9)
#plt.text(-0.05,1.5,'(a)')

