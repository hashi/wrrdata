#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:32:42 2020

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate
from pylab import * 
from scipy.interpolate import griddata


x = (pd.read_csv("./x.csv", header = None)).values
y = (pd.read_csv("./locH.csv", header = None)).values


plt.scatter(x/1.48,y)