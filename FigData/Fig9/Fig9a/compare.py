#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code is used to plot Fig.9a

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate
from pylab import * 
from scipy.interpolate import griddata

def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys

fig = plt.figure(figsize=(5,2),dpi=500)


markers=['<','o','s','>','<','o','s','>','o','o','<','o','s','>']
cases=['case8','case9','case10','case11','case5','case3','case6','case7','case12','case13','case17','case14b','case16','case15']
labels=['(5, 0.04, 2)','(1, 0.08, 2)','(2, 0.13, 2)','(6, 0.17, 2)','(7, 0.04, 3)','(3, 0.08, 3)','(4, 0.13, 3)','(8, 0.17, 3)','(9, 0.08, 4)','(10, 0.08, 5)','(11, 0.04, 6)','(12, 0.08, 6)',
        '(13, 0.13, 6)', '(14, 0.17, 6)']
plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

fig = plt.figure(figsize=(3.8,3.5),dpi=500)

i=0

for case in cases:
    
  x = (pd.read_csv("%s/x95.csv"%case, header = None)).values/2
  y = (pd.read_csv("%s/y95.csv"%case, header = None)).values/2
  y=smooth(y,16)
  if case=='case3':
        x = (pd.read_csv("%s/x95_fix.csv"%case, header = None)).values/2
        y = -(pd.read_csv("%s/y95_fix.csv"%case, header = None)).values/2
  if 3<i<=7:
    if i==5:
      y=-y    
    plt.scatter(x[::8],y[::8],marker=markers[i],color='',edgecolor='blue',s=15,label=labels[i])
  elif i<=3:
    if i==2:
        y=-y
    plt.scatter(x[::8],y[::8],marker=markers[i],color='',edgecolor='red',s=15,label=labels[i])
  elif 13>=i>=10:
    if i==13:
        y=-y
    if i==11:
        y=-y
    plt.scatter(x[::8],y[::8],marker=markers[i],color='',edgecolor='purple',s=15,label=labels[i]) 
  elif i==8:
    plt.scatter(x[::8],y[::8],marker=markers[i],color='',edgecolor='orange',s=15,label=labels[i])  
  elif i==9:
    plt.scatter(x[::8],y[::8],marker=markers[i],color='',edgecolor='green',s=15,label=labels[i])  
  i=i+1


plt.ylim(-0.6,0.6)



##################
####plot exp

points= (pd.read_csv("./EXP/exp36.csv", header = None)).values
x_pixel=points[:,1]
y_pixel=points[:,0]
y=-231.6+(y_pixel-0)*(232+231.6)/4095
x=334.2+(x_pixel-0)*(-13.54-334.2)/3071
addx=(x[len(x)-1]+x[len(x)-2])/2
addy=(y[len(y)-1]+y[len(y)-2])/2
x=np.append(x,[addx])
y=np.append(y,[addy])

y=smooth(y,3)
plt.scatter(x/100/2,y/100/2,marker='o',s=15,color='red',label='(1, 0.08, 2)')
plt.scatter(x/100/2,-y/100/2,marker='o',s=15,color='red')


points= (pd.read_csv("./EXP/exp35.csv", header = None)).values
x_pixel=points[:,1]
y_pixel=points[:,0]
y=-231.6+(y_pixel-0)*(232+231.6)/4095
x=334.2+(x_pixel-0)*(-13.54-334.2)/3071
y=smooth(y,3)
plt.scatter(x/100/2,y/100/2,marker='o',s=15,color='blue',label='(3, 0.08, 3)')
plt.scatter(x/100/2,-y/100/2,marker='o',s=15,color='blue')




plt.legend(frameon=0,loc=(0.37,0.03),ncol=2,columnspacing=0.3,handletextpad=0.2,labelspacing=0.4,fontsize=9)
plt.xlabel('$x/{B}_{0}$')
plt.ylabel('$y/{B}_{0}$')
#plt.text(0.18,-0.12,'(a)',fontsize=22)
plt.ylim(-0.6,0)
plt.xticks([0,0.25,0.5,0.75,1],fontsize=10)
plt.yticks([-0.5,-0.25,0],fontsize=10)
plt.xlim(0,1)
plt.text(0.57,-0.25,'(Case, ${Fr}_{0}$, ${Fr}_{d-0}$)',fontsize=9)



plt.show()



