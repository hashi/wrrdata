#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 15:45:16 2020

@author: haoran
"""

import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize  import minimize
import numpy as np
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from pylab import *
import openpyxl
from openpyxl import load_workbook

from scipy.signal import savgol_filter

plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys
fig = plt.figure(figsize=(3,3.5),dpi=600)
def plotexp (num,color,lab,marker):
   
    x = (pd.read_csv("./Case%d/EXP/con0.3_x.csv"%num, header = None)).values
    y = (pd.read_csv("./Case%d/EXP/con0.3_y.csv"%num, header = None)).values
    y=smooth(y,10)
    if num==19:
       plt.scatter(x[10:480:15]/2,y[10:480:15]/2,marker=marker,color=color,label=lab,s=15)
    else:
       plt.scatter(x[10::15]/2,y[10::15]/2,marker=marker,color=color,label=lab,s=15)


####################################################################################
#Frd=2 case2
############:############

def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    
    return ys



def plotnum (num,color,lab,marker):

    xnum = (pd.read_csv("./Case%d/NUM/con0.3_x.csv"%num, header = None)).values
    ynum = (pd.read_csv("./Case%d/NUM/con0.3_y.csv"%num, header = None)).values
    ynum2=np.zeros(len(ynum))
    for i in range (0,len(ynum)):
        ynum2[i]=ynum[i][0]
    #yhat = savgol_filter(ynum2, 51, 3)   
    yhat = smooth(ynum2, 5)  
    plt.scatter(xnum[::2]/2,yhat[::2]/2,marker=marker,c='',edgecolors=color,label=lab,s=15)



plotexp(20,'red','(1, 0.08, 2)','o')
plotexp(27,'red','(2, 0.13, 2)','s')
plotexp(23,'blue', '(3, 0.08, 3)','o')
plotexp(19,'blue','(4, 0.13, 3)','s')
plotnum(20,'red','(1, 0.08, 2)','o')
plotnum(27,'red','(2, 0.13, 2)','s')
plotnum(23,'blue', '(3, 0.08, 3)','o')
plotnum(19,'blue','(4, 0.13, 3)','s')



plt.yticks([-0.5,-0.25,0,0.25,0.5],fontsize=10)
plt.xticks([0,0.25,0.5,0.75],fontsize=10)
plt.xlim(0,0.9)
plt.ylim(-0.6,0)
plt.xlabel('$x/{B}_{0}$')
#plt.ylabel('$y$ (m)')
plt.legend(loc=(0.24,0.03),fontsize=9,frameon=0,ncol=2,labelspacing=0.3,handletextpad=0.2,columnspacing=0.3)
#plt.text(0.1,-0.12,'(b)',fontsize=15)
plt.text(0.4,-0.41,'(Case, ${Fr}_{0}$, ${Fr}_{d-0}$)',fontsize=9)

