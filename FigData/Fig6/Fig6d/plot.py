#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:32:42 2020

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate



plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

fig = plt.figure(figsize=(2.5, 2.1), dpi=300)


##############
##data smooth
#############
def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys

####case 3   12l Frd=3
##############################
####near bottom 4cm above
##############################


####x=xud
yexp=(-pd.read_csv("./x=xud/EXP/y.csv", header = None)).values/100
Uexp=(pd.read_csv("./x=xud/EXP/U.csv", header = None)).values

ynum=-(pd.read_csv("./x=xud/NUM/y.csv", header = None)).values
Unum=(pd.read_csv("./x=xud/NUM/U.csv", header = None)).values
Unum=smooth(Unum,25)
Uexp=smooth(Uexp,5)





plt.plot(ynum[10::5]/2,Unum[10::5],'--',color='orange',linewidth=1.5)
plt.scatter(yexp[::15]/2,Uexp[::15],marker='s',c='',edgecolor='orange',s=20)


####x=0.5xup
yexp=(-pd.read_csv("./x=0.5xup/EXP/y.csv", header = None)).values/100
Uexp=(pd.read_csv("./x=0.5xup/EXP/U.csv", header = None)).values

ynum=-(pd.read_csv("./x=0.5xup/NUM/y.csv", header = None)).values
Unum=(pd.read_csv("./x=0.5xup/NUM/U.csv", header = None)).values
Unum=smooth(Unum,15)
Uexp=smooth(Uexp,15)


plt.plot(ynum/2,Unum,'--',color='purple',linewidth=1.5)
plt.scatter(yexp[::10]/2,Uexp[::10],marker='o',c='',edgecolor='purple',s=20)


plt.xlabel('$y/{B}_{0}$')
plt.ylabel(r'${\overline{u}}_{s}$/${U}_{0}$')

plt.xticks([0.8,0.4,0,-0.4,-0.8],fontsize=10)
plt.yticks([0,0.4,0.8,1.2],fontsize=10)

plt.xlim(2/2,-2/2)





