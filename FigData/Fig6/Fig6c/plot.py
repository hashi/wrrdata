#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:32:42 2020

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate



plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

##############
##data smooth

##data smooth
#############
def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys


##############################

yexp=(pd.read_csv("./EXP/y.csv", header = None)).values/100  ###change cm into m
Vexp=(pd.read_csv("./EXP/V.csv", header = None)).values
Uexp=(pd.read_csv("./EXP/U.csv", header = None)).values



ynum=(pd.read_csv("./NUM/y.csv", header = None)).values
Vnum=(pd.read_csv("./NUM/V.csv", header = None)).values
Unum=(pd.read_csv("./NUM/U.csv", header = None)).values

Wnum=(pd.read_csv("./NUM/W.csv", header = None)).values

Unum=smooth(Unum,7)
Vnum=smooth(Vnum,7)
Uexp=smooth(Uexp,7)
Wnum=smooth(Wnum,7)


fig, ax1 = plt.subplots(figsize=(2.5,2.1),dpi=300)


ax1.set_xlabel('$y/{B}_{0}$')
ax1.set_ylabel(r'$\overline{u}$/${U}_{0}$', color='red')

Unum=smooth(Unum,8)
ax1.plot(ynum/2,Unum,'--',color='red')

ax1.scatter(yexp[::7]/2,Uexp[::7],c='',edgecolors='red',s=20)
ax1.set_yticks([-0.3,0,0.3,0.6,0.9])
ax1.tick_params(labelsize=10)
ax2 = ax1.twinx()
#ax2.set_ylabel(r'$\overline{v}/{U}_{0}$'' ''$&$'' ' r'$\overline{w}/{U}_{0}$', color='blue')
plt.text(-0.58,-0.12,'$\overline{v}/{U}_{0}$',rotation=90,color='blue')
plt.text(-0.58,0.05,'$\overline{w}/{U}_{0}$',rotation=90,color='green')
plt.text(-0.58,-0.005,'&',rotation=90,color='black')
ax2.set_yticks([-0.3,-0.15,0,0.15,0.3])
ax2.tick_params(labelsize=10)
ax2.set_ylim(-0.3,0.3)
ax2.plot(ynum/2,Vnum,'--',color='blue')
ax2.scatter(yexp[155::9]/2,Vexp[155::9],marker='s',c='',edgecolors='blue',s=20)
ax2.plot(ynum/2,Wnum,color='green',linewidth=0.5)

plt.xlim(0.4,-0.4)
plt.xticks([0.8/2,0.4/2,0,-0.4/2,-0.8/2],fontsize=10)



