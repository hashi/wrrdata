#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:32:42 2020

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate



plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

fig = plt.figure(figsize=(3, 1.7), dpi=300)
 

xup=1.48
xud=1.97
##############################
####near bottom
##############################
xexp=(pd.read_csv("./EXP/x.csv", header = None)).values/100
Uexp=(pd.read_csv("./EXP/U.csv", header = None)).values

xnum=(pd.read_csv("./NUM/x.csv", header = None)).values
Unum=(pd.read_csv("./NUM/U.csv", header = None)).values

def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys

(Unum)=smooth(Unum,12)
  
Uexp[15]=Uexp[3]
plt.scatter(xexp[15::10]/2,Uexp[15::10],marker='o',c='',edgecolors='purple',label='H=-4cm EXP',s=20)
plt.plot(xnum/2,Unum,'--',color='purple',label='H=-4cm NUM',linewidth=1.5)

plt.xlim(-0,1.4)
plt.ylim(0,1.5)

plt.xlabel('$x/{B}_{0}$')
plt.plot([xup/2,xup/2],[0,1.5],'--',color='black',linewidth=1.5)
plt.plot([xud/2,xud/2],[0,1.5],'--',color='black',linewidth=1.5)
plt.ylabel(r'${\overline{u}}_{s}$/${U}_{0}$')

#plt.plot([1.46,1.46],[-1,1.5], color='black',linewidth=1.5)
#plt.legend(frameon=0,loc='lower left',ncol=2)
#plt.text(1.15,1.3,'(a)',fontsize=12)
#plt.text(0.05,1.3,'(b)')
plt.text(0.7,1.6,"${x}_{p}$")
plt.text(0.95,1.6,"${x}_{ud}$")
plt.xticks([0,0.4,0.8,1.2],fontsize=10)
plt.yticks([0,0.5,1,1.5],fontsize=10)
plt.show()


