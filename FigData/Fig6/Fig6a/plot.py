#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 11:32:42 2020

@author: haoran
"""
from matplotlib import pylab
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import openpyxl
from openpyxl import load_workbook
from matplotlib.ticker import FuncFormatter
from scipy import interpolate



plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

fig = plt.figure(figsize=(3, 1.7), dpi=300)
 

####case 3   12l Frd=3
##############################
####near surface z=-4cm
##############################
xup=1.48
xexp=(pd.read_csv("./EXP/xcen.csv", header = None)).values/100
Uexp=(pd.read_csv("./EXP/Ue.csv", header = None)).values/8.2

       
       
xnum=(pd.read_csv("./NUM/x.csv", header = None)).values
Unum=(pd.read_csv("./NUM/U.csv", header = None)).values



plt.scatter(xexp[0::8]/2,Uexp[0::8],c='',edgecolors='red',label='z=-4cm EXP',s=20)
plt.plot(xnum[::10]/2,Unum[::10],'--',color='red',label='H=-4cm NUM',linewidth=1.5)

plt.xlim(0,1.05)
plt.ylim(-0.1,1.5)
plt.ylabel(r'$\overline{u}$/${U}_{0}$')
plt.plot([1.48/2,1.48/2],[-0.1,1.5],'--',color='black',linewidth=1.5)
plt.text(0.7,1.6,"${x}_{p}$")
#plt.text(0.05,1.3,'(a)')
plt.xticks([0,0.25,0.5,0.75,1],fontsize=10)
plt.yticks([0,0.5,1,1.5],fontsize=10)
plt.xlabel('$x/{B}_{0}$')
plt.show()
