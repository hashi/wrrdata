#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize  import minimize
import numpy as np
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from pylab import *

plt.rcParams.update({'font.size': 11,'font.family':'Times New Roman'})
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   :11,
}

fig, ax = plt.subplots(subplot_kw={"projection":"3d"},figsize=(3.5,3),dpi=500)

#plot the slope

def smooth (y,num):
    ys=np.zeros(len(y))
    for i in range (0,len(y)):
        if num<=i<len(y)-num:
            for j in range (-num,num+1):
                
                ys[i]=ys[i]+y[i+j]
            
            ys[i]=ys[i]/(2*num+1)
        else:
            ys[i]=y[i]
    return ys

X = np.linspace(-2,200,211)
Y = np.linspace(-150,150,301)
X, Y = np.meshgrid(X,Y)
Z = np.zeros((len(X),len(X[0])))
for i in range (0,len(X)):
    for j in range (0, len(X[0])):
         Z[i][j] = -X[i][j]*tan(8/180*np.pi)-8
surf = ax.plot_surface(X/100, Y/100, Z/100, color='grey',  alpha=0.3)



for i in range (20,170,20):
    y= (pd.read_csv("./y%d7cm.csv"%i, header = None)).values*100
    z= (pd.read_csv("./z%d7cm.csv"%i, header = None)).values*100
    y1=np.zeros(len(y))
    z1=np.zeros(len(z))    
    x=np.zeros(len(y))
    for j in range (0,len(z)):
        x[j]=i     
        y1[j]=y[j]
        z1[j]=z[j]
    z1=smooth(z1,20)
    ax.plot(x/100, y1/100, z1/100, '--',color='black',linewidth=1.5)


my_x_ticks = np.arange(0, 2, 0.5)
my_z_ticks = np.arange(-0.32, 0, 0.08)
my_y_ticks = np.arange(-1, 1.1, 1)
ax.set_xlim(0, 2)
ax.set_ylim(-1.5, 1.5)
ax.set_xticks(my_x_ticks)
ax.set_zticks(my_z_ticks)

ax.set_yticks(my_y_ticks)
ax.grid(True)

ax.set_xlabel('')
ax.set_ylabel('')
ax.set_zlabel('')
#ax.text(0.1,-1.50,0.0,'(e)')
ax.view_init(45,-35)
plt.tight_layout()
